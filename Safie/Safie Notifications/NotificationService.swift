//
//  NotificationService.swift
//  Safie Notifications
//
//  Created by Matyáš Procházka on 26.03.2022.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            let languageCode = LanguageController.shared.getLanguage().code

            if let title = request.content.userInfo["localized-title-\(languageCode)"] as? String,
                let body = request.content.userInfo["localized-body-\(languageCode)"] as? String
            {
                bestAttemptContent.title = title
                bestAttemptContent.body = body
            }
            
            if let type = request.content.userInfo["type"] as? String {
                let filePath = Bundle.main.path(forResource: type, ofType: "png")!
                let fileUrl = URL(fileURLWithPath: filePath)
                                    
                let imageAttachment = try? UNNotificationAttachment(
                    identifier: "image",
                    url: fileUrl,
                    options: nil)
                
                if let imageAttachment = imageAttachment {
                    bestAttemptContent.attachments = [imageAttachment]
                }
            }
            
            contentHandler(bestAttemptContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
