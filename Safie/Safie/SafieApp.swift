//
//  SafieApp.swift
//  Safie
//
//  Created by Matyáš Procházka on 20.02.2022.
//

import SwiftUI
import Firebase
import FacebookCore
import GoogleSignIn

@main
struct SafieApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    @State var deepLink: DeepLinkUtil.DeepLink?
    
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        guard let deepLink = DeepLinkUtil.handleDynamicLink(dynamicLink) else { return }
        
        self.deepLink = deepLink
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.deepLink = nil
        }
    }
    
    init() {
        UITextView.appearance().textColor = .white
        
        guard let languageCode = Locale.current.languageCode else { return }
        
        LanguageController.shared.setLanguage(languageCode)
    }
    
    var body: some Scene {
        WindowGroup {
            RootScreen()
                .environment(\.deepLink, self.deepLink)
                .onOpenURL { url in
                    // Firebase dynamic links
                    DynamicLinks.dynamicLinks()
                        .handleUniversalLink(url) { dynamicLink, error in
                            guard error == nil else {
                                return
                            }
                            
                            if let dynamicLink = dynamicLink {
                                self.handleDynamicLink(dynamicLink)
                            }
                        }
                    
                    // Facebook Sign In
                    ApplicationDelegate.shared.application(
                        UIApplication.shared,
                        open: url,
                        sourceApplication: nil,
                        annotation: UIApplication.OpenURLOptionsKey.annotation
                    )
                    
                    // Google Sign In
                    GIDSignIn.sharedInstance.handle(url)
                }            
        }
    }
}
