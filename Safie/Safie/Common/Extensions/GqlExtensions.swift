//
//  GqlExtensions.swift
//  Safie
//
//  Created by Matyáš Procházka on 18.08.2021.
//

import Foundation

extension SosState: Codable {
}

extension CheckinRequestType: Codable {
}

extension CheckinRequestState: Codable {
}

extension CheckinState: Codable {
}

extension TripState: Codable {
}

extension TripActivityKey: Codable {
}
