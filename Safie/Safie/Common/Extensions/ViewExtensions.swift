//
//  ViewExtensions.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2021.
//

import SwiftUI

func hhaptic(style: UIImpactFeedbackGenerator.FeedbackStyle = .medium) -> Void {
    let impact = UIImpactFeedbackGenerator(style: style)
    
    impact.impactOccurred()
}

extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    func haptic(style: UIImpactFeedbackGenerator.FeedbackStyle = .medium) -> some View {
      self.onTapGesture {
          hhaptic(style: style)
      }
    }
}
