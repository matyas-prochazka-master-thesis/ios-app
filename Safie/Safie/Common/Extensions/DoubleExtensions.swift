//
//  DoubleExtensions.swift
//  Safie
//
//  Created by Matyáš Procházka on 19.10.2021.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
