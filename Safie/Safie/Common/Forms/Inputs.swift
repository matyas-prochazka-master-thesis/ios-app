//
//  Inputs.swift
//  Safie
//
//  Created by Matyáš Procházka on 28.06.2021.
//

import Foundation
import Combine

/*
 BasicInput implements just the validate and subscribe method
 Then specific Input implement the concrete validators
 */
class BasicInput: ObservableObject {
    internal var notify: [(() -> Void)?] = []
    
    @Published var valid: Bool = false
    @Published var touched: Bool = false
    @Published var errors: [String] = []
    
    internal var validators: [() -> String?] = []

    func validate(_ touched: Bool = true) -> Void {
        if touched {
            self.touched = touched
        }
        
        var errors: [String] = []
        var valid: Bool = true
        
        self.validators.forEach { validator in
            if let error = validator() {
                errors.append(error)
                valid = false
            }
        }
        
        self.valid = valid
        self.errors = errors
        
        for notify in self.notify {
            notify?()
        }
    }
    
    func subscribe(notify: @escaping () -> Void) -> Void {
        self.notify.append(notify)
    }
}

class StringInput: BasicInput {
    @Published var value: String = "" {
        didSet {
            super.validate(true)
        }
    }
    
    private var required: Bool = false
    private var requiredMessage: String = "Value is required"
    
    private var max: Int?
    private var maxMessage: String = "Value is too long"
    
    private var min: Int?
    private var minMessage: String = "Value is too short"
    
    private var sameAs: StringInput?
    private var sameAsMessage: String = "Value is not the same"
    
    private var emailMessage: String = "Email is not valid"
    
    private var regexMessage: String = "Value is not valid"
    
    func required(_ message: String?) -> Self {
        self.required = true
        
        if let message = message {
            self.requiredMessage = message
        }
        
        super.validators.append {
            if self.required && self.value.isEmpty {
                return message ?? "Value is required"
            }
            
            return nil
        }
        
        return self
    }
    
    // value.count
    func max(_ max: Int, _ message: String?) -> Self {
        self.max = max
        
        super.validators.append {
            guard self.value.count <= max else {
                return message ?? self.maxMessage
            }
            
            return nil
        }
        
        return self
    }
    
    // value.count
    func min(_ min: Int, _ message: String?) -> Self {
        self.min = min
        
        super.validators.append {
            guard self.value.count >= min else {
                return message ?? self.minMessage
            }
            
            return nil
        }
        
        return self
    }
    
    func sameAs(_ input: inout StringInput, _ message: String?) -> Self {
        input.subscribe(notify: { self.validate(false) })
        
        self.sameAs = input
        
        super.validators.append {
            guard self.value == self.sameAs?.value else {
                return message ?? self.sameAsMessage
            }
            
            return nil
        }
        
        return self
    }
    
    // email regex
    func email(_ message: String?) -> Self {
        func isValidEmail(_ email: String) -> Bool {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

            let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailPred.evaluate(with: email)
        }
        
        if let message = message {
            self.emailMessage = message
        }
        
        super.validators.append {
            guard isValidEmail(self.value) else {
                return message ?? self.emailMessage
            }
            
            return nil
        }
        
        return self
    }
    
    func regex(_ regex: String, _ message: String?) -> Self {
        func isValid(_ text: String) -> Bool {
            let pred = NSPredicate(format: "SELF MATCHES %@", regex)
            return pred.evaluate(with: text)
        }
        
        if let message = message {
            self.regexMessage = message
        }
        
        super.validators.append {
            guard isValid(self.value) else {
                return message ?? self.regexMessage
            }
            
            return nil
        }
        
        return self
    }
}

