//
//  File.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import Foundation

struct File: Codable, Hashable, Identifiable {
    var id: String
    var path: String
    
    var absoluteUrl: String {
        get {
            "\(AppConfig.staticUrl)\(self.path)"
        }
    }
    
    init(id: String, path: String) {
        self.id = id
        self.path = path
    }
}
