//
//  QuickAction.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import Foundation

struct QuickAction: Codable, Hashable, Identifiable {
    var id: String
    var name: String
    var icon: String
    
    init(id: String, name: String, icon: String) {
        self.id = id
        self.name = name
        self.icon = icon
    }
}
