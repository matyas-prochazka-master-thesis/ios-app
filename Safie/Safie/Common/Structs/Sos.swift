//
//  Sos.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import Foundation

struct Sos: Codable, Hashable, Identifiable {
    var id: String
    var idFromClient: String
    var account: Account?
    
    var state: SosState
    
    var initialLatitude: Double?
    var initialLongitude: Double?
    
    var buttonPressedAt: Date?
    var buttonReleasedAt: Date?
    var pinEnteredAt: Date?
    var guardiansContactedAt: Date?
    
    var createdAt: Date
    
    var recent: Bool {
        (Date()).timeIntervalSince(self.createdAt) <= 3 * 60 * 60
    }
    
    init(
        id: String,
        idFromClient: String,
        account: Account?,
        state: SosState,
        initialLatitude: Double?,
        initialLongitude: Double?,
        buttonPressedAt: Date?,
        buttonReleasedAt: Date?,
        pinEnteredAt: Date?,
        guardiansContactedAt: Date?,
        createdAt: Date
    ) {
        self.id = id
        self.idFromClient = idFromClient
        self.account = account
        self.state = state
        self.initialLatitude = initialLatitude
        self.initialLongitude = initialLongitude
        self.buttonPressedAt = buttonPressedAt
        self.buttonReleasedAt = buttonReleasedAt
        self.pinEnteredAt = pinEnteredAt
        self.guardiansContactedAt = guardiansContactedAt
        self.createdAt = createdAt
    }
}
