//
//  Guardian.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import Foundation

struct Guardian: Codable, Hashable, Identifiable {
    var id: String
    var account: Account?
    var guardian: Account?
    var acceptedAt: Date?
    var createdAt: Date
    
    init(id: String, account: Account?, guardian: Account?, acceptedAt: Date?, createdAt: Date) {
        self.id = id
        self.account = account
        self.guardian = guardian
        self.acceptedAt = acceptedAt
        self.createdAt = createdAt
    }
}
