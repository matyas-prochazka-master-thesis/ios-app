//
//  Checkin.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.04.2022.
//

import Foundation
import MapKit

struct Checkin: Codable, Hashable, Identifiable {
    var id: String
    
    var request: CheckinRequest?
    
    var state: CheckinState
    
    var latitude: Double?
    var longitude: Double?
    
    var checkedAt: Date?
    var createdAt: Date
    
    var coordinate: CLLocationCoordinate2D? {
        guard let latitude = self.latitude, let longitude = self.longitude else { return nil }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(
        id: String,
        request: CheckinRequest?,
        state: CheckinState,
        latitude: Double?,
        longitude: Double?,
        checkedAt: Date?,
        createdAt: Date
    ) {
        self.id = id
        self.request = request
        self.state = state
        self.latitude = latitude
        self.longitude = longitude
        self.checkedAt = checkedAt
        self.createdAt = createdAt
    }
}
