//
//  TripActivity.swift
//  Safie
//
//  Created by Matyáš Procházka on 06.04.2022.
//

import Foundation
import MapKit

struct TripActivity: Codable, Hashable, Identifiable {
    static func == (lhs: TripActivity, rhs: TripActivity) -> Bool {
        lhs.id == rhs.id
    }
    
    var id: String
    
    var key: TripActivityKey
    
    var latitude: Double?
    var longitude: Double?
    var batteryLevel: Double?
    
    var payloadMessage: ActivityPayloadMessage?
    var payloadQuickAction: ActivityPayloadQuickAction?
    
    var attachment: File?
    
    var createdAt: Date
    
    var coordinates: CLLocationCoordinate2D? {
        guard let latitude = self.latitude, let longitude = self.longitude else { return nil }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(
        id: String,
        key: TripActivityKey,
        latitude: Double?,
        longitude: Double?,
        batteryLevel: Double?,
        payloadMessage: ActivityPayloadMessage?,
        payloadQuickAction: ActivityPayloadQuickAction?,
        attachment: File?,
        createdAt: Date
    ) {
        self.id = id
        self.key = key
        self.latitude = latitude
        self.longitude = longitude
        self.batteryLevel = batteryLevel
        self.payloadMessage = payloadMessage        
        self.payloadQuickAction = payloadQuickAction
        self.attachment = attachment
        self.createdAt = createdAt
    }
}

struct ActivityPayloadMessage: Codable, Hashable {
    var message: String
}

struct ActivityPayloadQuickAction: Codable, Hashable {
    var name: String
    var icon: String
}
