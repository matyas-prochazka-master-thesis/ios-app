//
//  CheckinRequest.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.04.2022.
//

import Foundation

struct CheckinRequest: Codable, Hashable, Identifiable {
    var id: String
    var requester: Account?
    var targetAccount: Account?
    
    var guardians: [Account]
    
    var type: CheckinRequestType
    var state: CheckinRequestState
    
    var startsAt: Date?
    var endsAt: Date?
    var periodInMinutes: Double?
    var nextCheckinAt: Date?
    
    var createdAt: Date
    
    
    init(
        id: String,
        requester: Account?,
        targetAccount: Account?,
        guardians: [Account],
        type: CheckinRequestType,
        state: CheckinRequestState,
        startsAt: Date?,
        endsAt: Date?,
        periodInMinutes: Double?,
        nextCheckinAt: Date?,
        createdAt: Date
    ) {
        self.id = id
        self.requester = requester
        self.targetAccount = targetAccount
        self.guardians = guardians
        self.type = type
        self.state = state
        self.startsAt = startsAt
        self.endsAt = endsAt
        self.periodInMinutes = periodInMinutes
        self.nextCheckinAt = nextCheckinAt
        self.createdAt = createdAt
    }
}
