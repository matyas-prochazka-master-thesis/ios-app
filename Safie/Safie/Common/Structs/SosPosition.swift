//
//  SosPosition.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import Foundation
import MapKit

struct SosPosition: Codable, Hashable, Identifiable {
    var id: String
    var latitude: Double
    var longitude: Double
    var createdAt: Date
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
    }
    
    init(id: String, latitude: Double, longitude: Double, createdAt: Date) {
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.createdAt = createdAt
    }
}
