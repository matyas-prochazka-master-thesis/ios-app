//
//  Trip.swift
//  Safie
//
//  Created by Matyáš Procházka on 06.04.2022.
//

import Foundation

struct Trip: Codable, Hashable, Identifiable {
    var id: String
    var account: Account?
    var state: TripState
    var shouldArriveAt: Date?
    var destination: TripDestination?
    var quickActions: [QuickAction]
    var currentQuickActionIndex: Int
    var createdAt: Date
    
    init(
        id: String,
        account: Account?,
        state: TripState,
        shouldArriveAt: Date?,
        destination: TripDestination?,
        quickActions: [QuickAction],
        currentQuickActionIndex: Int,
        createdAt: Date
    ) {
        self.id = id
        self.account = account
        self.state = state
        self.shouldArriveAt = shouldArriveAt
        self.destination = destination
        self.quickActions = quickActions
        self.currentQuickActionIndex = currentQuickActionIndex
        self.createdAt = createdAt
    }
}
