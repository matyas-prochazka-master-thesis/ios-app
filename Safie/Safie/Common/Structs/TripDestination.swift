//
//  TripDestination.swift
//  Safie
//
//  Created by Matyáš Procházka on 07.04.2022.
//

import Foundation
import MapKit

struct TripDestination: Codable, Hashable, Identifiable {
    static func == (lhs: TripDestination, rhs: TripDestination) -> Bool {
        lhs.id == rhs.id
    }
    
    var id: String
    
    var name: String
    
    var latitude: Double
    var longitude: Double
    
    var favourite: Bool
    
    var createdAt: Date
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
    }
    
    init(
        id: String,
        name: String,
        latitude: Double,
        longitude: Double,
        favourite: Bool,
        createdAt: Date
    ) {
        self.id = id
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.favourite = favourite
        self.createdAt = createdAt
    }
}
