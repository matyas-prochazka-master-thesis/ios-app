//
//  Account.swift
//  Fooder
//
//  Created by Matyáš Procházka on 25.06.2021.
//

import Foundation

struct Account: Codable, Hashable, Identifiable {
    var id: String
    var username: String?
    var email: String
    
    var profilePicture: File?
    
    init(id: String, username: String?, email: String, profilePicture: File?) {
        self.id = id
        self.username = username
        self.email = email
        
        self.profilePicture = profilePicture
    }
}
