//
//  SearchLocation.swift
//  Safie
//
//  Created by Matyáš Procházka on 08.04.2022.
//

import Foundation
import CoreLocation

struct SearchLocation: Codable, Hashable, Identifiable {
    var id: String
    var title: String
    var subtitle: String?
    
    var latitude: Double?
    var longitude: Double?
    
    var coordinates: CLLocationCoordinate2D? {
        guard let latitude = self.latitude, let longitude = self.longitude else { return nil }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(id: String, title: String, subtitle: String? = nil, latitude: Double? = nil, longitude: Double? = nil) {
        self.id = id
        self.title = title
        self.subtitle = subtitle
        self.latitude = latitude
        self.longitude = longitude
    }
}
