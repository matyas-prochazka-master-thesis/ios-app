//
//  ActivityView.swift
//  Safie
//
//  Created by Matyáš Procházka on 16.09.2021.
//

import SwiftUI

struct ShareSheet: UIViewControllerRepresentable {
    let sharing: [Any]
    let applicationActivities: [UIActivity]? = nil

    func makeUIViewController(context: UIViewControllerRepresentableContext<ShareSheet>) -> UIActivityViewController {
        let controller = UIActivityViewController(
            activityItems: sharing,
            applicationActivities: applicationActivities
        )
        controller.modalPresentationStyle = .pageSheet
        
        return controller
    }

    func updateUIViewController(
        _ uiViewController: UIActivityViewController,
        context: UIViewControllerRepresentableContext<ShareSheet>
    ) {
    }
}
