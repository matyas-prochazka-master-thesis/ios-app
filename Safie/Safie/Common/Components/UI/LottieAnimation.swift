//
//  LottieView.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2021.
//

import SwiftUI
import Lottie

struct LottieAnimation: UIViewRepresentable {
    typealias UIViewType = UIView
    var filename: String
    var animationSpeed: CGFloat = 1
    var loop: Bool = true
  
    func makeUIView(context: UIViewRepresentableContext<LottieAnimation>) -> UIView {
        let view = UIView(frame: .zero)

        let animationView = AnimationView()
        let animation = Animation.named(filename)
            
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = loop ? .loop : .playOnce
        animationView.play()
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.animationSpeed = animationSpeed

        animationView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(animationView)

        NSLayoutConstraint.activate([
          animationView.widthAnchor.constraint(equalTo: view.widthAnchor),
          animationView.heightAnchor.constraint(equalTo: view.heightAnchor),
          animationView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
          animationView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])

        return view
    }
    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<LottieAnimation>) { }
}
