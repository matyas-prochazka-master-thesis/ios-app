//
//  ArrowLinkView.swift
//  Safie
//
//  Created by Matyáš Procházka on 31.03.2021.
//

import SwiftUI

struct ArrowLink: View {
    var label: Text
    var arrowLabel: Text?
    var showArrow: Bool = true
    var foregroundColor: Color = .black
    
    var body: some View {
        HStack {
            self.label
                .bodyTextStyle()
                .foregroundColor(self.foregroundColor)
            
            Spacer()
            
            if let arrowLabel = self.arrowLabel {
                arrowLabel
                    .bodyLabelTextStyle()
            }
                        
            Image(systemName: "chevron.right")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 15, height: 15)
                .foregroundColor(Color("Label"))
                .opacity(self.showArrow ? 1 : 0)
        }
    }
}

struct ArrowLink_Previews: PreviewProvider {
    static var previews: some View {
        ArrowLink(label: Text("preview.arrow_link"))
    }
}
