//
//  SectionLabelView.swift
//  Safie
//
//  Created by Matyáš Procházka on 25.03.2021.
//

import SwiftUI

struct SectionLabel: View {
    var label: Text   
    
    var body: some View {
        self.label
            .body2LabelTextStyle()
            .alignContent(.leading)
            .padding([.leading, .top, .trailing], .normal)
            .padding(.bottom, .small)
            .background(Color("BackgroundGray"))
    }
}

struct SectionLabel_Previews: PreviewProvider {
    static var previews: some View {
        SectionLabel(label: Text("preview.section_label"))
    }
}
