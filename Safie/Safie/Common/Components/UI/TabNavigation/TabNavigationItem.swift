//
//  TabNavigationItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2021.
//

import Foundation
import SwiftUI

struct TabNavigationItem<Tabs> {
    var tab: Tabs
    var title: Text
    var icon: String
}
