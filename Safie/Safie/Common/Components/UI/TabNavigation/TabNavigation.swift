//
//  TabNavigationView.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2021.
//

import SwiftUI

struct TabNavigation<Tab: Equatable>: View {
    @Binding var selection: Tab
    var tabs: [TabNavigationItem<Tab>]
    
    var body: some View {
        HStack {
            ForEach(0..<self.tabs.count, id: \.self) { tabIndex in
                let tab = tabs[tabIndex]
                
                Button(action: {
                    withAnimation {
                        self.selection = tab.tab
                    }
                }) {
                    VStack {
                        Image(systemName: tab.icon)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 20, height: 20)
                        
                        tab.title
                            .body2TextStyle()
                            .padding(.top, 5)
                    }
                    .foregroundColor(self.selection == tab.tab ? Color("Primary") : .black)
                }
                .frame(maxWidth: .infinity)
            }
        }
    }
}
