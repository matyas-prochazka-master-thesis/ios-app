//
//  TextFieldAlert.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.04.2021.
//

import SwiftUI

extension View {
    func textFieldAlert(
        isShowing: Binding<Bool>,
        title: Text,
        placeholder: String,
        text: Binding<String>,
        onSubmit: @escaping () -> Void
    ) -> some View {
        TextFieldAlert(
            isShowing: isShowing,
            title: title,
            placeholder: placeholder,
            text: text,
            onSubmit: onSubmit,
            presenting: self
        )
    }
}

struct TextFieldAlert<Presenting>: View where Presenting: View {
    @Binding var isShowing: Bool
    
    let title: Text
    var placeholder: String
    @Binding var text: String
    var onSubmit: () -> Void
    
    let presenting: Presenting

    var body: some View {
        GeometryReader { (deviceSize: GeometryProxy) in
            ZStack {
                self.presenting.disabled(self.isShowing)
                                
                VStack {
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .background(Color(.black))
                .opacity(self.isShowing ? 0.4 : 0)
                .edgesIgnoringSafeArea(.bottom)
                
                VStack {
                    self.title
                        .font(.bold, .normal)                        
                    
                    TextField(self.placeholder, text: self.$text)
                        .textFieldStyle(BasicTextFieldStyle())
                    
                    Divider()
                    
                    HStack {
                        Spacer()
                        
                        Button(action: {
                            withAnimation {
                                hideKeyboard()
                                self.isShowing.toggle()
                            }
                        }) {
                            Text("Cancel")
                                .bodyDangerTextStyle()
                        }
                        
                        Spacer()
                        
                        Button(action: {
                            withAnimation {
                                hideKeyboard()
                                onSubmit()                                
                                self.isShowing.toggle()
                            }
                        }) {
                            Text("Confirm")
                                .bodyActionTextStyle()
                        }
                        
                        Spacer()
                    }
                    .padding(.top, .small)
                }
                .padding(.normal)
                .background(Color.black)
                .cornerRadius(.small)
                .frame(
                    width: deviceSize.size.width * 0.7,
                    height: deviceSize.size.height * 0.7,
                    alignment: .bottom
                )
                .opacity(self.isShowing ? 1 : 0)
            }
        }
    }
}
