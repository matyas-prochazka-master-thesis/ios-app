//
//  LogoutView.swift
//  Safie
//
//  Created by Matyáš Procházka on 31.03.2021.
//

import SwiftUI

struct LogoutLabel: View {
    var label: Text
    
    var body: some View {
        self.label
            .bodyDangerTextStyle()
            .frame(maxWidth: .infinity, alignment: .center)
            .padding(.normal)            
            .background(Color("BackgroundGray"))
    }
}

struct LogoutView_Previews: PreviewProvider {
    static var previews: some View {
        LogoutLabel(label: Text("preview.logout_label"))
    }
}
