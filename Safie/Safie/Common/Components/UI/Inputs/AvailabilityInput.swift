//
//  AvailabilityInputView.swift
//  Safie
//
//  Created by Matyáš Procházka on 31.03.2021.
//

import SwiftUI

struct AvailabilityInput: View {
    @Binding var from: Date
    @Binding var to: Date
    
    var range = Date()...Calendar.current.date(byAdding: .day, value: 7, to: Date())!
    
    var body: some View {
        VStack(spacing: 0) {
            DatePicker(selection: $from, in: range, label: {
                Text("availability_input.start")
                    .bodyTextStyle()
            })
            
            Divider()
                .padding([.top, .bottom], .extraSmall)
                                        
            DatePicker(selection: $to, in: range, label: {
                Text("availability_input.end")
                    .bodyTextStyle()
            })
        }
    }
}

struct AvailabilityInput_Previews: PreviewProvider {
    @State static var from: Date = Date()
    @State static var to: Date = Date()
    
    static var previews: some View {
        AvailabilityInput(from: $from, to: $to)
    }
}
