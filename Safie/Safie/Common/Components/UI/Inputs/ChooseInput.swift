//
//  ChooseInputView.swift
//  Safie
//
//  Created by Matyáš Procházka on 25.03.2021.
//

import SwiftUI

struct ChooseInputOption<Options> {
    var label: Text
    var value: Options
}

struct ChooseInput<Options: Equatable>: View {
    var label: Text
    
    var leftOption: ChooseInputOption<Options>
    var rightOption: ChooseInputOption<Options>
    
    @Binding var selectedValue: Options
    
    var body: some View {
        VStack(spacing: 0) {
            self.label
                .bodyTextStyle()
                .frame(maxWidth: .infinity, alignment: .leading)
                        
            HStack(spacing: 0) {
                leftOption.label
                    .h4TextStyle()
                    .foregroundColor(selectedValue == leftOption.value ? .white : .black)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(selectedValue == leftOption.value ? Color("LightPrimary") : Color("Gray"))
                    .cornerRadius(.extraSmall)
                    .onTapGesture {
                        withAnimation {
                            self.selectedValue = leftOption.value
                        }
                    }
                    .haptic()
                            
                rightOption.label
                    .h4TextStyle()
                    .foregroundColor(selectedValue == rightOption.value ? .white : .black)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(selectedValue == rightOption.value ? Color("LightPrimary") : Color("Gray"))
                    .cornerRadius(.extraSmall)
                    .onTapGesture {
                        withAnimation {
                            self.selectedValue = rightOption.value
                        }
                    }
                    .haptic()
            }
            .frame(height: 30)
            .padding(2)
            .background(Color("Gray"))
            .cornerRadius(.extraSmall)
            .padding(.top, .small)
        }
    }
}

struct ChooseInput_Previews: PreviewProvider {
    enum Options {
        case now
        case period
    }
    
    static var leftOption = ChooseInputOption<Options>(label: Text("preview.choose_input.now"), value: .now)
    static var rightOption = ChooseInputOption<Options>(label: Text("preview.choose_input.period"), value: .period)
    
    @State static var selectedValue: Options = .now
    
    static var previews: some View {
        ChooseInput<Options>(label: Text("preview.choose_input.availability"), leftOption: leftOption, rightOption: rightOption, selectedValue: $selectedValue)
    }
}
