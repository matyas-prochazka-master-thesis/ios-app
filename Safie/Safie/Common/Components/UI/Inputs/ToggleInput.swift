//
//  ToggleWithActionsView.swift
//  Safie
//
//  Created by Matyáš Procházka on 06.04.2021.
//

import SwiftUI

struct ToggleInput: View {
    var label: Text
    var isOn: Bool
    
    var onSelect: () -> Void = {}
    var onDeselect: () -> Void = {}
    
    var body: some View {
        HStack {
            self.label
                .bodyTextStyle()
            
            Spacer()
            
            VStack {
                Circle()
                    .fill(Color.white)
                    .frame(width: 27, height: 27)
                    .shadow(radius: 2, x: 1, y: 1)
                    .offset(x: self.isOn ? 20 : 0, y: 0)
            }
            .padding(2)
            .frame(width: 51, height: 31, alignment: .leading)
            .background(self.isOn ? Color("PrimaryAction") : Color("DarkGray"))
            .cornerRadius(15.5)
            .animation(.easeInOut, value: self.isOn)
            .onTapGesture {
                if self.isOn {
                    self.onDeselect()
                } else {
                    self.onSelect()
                }
            }
            .haptic()
        }
    }
}
