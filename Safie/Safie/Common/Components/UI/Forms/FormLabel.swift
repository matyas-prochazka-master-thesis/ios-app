//
//  FormLabel.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.08.2021.
//

import SwiftUI

struct FormLabel: View {
    var label: Text
    
    var body: some View {
        self.label
            .body2LabelTextStyle()
            .alignContent(.leading)
    }
}

struct FormLabel_Previews: PreviewProvider {
    static var previews: some View {
        FormLabel(label: Text("preview.form_label"))
    }
}
