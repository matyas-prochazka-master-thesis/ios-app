//
//  FormError.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.08.2021.
//

import SwiftUI

struct FormError: View {
    var error: Text = Text("")
    
    @State var maxHeight: CGFloat? = 0
    
    var body: some View {
        self.error
            .body2DangerTextStyle()
            .alignContent(.leading)
            .padding(.top, .extraSmall)            
            .frame(height: self.maxHeight)
            .clipped()
            .onAppear {
                withAnimation {
                    self.maxHeight = nil
                }
            }
    }
}

struct FormError_Previews: PreviewProvider {
    static var previews: some View {
        FormError(error: Text("preview.form_error"))
    }
}
