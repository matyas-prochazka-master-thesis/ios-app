//
//  BlueButtonView.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2021.
//

import SwiftUI

struct SecondaryButton: View {
    var title: Text
    var radius: RadiusSize = .normal
    var shadow: ShadowSize? = nil
    
    var action: () -> Void
    
    var body: some View {
        ActionButton(
            title: self.title,
            backgroundColor: Color("Gray"),
            foregroundColor: Color("Action"),
            radius: self.radius,
            shadow: self.shadow,
            action: self.action
        )
    }
}

struct SecondaryButton_Previews: PreviewProvider {
    static var previews: some View {
        SecondaryButton(title: Text("preview.gray_button"), action: {})
    }
}
