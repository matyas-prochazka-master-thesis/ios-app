//
//  BlueButtonView.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2021.
//

import SwiftUI

struct ActionButton: View {
    var title: Text
    var large: Bool = false
    var icon: String?
    var image: String?
    var backgroundColor: Color = Color("PrimaryAction")
    var foregroundColor: Color = .white
    var radius: RadiusSize = .small
    var shadow: ShadowSize? = nil
    var disabled: Bool = false
    
    var action: () -> Void
    
    var body: some View {
        Button(action: self.action) {
            HStack {
                if let iconName = self.icon {
                    Image(systemName: iconName)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 20, height: 20)
                        .foregroundColor(.white)
                        .colorMultiply(self.disabled ? .black : self.foregroundColor)
                }
                
                if let imageName = self.image {
                    Image(imageName)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 20, height: 20)
                }
                
                self.title
                    .buttonTextStyle()
                    .foregroundColor(.white)
                    .colorMultiply(self.disabled ? .black : self.foregroundColor)
            }
            .padding(10)            
            .frame(maxWidth: .infinity)
            .background(self.disabled ? Color("Label") : self.backgroundColor)
            .cornerRadius(self.radius)
            .shadow(radius: self.shadow?.rawValue ?? 0)
        }
        .haptic()
    }
}

struct ActionButton_Previews: PreviewProvider {
    static var previews: some View {
        ActionButton(title: Text("preview.action_button"), action: {})
    }
}
