//
//  RefreshControl.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import SwiftUI

struct RefreshControl: View {
    var coordinateSpace: CoordinateSpace
    @Binding var refreshing: Bool    
    var onRefresh: () -> Void
    
    var body: some View {
        GeometryReader { geo in
            if (geo.frame(in: self.coordinateSpace).midY > 50) {
                Spacer()
                    .onAppear {
                        if self.refreshing == false {
                            self.onRefresh() ///call refresh once if pulled more than 50px
                        }
                    }
            } else if (geo.frame(in: self.coordinateSpace).maxY < 1) {
                Spacer()                    
            }
            
            ZStack(alignment: .center) {
                if self.refreshing { ///show loading if refresh called
                    ProgressView()
                } else { ///mimic static progress bar with filled bar to the drag percentage
                    ForEach(0..<8) { tick in
                        VStack {
                            Rectangle()
                                .fill(Color(UIColor.tertiaryLabel))
                                .opacity((Int((geo.frame(in: self.coordinateSpace).midY)/7) < tick) ? 0 : 1)
                                .frame(width: 3, height: 7)
                                .cornerRadius(3)
                            Spacer()
                        }.rotationEffect(Angle.degrees(Double(tick)/(8) * 360))
                    }.frame(width: 20, height: 20, alignment: .center)
                }
            }.frame(width: geo.size.width)
        }.padding(.top, -50)
    }
}
