//
//  OnboardingQuickActionsStep.swift
//  Safie
//
//  Created by Matyáš Procházka on 27.02.2022.
//

import SwiftUI

struct OnboardingQuickActionsStep: View {
    var next: (() -> Void)?
    var done: (() -> Void)?
    var previous: (() -> Void)?
    
    var body: some View {
        OnboardingLayout(
            title: self.title,
            description: self.descrption,
            preview: AnyView(self.preview),
            next: self.next,
            done: self.done,
            previous: self.previous
        )
    }
    
    var title = Text("onboarding.quick_actions.title")
    
    var descrption = Text("onboarding.quick_actions.description")
    
    var preview: some View {
        GeometryReader { geo in
            VStack(spacing: 0) {
                Spacer()
                
                HStack(spacing: 0) {
                    VStack(spacing: 0) {
                        ActivityIcon(icon: "bus")
                        
                        Text("onboarding.quick_actions.get_off_bus")
                            .body2TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.top, .normal)
                    }
                    .padding([.trailing, .leading], .small)
                    .frame(width: geo.size.width / 3)
                    
                    VStack(spacing: 0) {
                        ActivityIcon(icon: "person.3")
                        
                        Text("onboarding.quick_actions.group")
                            .body2TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.top, .normal)
                    }
                    .padding([.trailing, .leading], .small)
                    .frame(width: geo.size.width / 3)
                    
                    VStack(spacing: 0) {
                        ActivityIcon(icon: "checkmark")
                        
                        Text("onboarding.quick_actions.okay")
                            .body2TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.top, .normal)
                    }
                    .padding([.trailing, .leading], .small)
                    .frame(width: geo.size.width / 3)
                }
                .padding([.top, .bottom], .normal)
                .background(Color("BackgroundDark"))
                .cornerRadius(.normal)
                
                Spacer()
            }
        }
    }
}

struct OnboardingQuickActionsStep_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingQuickActionsStep()
    }
}
