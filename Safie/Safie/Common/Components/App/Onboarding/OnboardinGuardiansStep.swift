//
//  OnboardinGuardiansStep.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import SwiftUI

struct OnboardinGuardiansStep: View {
    var next: (() -> Void)? = nil
    var done: (() -> Void)? = nil
    var previous: (() -> Void)? = nil
    
    var body: some View {
        OnboardingLayout(
            title: self.title,
            description: self.description,
            preview: AnyView(self.preview),
            next: self.next,
            done: self.done,
            previous: self.previous
        )
    }
    
    var title = Text("Guardians")
    
    var description = Text("Your guardians will be notified when you happen to be in a critical situation.")
    
    var preview: some View {
        VStack {
            Text("Add your friends, family and close ones as your guardians. When something happens they will be notified about your situation and vice versa.")
                .body2LabelTextStyle()
                .alignContent(.leading)
        }
        .padding([.top, .bottom], .normal)
        .padding([.leading, .trailing], .large)
        .background(Color("BackgroundDark"))
        .cornerRadius(.normal)
    }
}

struct OnboardinGuardiansStep_Previews: PreviewProvider {
    static var previews: some View {
        OnboardinGuardiansStep()
    }
}
