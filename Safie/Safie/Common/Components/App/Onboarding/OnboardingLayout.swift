//
//  OnboardingLayout.swift
//  Safie
//
//  Created by Matyáš Procházka on 27.02.2022.
//

import SwiftUI

struct OnboardingLayout: View {
    var title: Text
    var description: Text
    var preview: AnyView
    
    var next: (() -> Void)?
    var done: (() -> Void)?
    var previous: (() -> Void)?
    
    var body: some View {
        VStack(spacing: 0) {                        
            self.title
                .h1TextStyle()
                .multilineTextAlignment(.center)
            
            Spacer()
            
            self.preview
            
            Spacer()
            
            self.description
                .bodyTextStyle()                
                .alignContent(.leading)
            
            HStack {
                if let previous = self.previous {
                    ActionButton(
                        title: Text("onboarding.previous"),
                        backgroundColor: Color("BackgroundDark"),
                        action: { previous() }
                    )
                    .frame(maxWidth: 150)
                }
                
                if let next = self.next {
                    ActionButton(
                        title: Text("onboarding.next"),
                        action: { next() }
                    )
                }
                
                if let done = self.done {
                    ActionButton(
                        title: Text("onboarding.done"),
                        action: { done() }
                    )
                }
            }
            .padding(.top, .extraLarge)
        }
        .padding([.leading, .trailing], .normal)
        .padding(.bottom, .small)
    }
}

//struct OnboardingLayout_Previews: PreviewProvider {
//    static var previews: some View {
//        OnboardingLayout()
//    }
//}
