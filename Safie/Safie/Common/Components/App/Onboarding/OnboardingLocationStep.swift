//
//  OnboardingLocationStep.swift
//  Safie
//
//  Created by Matyáš Procházka on 26.03.2022.
//

import SwiftUI

struct OnboardingLocationStep: View {
    var next: (() -> Void)? = nil
    var done: (() -> Void)? = nil
    var previous: (() -> Void)? = nil
    var askForLocationAuthorization: () -> Void
    
    var body: some View {
        OnboardingLayout(
            title: self.title,
            description: self.description,
            preview: AnyView(self.preview),
            next: self.next,
            done: self.done,
            previous: self.previous
        )
    }
    
    var title = Text("Location")
    
    var description = Text("Your location is tracked in critical situations so that your guardians know the information if something happens.")
    
    var preview: some View {
        VStack {
//            Text("onboarding.location.preview")
//                .body2LabelTextStyle()
//                .alignContent(.leading)
            
            ActionButton(title: Text("Grant permissions")) {
                self.askForLocationAuthorization()
            }
        }
        .padding([.top, .bottom], .normal)
        .padding([.leading, .trailing], .large)
        .background(Color("BackgroundDark"))
        .cornerRadius(.normal)
    }
}

//struct OnboardingLocationStep_Previews: PreviewProvider {
//    static var previews: some View {
//        OnboardingLocationStep()
//    }
//}
