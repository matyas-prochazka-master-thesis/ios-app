//
//  OnboardingAdviceStep.swift
//  Safie
//
//  Created by Matyáš Procházka on 27.02.2022.
//

import SwiftUI

struct OnboardingAdviceStep: View {
    var next: (() -> Void)?
    var done: (() -> Void)?
    var previous: (() -> Void)?
    
    var body: some View {
        OnboardingLayout(
            title: self.title,
            description: self.descrption,
            preview: AnyView(self.preview),
            next: self.next,
            done: self.done,
            previous: self.previous
        )
    }
    
    var title = Text("onboarding.advice.title")
    
    var descrption = Text("onboarding.advice.description")
    
    var preview: some View {
        VStack(spacing: 0) {
            HStack {
                ActivityIcon(icon: "bus")
                
                VStack(spacing: 0) {
                    Text("onboarding.advice.preview.title")
                        .h1TextStyle()
                        .alignContent(.leading)
                    
                    Text("onboarding.advice.preview.subtitle")
                        .body2LabelTextStyle()
                        .alignContent(.leading)
                        .padding(.top, .extraSmall)
                }
            }
            
            Text("onboarding.advice.preview.advices")
                .body2TextStyle()
                .alignContent(.leading)
                .padding(.top, .normal)
            
        }
        .padding([.top, .bottom], .normal)
        .padding([.leading, .trailing], .large)
        .background(Color("BackgroundDark"))
        .cornerRadius(.normal)
    }
}

struct OnboardingAdviceStep_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingAdviceStep()
    }
}
