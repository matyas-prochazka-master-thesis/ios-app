//
//  OnboardingSosStep.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import SwiftUI

struct OnboardingSosStep: View {
    var next: (() -> Void)? = nil
    var done: (() -> Void)? = nil
    var previous: (() -> Void)? = nil
    
    var body: some View {
        OnboardingLayout(
            title: self.title,
            description: self.description,
            preview: AnyView(self.preview),
            next: self.next,
            done: self.done,
            previous: self.previous
        )        
    }
    
    var title = Text("SOS Button")
    
    var description = Text("Press and hold the SOS button for 3 seconds to activate.")
    
    @ViewBuilder
    var preview: some View {
        VStack {
            VStack {
                Text("You can try the button now without contacting anyone:")
                    .body2LabelTextStyle()
                    .alignContent(.leading)
            }
            .padding([.top, .bottom], .normal)
            .padding([.leading, .trailing], .large)
            .background(Color("BackgroundDark"))
            .cornerRadius(.normal)
            
            SosButton(fakeApiCalls: true)
                .padding(.top, .normal)
        }
        .zIndex(1000)
    }
}

struct OnboardingSosStep_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingSosStep()
    }
}
