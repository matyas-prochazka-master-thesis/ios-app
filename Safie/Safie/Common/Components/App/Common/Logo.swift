//
//  Logo.swift
//  Safie
//
//  Created by Matyáš Procházka on 27.02.2022.
//

import SwiftUI

struct Logo: View {
    var body: some View {
        Image("safie")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .foregroundColor(.white)
    }
}

struct Logo_Previews: PreviewProvider {
    static var previews: some View {
        Logo()
    }
}
