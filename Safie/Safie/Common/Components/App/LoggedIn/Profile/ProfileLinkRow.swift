//
//  ProfileLinkRow.swift
//  Safie
//
//  Created by Matyáš Procházka on 23.03.2022.
//

import SwiftUI
import Kingfisher

struct ProfileLinkRow: View {
    var account: Account
    
    var body: some View {
        HStack {
            Group {
                if let profilePicture = account.profilePicture {
                    KFImage(URL(string: profilePicture.absoluteUrl)!)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                } else {
                    Circle()
                        .fill(.gray)
                }
            }
            .frame(width: 50, height: 50)
            .cornerRadius(25)
            .clipped()
            
            Text(account.username ?? account.email)
                .h2TextStyle()
                .foregroundColor(.white)
            
            Spacer()
        }
        .padding([.top, .bottom], .small)
    }
}

//struct ProfileLinkRow_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileLinkRow()
//    }
//}
