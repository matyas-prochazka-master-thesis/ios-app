//
//  PinEnter.swift
//  Safie
//
//  Created by Matyáš Procházka on 26.03.2022.
//

import SwiftUI

struct PinEnter: View {
    @StateObject var pinEnterViewModel: PinEnterViewModel
    
    var beforePin: AnyView
    var afterPin: AnyView
    
    enum Field: Hashable {
        case none
        case pin
    }
    
    @FocusState var focusedField: Field?
    
    init(
        success: @escaping () -> Void,
        beforePin: AnyView = AnyView(EmptyView()),
        afterPin: AnyView = AnyView(EmptyView())
    ) {
        self._pinEnterViewModel = StateObject(wrappedValue: PinEnterViewModel(success: success))
        
        self.beforePin = beforePin
        self.afterPin = afterPin
    }
    
    var body: some View {
        VStack {
            Spacer()
            
            Text("Enter your PIN")
                .h1TextStyle()
            
            self.beforePin

            HStack {
                PinLetter(letter: self.pinEnterViewModel.pin[0])
                PinLetter(letter: self.pinEnterViewModel.pin[1])
                PinLetter(letter: self.pinEnterViewModel.pin[2])
                PinLetter(letter: self.pinEnterViewModel.pin[3])
            }
            .onTapGesture {
                self.focusedField = .pin
            }
            .padding(.top, .normal)
            
            self.afterPin
            
            TextField("Pin", text: self.$pinEnterViewModel.pin)
                .focused($focusedField, equals: .pin)
                .keyboardType(.numberPad)
                .frame(height: 0)
                .opacity(0)
            
            if let error = self.pinEnterViewModel.pinError {
                FormError(error: Text(error))
            }
                    
            Spacer()
            
            ActionButton(
                title: Text("Check"),
                disabled: self.pinEnterViewModel.pin.count != 4
            ) {
                self.pinEnterViewModel.checkPin()
            }
            .animation(.default, value: self.pinEnterViewModel.pin)
        }        
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)) {
                self.focusedField = .pin
            }
        }
    }
}

//struct PinEnter_Previews: PreviewProvider {
//    static var previews: some View {
//        PinEnter()
//    }
//}
