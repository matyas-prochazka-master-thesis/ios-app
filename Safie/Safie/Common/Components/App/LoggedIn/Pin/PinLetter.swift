//
//  PinLetter.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import SwiftUI

struct PinLetter: View {
    var letter: String
    
    var body: some View {
        VStack {
            Text(self.letter)
                .h1TextStyle()
                .foregroundColor(.white)
        }
        .frame(width: 50, height: 50, alignment: .center)
        .overlay(
            RoundedRectangle(cornerRadius: RadiusSize.extraSmall.rawValue)
                .stroke(Color.gray, lineWidth: 2)
        )
    }
}

struct PinLetter_Previews: PreviewProvider {
    static var previews: some View {
        PinLetter(letter: "1")
    }
}
