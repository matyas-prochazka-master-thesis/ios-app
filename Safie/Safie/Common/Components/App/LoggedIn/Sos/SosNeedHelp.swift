//
//  SosNeedHelp.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import SwiftUI
import Kingfisher

struct SosNeedHelp: View {
    var soses: [Sos]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(self.soses) { sos in
                    let account = sos.account!
                    
                    NavigationLink(destination: SosDetailScreen(sosId: sos.id, sos: sos)) {
                        ZStack(alignment: .topTrailing) {
                            VStack(alignment: .leading) {
                                switch sos.state {
                                case .pinEntered:
                                    Text("PIN entered")
                                        .bodyTextStyle()
                                        .foregroundColor(Color("Success"))
                                case .emergency:
                                    Text("EMERGENCY")
                                        .bodyTextStyle()
                                        .foregroundColor(Color("Danger"))
                                case .released:
                                    Text("Released")
                                        .bodyTextStyle()
                                        .foregroundColor(Color("Warning"))
                                case .holding:
                                    Text("Holding")
                                        .bodyTextStyle()
                                        .foregroundColor(Color("Warning"))
                                default:
                                    Text("Unknown state")
                                        .bodyTextStyle()
                                        .foregroundColor(Color("Warning"))
                                }
                            
                                HStack(alignment: .center) {
                                    Group {
                                        if let profilePicture = account.profilePicture {
                                            KFImage(URL(string: profilePicture.absoluteUrl)!)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                        } else {
                                            Circle()
                                                .fill(.gray)
                                        }
                                    }
                                    .frame(width: 50, height: 50)
                                    .cornerRadius(25)
                                    .clipped()
                                    
                                    VStack(alignment: .leading) {
                                        Text(account.username ?? account.email)
                                            .body2TextStyle()
                                            .foregroundColor(.white)
                                        
                                        Text(DateUtil.shared.relativeWithTime(date: sos.createdAt)!)
                                            .body2LabelTextStyle()
                                    }
                                }
                            }
                            .padding(.all, .normal)
                            .background(Color("DarkGray"))
                            .cornerRadius(.small)
                                                        
                            if sos.recent {
                                Text("RECENT")
                                    .body2TextStyle()
                                    .foregroundColor(.white)
                                    .padding(.extraSmall)
                                    .background(
                                        RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                            .fill(Color("Danger"))
                                    )
                            }
                        }
                    }
                }
            }
        }
    }
}

//struct SosNeedHelp_Previews: PreviewProvider {
//    static var previews: some View {
//        SosNeedHelp()
//    }
//}
