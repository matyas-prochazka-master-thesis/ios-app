//
//  SosButton.swift
//  Safie
//
//  Created by Matyáš Procházka on 25.03.2022.
//

import SwiftUI

struct SosButton: View {
    @StateObject var sosButtonViewModel: SosButtonViewModel = SosButtonViewModel()
    
    var fakeApiCalls: Bool = false
    
    @State private var showSosScreen: Bool = false
    @State private var showPinEnter: Bool = false
    @State private var showEmergencyContacted: Bool = false
    
    @State private var workItems: [DispatchWorkItem] = []
    @State private var text = "SOS"
    @State private var size: CGFloat = 125
    
    @State private var releasedAt: Date = Date()
    
    private let timer = Timer.publish(every: 0.5, on: .main, in: .common).autoconnect()
    @State private var secondsRemain: Int = 10
    
    @GestureState private var pressingState = false
    
    var body: some View {
        ZStack(alignment: .center) {
            Circle()
                .fill(RadialGradient(colors: [.red, .red.opacity(0.5)], center: .center, startRadius: 75, endRadius: self.size))
                .frame(width: self.size, height: self.size)
            
            Text(self.text)
                .bodyTextStyle()
        }
        .zIndex(1000)
        .frame(width: 125, height: 125)
        .gesture(self.pressingGesture)
        .onChange(of: self.pressingState) { value in
            if value {
                self.start()
            } else {
                self.stop()
            }
        }
//        .gesture(
//            DragGesture(minimumDistance: 0)
//                .onChanged { gesture in
//                    self.start()
//
//                    //print(gesture)
//                }
//                .onEnded { _ in
//                    print("STOP")
//
//                    self.stop()
//                }
//                .updating($dragOffset) { value, state, transaction in
//                    print(value, state, transaction)
//
//                    state = value.translation
//                }
//        )
        .fullScreenCover(isPresented: $showSosScreen) {
            GeometryReader { _ in
                VStack {
                    if self.showEmergencyContacted {
                       Text("GUARDIANS WERE ALERTED ABOUT YOUR SITUATION")
                           .h1TextStyle()
                           .foregroundColor(Color("Danger"))
                        
                        Text("Close")
                            .bodyActionTextStyle()
                            .padding(.top, .large)
                            .onTapGesture {
                                self.showSosScreen = false
                                self.showPinEnter = false
                                self.showEmergencyContacted = false
                                self.secondsRemain = 10
                                
                                self.sosButtonViewModel.clean()
                            }
                   } else if self.showPinEnter {
                        PinEnter(
                            success: {
                                if !self.fakeApiCalls {
                                    self.sosButtonViewModel.pinEntered()
                                }
                                                                                                    
                                self.showSosScreen = false
                                self.showPinEnter = false
                                
                                self.sosButtonViewModel.clean()
                            },
                            afterPin: AnyView(
                                VStack(spacing: 0) {
                                    Text("You have \(self.secondsRemain) seconds to enter your pin")
                                        .bodyLabelTextStyle()
                                        .padding(.top, .small)
                                    
                                    Text("After that your guardians will be alerted")
                                        .body2LabelTextStyle()
                                        .padding(.top, .extraSmall)
                                }
                            )
                        )
                        .onReceive(self.timer) { _ in
                            self.secondsRemain = Int(self.releasedAt.timeIntervalSinceNow)
                            
                            if self.secondsRemain <= 0 {
                                self.showEmergencyContacted = true
                            }
                        }
                    } else {
                        Text("HOLD SCREEN UNTIL YOU'RE SAFE")
                            .h1TextStyle()
                            .foregroundColor(Color("Danger"))
                    }
                }
                .padding(.all, .normal)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
            .background(.black)
        }
    }
    
    var pressingGesture: some Gesture {
        LongPressGesture(minimumDuration: 0.1)
            .sequenced(before: DragGesture(minimumDistance: 0, coordinateSpace: .local))
            .updating($pressingState) { value, state, transaction in
                switch value {
                    case .second(true, nil):
                        state = true
                    default:
                        break
                }
            }
        }
    
    private func start() -> Void {
        if self.workItems.isEmpty {
            let workItemShowModal = DispatchWorkItem {
                self.showSosScreen = true
                hhaptic()
                
                if !self.fakeApiCalls {
                    self.sosButtonViewModel.buttonPressed()
                }
            }
            let workItemShow3 = DispatchWorkItem {
                withAnimation(.linear(duration: 0.3)) {
                    self.text = "3 seconds"
                    self.size = max(200, UIScreen.main.bounds.size.height / 4)
                    hhaptic()
                }
            }
            let workItemShow2 = DispatchWorkItem {
                withAnimation(.linear(duration: 0.3)) {
                    self.text = "2 seconds"
                    self.size = max(250, UIScreen.main.bounds.size.height / 2)
                    hhaptic()
                }
            }
            let workItemShow1 = DispatchWorkItem {
                withAnimation(.linear(duration: 0.3)) {
                    self.text = "1 second"
                    self.size = max(300, UIScreen.main.bounds.size.height / 0.8)
                    hhaptic()
                }
            }
            
            self.workItems = [workItemShowModal, workItemShow3, workItemShow2, workItemShow1]
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: workItemShow3)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: workItemShow2)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: workItemShow1)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: workItemShowModal)
        }
    }
    
    private func stop() -> Void {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            self.workItems.forEach { workItem in
                workItem.cancel()
            }
            
            self.workItems = []
            self.text = "SOS"
            self.size = 125
            
            if self.showSosScreen {
                self.showPinEnter = true
                
                if !self.fakeApiCalls {
                    self.sosButtonViewModel.buttonReleased()
                }
                
                self.releasedAt = Date().addingTimeInterval(TimeInterval(10))
            }
        }
    }
}

struct SosButton_Previews: PreviewProvider {
    static var previews: some View {
        SosButton()
    }
}
