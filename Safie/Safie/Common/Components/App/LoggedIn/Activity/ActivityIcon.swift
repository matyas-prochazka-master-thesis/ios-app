//
//  ActivityIcon.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.03.2022.
//

import SwiftUI

struct ActivityIcon: View {
    var icon: String
    
    var body: some View {
        VStack {
            Image(systemName: self.icon)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .foregroundColor(Color("Label"))
                .frame(width: 25, height: 25)
        }
        .padding(.normal)
        .background(
            Circle()
                .fill(Color("Background"))
        )
    }
}

struct ActivityIcon_Previews: PreviewProvider {
    static var previews: some View {
        ActivityIcon(icon: "bus")
    }
}
