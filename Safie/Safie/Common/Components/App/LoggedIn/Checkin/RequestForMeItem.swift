//
//  RequestForMeItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import SwiftUI
import Kingfisher

struct RequestForMeItem: View {
    var request: CheckinRequest
    
    var body: some View {
        HStack {
            Group {
                if let profilePicture = self.request.requester!.profilePicture {
                    KFImage(URL(string: profilePicture.absoluteUrl)!)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                } else {
                    Circle()
                        .fill(Color("Label"))
                }
            }
            .frame(width: 50, height: 50)
            .cornerRadius(25)
            .clipped()
            
            VStack(alignment: .leading) {
                Text("Request from \(formatDisplayName(self.request.requester!))")
                    .body2TextStyle()
                
                Text("\(DateUtil.shared.relativeWithTime(date: self.request.createdAt)!)")
                    .body2LabelTextStyle()
                
                switch self.request.type {
                case .oneTime:
                    Text("One time")
                        .body2TextStyle()
                case .periodical:
                    Text("Periodical")
                        .body2TextStyle()
                default:
                    EmptyView()
                }
            }
            .foregroundColor(.white)
                        
            Spacer()
            
            switch self.request.state {
            case .requested:
                ZStack {
                    Text("In Progress")
                        .foregroundColor(Color("Warning"))
//                    Circle()
//                        .fill(Color("Warning"))
//                        .frame(width: 40, height: 40)
//
//                    Image(systemName: "questionmark")
//                        .resizable()
//                        .aspectRatio(contentMode: .fit)
//                        .frame(width: 20, height: 20)
//                        .foregroundColor(.white)
                }
            case .ended:
                ZStack {
                    Text("Checked")
                        .foregroundColor(Color("Success"))
//                    Circle()
//                        .fill(Color("Success"))
//                        .frame(width: 40, height: 40)
//
//                    Image(systemName: "checkmark")
//                        .resizable()
//                        .aspectRatio(contentMode: .fit)
//                        .frame(width: 20, height: 20)
//                        .foregroundColor(.white)
                }
            default:
                EmptyView()
            }
        }
        .padding(.all, .normal)
        .background(Color("DarkGray"))
        .cornerRadius(.small)
    }
}

//struct RequestForMeItem_Previews: PreviewProvider {
//    static var previews: some View {
//        RequestForMeItem()
//    }
//}
