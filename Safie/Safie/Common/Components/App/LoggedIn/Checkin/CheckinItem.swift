//
//  CheckinItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 05.04.2022.
//

import SwiftUI

struct CheckinItem: View {
    var checkin: Checkin
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Created at \(DateUtil.shared.relativeWithTime(date: self.checkin.createdAt)!)")
                    .body2LabelTextStyle()
                
                if let checkedAt = self.checkin.checkedAt {
                    Text("Checked at \(DateUtil.shared.relativeWithTime(date: checkedAt)!)")
                        .body2LabelTextStyle()
                }
            }
            
            Spacer()
            
            switch self.checkin.state {
            case .requested:
                Text("Requested")
                    .foregroundColor(Color("Warning"))
            case .checked:
                Text("Checked")
                    .foregroundColor(Color("Success"))
            default:
                EmptyView()
            }
        }
        .padding(.all, .normal)
        .background(Color("DarkGray"))
        .cornerRadius(.small)
    }
}

//struct CheckinItem_Previews: PreviewProvider {
//    static var previews: some View {
//        CheckinItem()
//    }
//}
