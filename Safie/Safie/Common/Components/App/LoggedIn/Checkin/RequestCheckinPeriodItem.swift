//
//  RequestCheckinPeriodItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import SwiftUI

struct RequestCheckinPeriodItem: View {
    var minutes: Int    
    var active: Bool
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .fill(self.active ? Color("PrimaryAction") : Color("DarkGray"))
                
                Text("\(self.minutes)m")
                    .body2TextStyle()
            }
            .frame(width: 50, height: 50)
        }
    }
}
//
//struct RequestCheckinPeriodItem_Previews: PreviewProvider {
//    static var previews: some View {
//        RequestCheckinPeriodItem()
//    }
//}
