//
//  UncheckedCheckinItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import SwiftUI
import Kingfisher

struct UncheckedCheckinItem: View {
    var checkin: Checkin
    var checkCheckin: () -> Void
    var checking: Bool
    
    var body: some View {
        HStack {
            Group {
                if let profilePicture = self.checkin.request!.requester!.profilePicture {
                    KFImage(URL(string: profilePicture.absoluteUrl)!)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                } else {
                    Circle()
                        .fill(Color("Label"))
                }
            }
            .frame(width: 50, height: 50)
            .cornerRadius(25)
            .clipped()
            
            VStack(alignment: .leading) {
                Text("\(formatDisplayName(self.checkin.request!.requester!)) asks if you are okay")
                    .body2TextStyle()
                
                Text("\(DateUtil.shared.relativeWithTime(date: self.checkin.createdAt)!)")
                    .body2LabelTextStyle()
            }
            
            Button(action: {
                self.checkCheckin()
            }) {
                ZStack {
                    Circle()
                        .fill(Color("Success"))
                        .frame(width: 40, height: 40)
                    
                    if self.checking {
                        ProgressView()
                    } else {
                        Image(systemName: "checkmark")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 20, height: 20)
                            .foregroundColor(.white)
                    }
                }
            }
        }
        .padding(.all, .normal)
        .background(Color("DarkGray"))
        .cornerRadius(.small)
    }
}

//struct UncheckedCheckinItem_Previews: PreviewProvider {
//    static var previews: some View {
//        UncheckedCheckinItem()
//    }
//}
