//
//  TabMenuItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import SwiftUI

struct TabMenuItem: View {
    var label: Text
    var icon: String
    var badge: Int?
    var backgroundColor: Color?
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .fill(self.backgroundColor ?? Color("DarkGray"))
                
                Image(systemName: self.icon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 20, height: 20)
                    .foregroundColor(.white)
                
                if let badge = self.badge {
                    ZStack(alignment: .topTrailing) {
                        VStack {
                        }
                        .frame(width: 50, height: 50)
                        
                        ZStack {
                            Circle()
                                .fill(badge == 0 ? Color("Success") : Color("Danger"))
                            
                            Text("\(badge)")
                                .body2TextStyle()
                                .foregroundColor(.white)
                        }
                        .frame(width: 20, height: 20)
                    }
                }
            }
            .frame(width: 50, height: 50)
            
            self.label
                .body2LabelTextStyle()
        }
    }
}

//struct TabMenuItem_Previews: PreviewProvider {
//    static var previews: some View {
//        TabMenuItem()
//    }
//}
