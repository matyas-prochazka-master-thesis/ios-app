//
//  AccountAvatar.swift
//  Safie
//
//  Created by Matyáš Procházka on 05.04.2022.
//

import SwiftUI
import Kingfisher

struct AccountAvatar: View {
    var account: Account
    var size: CGFloat = 50
    
    var body: some View {
        Group {
            if let profilePicture = account.profilePicture {
                KFImage(URL(string: profilePicture.absoluteUrl)!)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
            } else {
                ZStack {
                    Circle()
                        .fill(.gray)
                    
                    Text(formatDisplayName(self.account)[0..<1])
                        .foregroundColor(.white)
                }
            }
        }
        .frame(width: self.size, height: self.size)
        .cornerRadius(self.size / 2)
        .clipped()
    }
}

//struct AccountAvatar_Previews: PreviewProvider {
//    static var previews: some View {
//        AccountAvatar()
//    }
//}
