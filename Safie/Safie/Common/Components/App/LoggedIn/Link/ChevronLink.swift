//
//  ProfileOtherLink.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import SwiftUI

struct ChevronLink: View {
    var label: Text
    var icon: String?
    
    var body: some View {
        HStack(alignment: .center) {
            if let icon = self.icon {
                Image(systemName: icon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 18, height: 18)
            }
            
            self.label
                .bodyTextStyle()
            
            Spacer()
            
            Image(systemName: "chevron.right")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 10, height: 10)
                .foregroundColor(Color("DarkishGray"))
        }
        .foregroundColor(.white)
        .padding(.all, .normal)
        .background(Color("DarkGray"))
        .cornerRadius(.small)
    }
}

//struct ProfileOtherLink_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileOtherLink()
//    }
//}
