//
//  GuardianAddHorizontalListItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import SwiftUI

struct GuardianAddHorizontalListItem: View {
    var body: some View {
        VStack {
            Group {
                ZStack {
                    Circle()
                        .fill(.gray)
                    
                    Image(systemName: "plus")
                        .frame(width: 30, height: 30)
                        .foregroundColor(.white)
                }
            }
            .frame(width: 80, height: 80)
            .cornerRadius(40)
            .clipped()
            
            Text("Add guardian")
                .body2TextStyle()
                .foregroundColor(.white)
        }
    }
}

struct GuardianAddHorizontalListItem_Previews: PreviewProvider {
    static var previews: some View {
        GuardianAddHorizontalListItem()
    }
}
