//
//  GuardianAddListItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 23.03.2022.
//

import SwiftUI

struct GuardianListLinkItem: View {
    var icon: String
    var label: Text
    
    var body: some View {
        HStack(alignment: .center) {
            Image(systemName: self.icon)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 18, height: 18)
            
            self.label
                .bodyTextStyle()
        }
        .padding([.top, .bottom], .small)
    }
}

//struct GuardianAddListItem_Previews: PreviewProvider {
//    static var previews: some View {
//        GuardianAddListItem()
//    }
//}
