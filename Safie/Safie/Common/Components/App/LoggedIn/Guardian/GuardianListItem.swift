//
//  GuardianListItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 23.03.2022.
//

import SwiftUI
import Kingfisher

struct GuardianListItem: View {
    var guardian: Guardian
    
    var body: some View {
        //NavigationLink(destination: GuardianScreen(guardianId: guardian.id, guardian: guardian)) {
            HStack {
                if let guardian = self.guardian.guardian {
                    Group {
                        if let profilePicture = guardian.profilePicture {
                            KFImage(URL(string: profilePicture.absoluteUrl)!)
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                        } else {
                            Circle()
                                .fill(.gray)
                        }
                    }
                    .frame(width: 50, height: 50)
                    .cornerRadius(25)
                    .clipped()
                    
                    Text(guardian.username ?? guardian.email)
                        .h2TextStyle()
                }
            }
            .padding([.top, .bottom], .small)
        //}
    }
}

//struct GuardianListItem_Previews: PreviewProvider {
//    static var previews: some View {
//        GuardianListItem()
//    }
//}
