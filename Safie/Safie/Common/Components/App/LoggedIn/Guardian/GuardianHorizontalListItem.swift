//
//  GuardianHorizontalListItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import SwiftUI
import Kingfisher

struct GuardianHorizontalListItem: View {
    var guardian: Guardian
    
    var body: some View {
        VStack {
            if let guardian = self.guardian.guardian {
                Group {
                    if let profilePicture = guardian.profilePicture {
                        KFImage(URL(string: profilePicture.absoluteUrl)!)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                    } else {
                        Circle()
                            .fill(.gray)
                    }
                }
                .frame(width: 80, height: 80)
                .cornerRadius(40)
                .clipped()
                
                Text(guardian.username ?? guardian.email)
                    .body2TextStyle()
                    .foregroundColor(.white)
            }
        }        
    }
}

//struct GuardianHorizontalListItem_Previews: PreviewProvider {
//    static var previews: some View {
//        GuardianHorizontalListItem()
//    }
//}
