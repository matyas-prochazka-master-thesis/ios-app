//
//  TripActivityItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import SwiftUI

struct TripActivityItem: View {
    var tripActivity: TripActivity    
    
    var icon: String
    var label: String
    
    init(tripActivity: TripActivity) {
        self.tripActivity = tripActivity
        
        switch tripActivity.key {
        case .quickAction:
            self.icon = tripActivity.payloadQuickAction?.icon ?? "questionmark"
            self.label = tripActivity.payloadQuickAction?.name ?? "Unknown"
        case .message:
            self.icon = "message"
            self.label = "Message"
        case .photo:
            self.icon = "photo"
            self.label = "Photo"
        default:
            self.icon = "questionmark"
            self.label = "Unknown"
        }
    }
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .fill(Color("DarkGray"))
                                            
                Image(systemName: self.icon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 30)
                    .foregroundColor(.white)
            }
            .frame(width: 60, height: 60)
            
            Text(self.label)
                .body2TextStyle()
        }
    }
}

//struct TripActivityItem_Previews: PreviewProvider {
//    static var previews: some View {
//        TripActivityItem()
//    }
//}
