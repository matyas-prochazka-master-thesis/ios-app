//
//  MyTripHistoryItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 10.04.2022.
//

import SwiftUI

struct MyTripHistoryItem: View {
    var trip: Trip
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                if let destination = trip.destination {
                    Text("Going to **\(destination.name)**")
                        .body2TextStyle()
                }
                
                if let shouldArriveAt = trip.shouldArriveAt {
                    Text("Should arrive **\(DateUtil.shared.relativeWithTime(date: shouldArriveAt)!)**")
                        .body2TextStyle()
                }
                
                Text("Created \(DateUtil.shared.relativeWithTime(date: self.trip.createdAt)!)")
                    .body2LabelTextStyle()
                
                switch self.trip.state {
                case .started:
                    Text("Started")
                        .body2TextStyle()
                        .foregroundColor(Color("Warning"))
                case .finished:
                    Text("Finished")
                        .body2TextStyle()
                        .foregroundColor(Color("Success"))
                default:
                    EmptyView()
                }
            }
            .foregroundColor(.white)
            
            Spacer()
            
            Image(systemName: "chevron.right")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 10, height: 10)
                .foregroundColor(Color("DarkishGray"))
        }        
        .padding(.all, .normal)
        .background(Color("DarkGray"))
        .cornerRadius(.small)
    }
}

//struct MyTripHistoryItem_Previews: PreviewProvider {
//    static var previews: some View {
//        MyTripHistoryItem()
//    }
//}
