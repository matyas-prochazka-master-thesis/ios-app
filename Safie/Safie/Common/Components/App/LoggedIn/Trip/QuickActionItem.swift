//
//  QuickActionItem.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import SwiftUI

struct QuickActionItem: View {
    var quickAction: QuickAction
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .fill(Color("DarkGray"))
                                            
                Image(systemName: self.quickAction.icon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 30)
                    .foregroundColor(.white)
            }
            .frame(width: 60, height: 60)            
            
            Text(self.quickAction.name)
                .h4TextStyle()
        }
    }
}

//struct QuickActionItem_Previews: PreviewProvider {
//    static var previews: some View {
//        QuickActionItem()
//    }
//}
