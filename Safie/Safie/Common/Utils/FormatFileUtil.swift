//
//  FormatFileUtil.swift
//  Safie
//
//  Created by Matyáš Procházka on 10.08.2021.
//

import Foundation

func formatPath(_ file: FileBasicInfo) -> String {
    return "\(AppConfig.staticUrl)\(file.path)"
}
