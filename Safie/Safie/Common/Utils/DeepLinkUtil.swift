//
//  DeppLinkUtil.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.10.2021.
//

import Foundation
import SwiftUI
import Firebase

class DeepLinkUtil {
    enum DeepLink: Equatable {
        case meal(mealId: String)
        case passwordReset(code: String)
        
        func toString() -> String {
            switch self {
            case .meal(let mealId):
                return "mealId:\(mealId)"
            case .passwordReset(let code):
                return "code:\(code)"
            }
        }
    }
    
    static func handleDynamicLink(_ dynamicLink: DynamicLink) -> DeepLink? {
        guard let url = dynamicLink.url else { return nil }

        guard
            dynamicLink.matchType == .unique ||
            dynamicLink.matchType == .default
        else {
            return nil
        }

        guard let deepLink = DeepLinkUtil.parseComponents(from: url) else {
            return nil
        }
        
        return deepLink
    }

    static func parseComponents(from url: URL) -> DeepLink? {
        guard url.scheme == "https" else {
            return nil
        }
        
        guard let query = url.query else {
            return nil
        }
        
        let components = query.split(separator: ",").flatMap {
            $0.split(separator: "=")
        }
        
//        if url.pathComponents.contains("open-meal") {
//            guard let idIndex = components.firstIndex(of: Substring("mealId")) else {
//                return nil
//            }
//
//            guard idIndex + 1 < components.count else {
//                return nil
//            }
//
//            return .meal(mealId: String(components[idIndex + 1]))
//        } else if url.pathComponents.contains("open-password-reset") {
//            guard let codeIndex = components.firstIndex(of: Substring("code")) else {
//                return nil
//            }
//
//            guard codeIndex + 1 < components.count else {
//                return nil
//            }
//
//            return .passwordReset(code: String(components[codeIndex + 1]))
//        }
        
        return nil
    }
}


struct DeepLinkKey: EnvironmentKey {
    static var defaultValue: DeepLinkUtil.DeepLink? {
        return nil
    }
}

extension EnvironmentValues {
    var deepLink: DeepLinkUtil.DeepLink? {
        get {
            self[DeepLinkKey.self]
        }
        set {
            self[DeepLinkKey.self] = newValue
        }
    }
}

struct ClearDeepLinkKey: EnvironmentKey {
    static var defaultValue: () -> Void {
        return {}
    }
}

extension EnvironmentValues {
    var clearDeepLink: () -> Void {
        get {
            self[ClearDeepLinkKey.self]
        }
        set {
            self[ClearDeepLinkKey.self] = newValue
        }
    }
}
