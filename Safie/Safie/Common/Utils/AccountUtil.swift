//
//  AccountUtil.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.04.2022.
//

import Foundation

func formatDisplayName(_ account: Account) -> String {
    return account.username ?? account.email
}
