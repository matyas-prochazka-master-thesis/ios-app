//
//  DateFormatUtil.swift
//  Safie
//
//  Created by Matyáš Procházka on 12.08.2021.
//

import Foundation

final class DateUtil {
    static let shared = DateUtil()
    
    let iso8601Formatter = ISO8601DateFormatter()
    let relativeFormatter = DateFormatter()
    let relativeWithTimeFormatter = DateFormatter()
    let timeFormatter = DateFormatter()
    let dateTimeFormatter = DateFormatter()
    
    init() {
        self.iso8601Formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        
        self.relativeFormatter.timeStyle = .none
        self.relativeFormatter.dateStyle = .medium
        //self.relativeFormatter.locale = Locale(identifier: "en_GB")
        self.relativeFormatter.doesRelativeDateFormatting = true
        self.relativeFormatter.timeZone = .current
        
        self.timeFormatter.timeStyle = .short
        self.timeFormatter.dateStyle = .none
        //self.timeFormatter.locale = Locale(identifier: "cs_CZ")
        self.timeFormatter.timeZone = .current
        
        self.dateTimeFormatter.timeStyle = .medium
        self.dateTimeFormatter.dateStyle = .medium
        //self.dateTimeFormatter.locale = Locale(identifier: "cs_CZ")
        self.dateTimeFormatter.timeZone = .current
    }
    
    func format(date: String) -> Date? {
        iso8601Formatter.date(from: date)
    }
    
    // Today, yesterday...
    func relative(date: Date) -> String? {
        return self.relativeFormatter.string(from: date)
    }
    
    // Today 10:00
    func relativeWithTime(date: Date) -> String? {
        return "\(self.relativeFormatter.string(from: date)) \(self.timeFormatter.string(from: date))"
    }
    
    // 10:00
    func time(date: Date) -> String? {
        return self.timeFormatter.string(from: date)
    }
    
    // 25. 4. 10:00
    func dateTime(date: Date) -> String? {
        return self.dateTimeFormatter.string(from: date)
    }
    
    func toISO8601(date: Date) -> String? {
        return self.iso8601Formatter.string(from: date)
    }
}


extension ISO8601DateFormatter {
    func formatToDefault(_ formattedDate: String) -> Date {
        // formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        
        return date(from: formattedDate)!
    }
}
