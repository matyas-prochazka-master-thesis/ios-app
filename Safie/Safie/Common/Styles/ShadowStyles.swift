//
//  ShadowStyles.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2021.
//

import SwiftUI

extension View {
    func shadow(_ size: ShadowSize) -> some View {
        self.shadow(radius: size.rawValue)
    }
    
    func shadow(_ color: Color, _ size: ShadowSize) -> some View {
        self.shadow(color: color, radius: size.rawValue)
    }
}
