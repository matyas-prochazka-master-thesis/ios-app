//
//  RadiusStyles.swift
//  Safie
//
//  Created by Matyáš Procházka on 25.03.2021.
//

import SwiftUI

extension View {
    func cornerRadius(_ size: RadiusSize) -> some View {
        self.cornerRadius(size.rawValue)
    }
}
