//
//  BodyTextStyle.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2021.
//

import SwiftUI

struct BasicTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(.small)
            .foregroundColor(.white)
            .background(Color("DarkGray"))            
            .cornerRadius(.small)
    }
}
