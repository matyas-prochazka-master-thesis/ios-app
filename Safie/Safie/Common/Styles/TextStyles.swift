//
//  BodyTextStyle.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2021.
//

import SwiftUI

extension View {
    func font(_ weight: Font.Weight, _ size: FontSize) -> some View {
        self
            .font(.system(size: size.rawValue, weight: weight, design: .default))
    }
    
    func h1TextStyle() -> some View {
        self
            .font(.semibold, .extraLarge)
    }
    
    func h2TextStyle() -> some View {
        self
            .font(.semibold, .large)
    }
    
    func h3TextStyle() -> some View {
        self
            .font(.semibold, .normal)
    }
    
    func h4TextStyle() -> some View {
        self
            .font(.semibold, .small)
    }
    
    func bodyLargeTextStyle() -> some View {
        self
            .font(.regular, .large)
    }
    
    func bodyTextStyle() -> some View {
        self
            .font(.medium, .normal)
    }
    
    func bodyActionTextStyle() -> some View {
        self
            .bodyTextStyle()
            .foregroundColor(Color("PrimaryAction"))
    }
    
    func bodyDangerTextStyle() -> some View {
        self
            .bodyTextStyle()
            .foregroundColor(Color("Danger"))
    }
    
    func bodyLabelTextStyle() -> some View {
        self
            .bodyTextStyle()
            .foregroundColor(Color("Label"))
    }
    
    func body2TextStyle() -> some View {
        self
            .font(.regular, .small)
    }
    
    func body2DangerTextStyle() -> some View {
        self
            .body2TextStyle()
            .foregroundColor(Color("Danger"))
    }
    
    func body2LabelTextStyle() -> some View {
        self
            .body2TextStyle()
            .foregroundColor(Color("Label"))
    }
    
    func buttonTextStyle() -> some View {
        self
            .font(.semibold, .normal)
    }
    
    func alignContent(_ alignment: Alignment) -> some View {
        self
            .frame(maxWidth: .infinity, alignment: alignment)
    }
}
