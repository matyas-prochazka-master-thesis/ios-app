//
//  PaddingStyles.swift
//  Safie
//
//  Created by Matyáš Procházka on 25.03.2021.
//

import SwiftUI

extension View {
    func padding(_ edges: Edge.Set = .all, _ size: PaddingSize) -> some View {
        self.padding(edges, size.rawValue)
    }
    
    func padding(_ size: PaddingSize) -> some View {
        self.padding(size.rawValue)
    }
}
