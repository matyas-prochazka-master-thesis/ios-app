//
//  Sizes.swift
//  Safie
//
//  Created by Matyáš Procházka on 25.03.2021.
//

import SwiftUI

enum PaddingSize: CGFloat {
    case extraSmall = 5
    case small = 10
    case normal = 20
    case large = 40
    case extraLarge = 60
}

enum FontSize: CGFloat {
    case extraSmall = 12
    case small = 14
    case normal = 18
    case large = 22
    case extraLarge = 28
}

enum FontWeight: String {
    case black = "Black"
    case extraBold = "ExtraBold"
    case bold = "Bold"
    case semiBold = "SemiBold"
    case regular = "Regular"
    case light = "Light"
    case extraLight = "ExtraLigh"
}

enum RadiusSize: CGFloat {
    case extraSmall = 8
    case small = 16
    case normal = 24
    case large = 32
    case extraLarge = 40
}

enum ShadowSize: CGFloat {
    case extraSmall = 2
    case small = 4
    case normal = 8
    case large = 16
    case extraLarge = 20
}
