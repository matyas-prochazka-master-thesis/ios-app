//
//  AppDelegate.swift
//  Safie
//
//  Created by Matyáš Procházka on 17.03.2022.
//

import Foundation
import FacebookCore
import Firebase
import FirebaseMessaging
import FirebaseAnalytics

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {
        // Firebase configuration
        FirebaseApp.configure()
        FirebaseConfiguration.shared.setLoggerLevel(.min)

        UNUserNotificationCenter.current().delegate = self
        application.registerForRemoteNotifications()
        
        UIDevice.current.isBatteryMonitoringEnabled = true

        Messaging.messaging().delegate = self
        
        // Facebook Sign In
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )

        return true
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    // Notification display
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler:
        @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        process(notification)
        completionHandler([[.banner, .sound]])
    }

    // Notification tap
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        processAndOpen(response.notification)
        completionHandler()
    }

    // Register APN token
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
        Messaging.messaging().apnsToken = deviceToken
    }

    // Process notifications
    private func process(_ notification: UNNotification) {
        let userInfo = notification.request.content.userInfo
        UIApplication.shared.applicationIconBadgeNumber = 0
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
    
    private func processAndOpen(_ notification: UNNotification) {
        let userInfo = notification.request.content.userInfo
        UIApplication.shared.applicationIconBadgeNumber = 0
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Open specific screen
        let notificationOpenModel = DI.shared.container.resolve(NotificationOpenModel.self)!
        notificationOpenModel.processUserInfo(userInfo: userInfo)
    }
}

extension AppDelegate: MessagingDelegate {
    // Register Firebase Token
    func messaging(
        _ messaging: Messaging,
        didReceiveRegistrationToken fcmToken: String?
    ) {        
        let tokenDict = ["token": fcmToken ?? ""]
        
        NotificationCenter.default.post(
            name: Notification.Name("FCMToken"),
            object: nil,
            userInfo: tokenDict
        )
        
        guard let token = fcmToken else { return }
        
        let notificationModel = DI.shared.container.resolve(NotificationModel.self)!        
        notificationModel.saveDeviceToken(deviceToken: token)
    }
}

