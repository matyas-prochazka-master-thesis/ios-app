//
//  NotificationModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 17.03.2022.
//

import Foundation
import UserNotifications

protocol NotificationModelProtocol: ObservableObject {
    var notificationAuthorized: Bool { get set }
    
    func saveDeviceToken(deviceToken: String) -> Void
    
    func sendDeviceTokenToServer() -> Void
}

final class NotificationModel: NotificationModelProtocol {
    @Published var notificationAuthorized: Bool = false
    
    private var deviceToken: String?
    private var tryOnSave: Bool = false
    
    init() {
        self.checkAuthorizationStatus()
    }
    
    // Save Firebase token
    func saveDeviceToken(deviceToken: String) -> Void {
        self.deviceToken = deviceToken
        
        if self.tryOnSave {
            self.sendDeviceTokenToServer()
        }
    }
    
    func sendDeviceTokenToServer() -> Void {
        guard let deviceToken = self.deviceToken else {
            self.tryOnSave = true
            
            return
        }
        
        DispatchQueue.main.async {
            Network.shared.apollo.perform(
                mutation: AssignDeviceTokenMutation(deviceToken: deviceToken)
            ) { _ in }
        }
    }
    
    // Check if user has changed notification status in mobile settings
    private func checkAuthorizationStatus() -> Void {
        let center = UNUserNotificationCenter.current()
        
        let _ = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            center.getNotificationSettings { (settings) in
                if settings.authorizationStatus == .authorized {
                    timer.invalidate()
                    
                    self.notificationAuthorized = true
                    self.sendDeviceTokenToServer()
                } else {
                    self.notificationAuthorized = false
                }
            }
        }
    }
}
