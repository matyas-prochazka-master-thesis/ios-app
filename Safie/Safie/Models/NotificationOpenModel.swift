//
//  NotificationOpenModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import Foundation

// Model for opening screens from notifications
enum NotificationOpenKey: Equatable {
    case guardianRequests
    case checkins
    case myGuardiansTripHistory
}

protocol NotificationOpenModelProtocol: ObservableObject {
    var notificationOpenKey: NotificationOpenKey? { get set }
    
    func processUserInfo(userInfo: [AnyHashable : Any]) -> Void
}

final class NotificationOpenModel: NotificationOpenModelProtocol {
    @Published var notificationOpenKey: NotificationOpenKey? = nil
    
    func processUserInfo(userInfo: [AnyHashable: Any]) -> Void {
        if let key = userInfo["open"] as? String {
            switch key {
            case "screen-guardian-requests":
                self.notificationOpenKey = .guardianRequests
            case "screen-checkins":
                self.notificationOpenKey = .checkins
            case "screens-my-guardians-trip-history":
                self.notificationOpenKey = .myGuardiansTripHistory
            default:
                self.notificationOpenKey = nil
            }
        }
    }
}
