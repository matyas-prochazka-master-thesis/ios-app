//
//  SosModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 26.03.2022.
//

import Foundation

enum SosButtonPressedError: Error {
    case baseError(message: String)
}

enum SosButtonReleasedError: Error {
    case baseError(message: String)
}

enum SosPinEnteredError: Error {
    case baseError(message: String)
}

enum SosPositionUpdateError: Error {
    case baseError(message: String)
}

enum MySosHistoryError: Error {
    case baseError(message: String)
}

enum LatestSosByAccountError: Error {
    case baseError(message: String)
}

enum SosPositionsError: Error {
    case baseError(message: String)
}

enum SosesOfAccountsIAmGuardianOfError: Error {
    case baseError(message: String)
}

protocol SosModelProtocol: ObservableObject {    
    func buttonPressed(
        idFromClient: String,
        resultHandler: @escaping (ApiResult<Void?, SosButtonPressedError>) -> Void
    ) -> Void
    func buttonReleased(
        idFromClient: String,
        resultHandler: @escaping (ApiResult<Void?, SosButtonReleasedError>) -> Void
    ) -> Void
    func pinEntered(
        idFromClient: String,
        resultHandler: @escaping (ApiResult<Void?, SosPinEnteredError>) -> Void
    ) -> Void
    func positionUpdate(
        idFromClient: String,
        resultHandler: @escaping (ApiResult<Void?, SosPositionUpdateError>) -> Void
    ) -> Void
    
    func getMySosHistory(resultHandler: @escaping (ApiResult<[Sos], MySosHistoryError>) -> Void) -> Void
    func getLatestSosByAccount(
        accountId: String,
        resultHandler: @escaping (ApiResult<Sos, LatestSosByAccountError>) -> Void
    ) -> Void
    func getSosPositions(
        sosId: String,
        resultHandler: @escaping (ApiResult<[SosPosition], SosPositionsError>) -> Void
    ) -> Void
    func getSosesOfAccountsIAmGuardianOf(
        resultHandler: @escaping (ApiResult<[Sos], SosesOfAccountsIAmGuardianOfError>) -> Void
    ) -> Void
}

final class SosModel: SosModelProtocol {
    private var locationModel: LocationModel
    
    init() {
        self.locationModel = DI.shared.container.resolve(LocationModel.self)!
    }
    
    func buttonPressed(
        idFromClient: String,
        resultHandler: @escaping (ApiResult<Void?, SosButtonPressedError>) -> Void
    ) -> Void {
        let latitude = self.locationModel.latitude
        let longitude = self.locationModel.longitude
        
        let buttonPressed = SosButtonPressedDto(idFromClient: idFromClient, latitude: latitude, longitude: longitude)
        
        Network.shared.apollo.perform(mutation: SosButtonPressedMutation(buttonPressed: buttonPressed)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SosButtonPressedError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.sosButtonPressed else {
                    resultHandler(.failure(SosButtonPressedError.baseError(message: "Problem with button pressed 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SosButtonPressedError.baseError(message: "Problem with button pressed 2")))
            }
        }
    }
    
    func buttonReleased(
        idFromClient: String,
        resultHandler: @escaping (ApiResult<Void?, SosButtonReleasedError>) -> Void
    ) -> Void {
        let buttonReleased = SosButtonReleasedDto(idFromClient: idFromClient)
        
        Network.shared.apollo.perform(mutation: SosButtonReleasedMutation(buttonReleased: buttonReleased)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SosButtonReleasedError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.sosButtonReleased else {
                    resultHandler(.failure(SosButtonReleasedError.baseError(message: "Problem with button released 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SosButtonReleasedError.baseError(message: "Problem with button released 2")))
            }
        }
    }
    
    func pinEntered(
        idFromClient: String,
        resultHandler: @escaping (ApiResult<Void?, SosPinEnteredError>) -> Void
    ) -> Void {
        let pinEntered = SosPinEnteredDto(idFromClient: idFromClient)
        
        Network.shared.apollo.perform(mutation: SosPinEnteredMutation(pinEntered: pinEntered)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SosPinEnteredError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.sosPinEntered else {
                    resultHandler(.failure(SosPinEnteredError.baseError(message: "Problem with pin entered 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SosPinEnteredError.baseError(message: "Problem with pin entered 2")))
            }
        }
    }
    
    func positionUpdate(
        idFromClient: String,
        resultHandler: @escaping (ApiResult<Void?, SosPositionUpdateError>) -> Void
    ) -> Void {
        guard let latitude = self.locationModel.latitude, let longitude = self.locationModel.longitude else { return }
        
        let positionUpdate = SosPositionUpdateDto(idFromClient: idFromClient, latitude: latitude, longitude: longitude)
        
        Network.shared.apollo.perform(mutation: SosPositionUpdateMutation(positionUpdate: positionUpdate)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SosPositionUpdateError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.sosPositionUpdate else {
                    resultHandler(.failure(SosPositionUpdateError.baseError(message: "Problem with position update 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SosPositionUpdateError.baseError(message: "Problem with position update 2")))
            }
        }
    }
    
    func getMySosHistory(resultHandler: @escaping (ApiResult<[Sos], MySosHistoryError>) -> Void) -> Void {
        Network.shared.apollo.fetch(query: MySosHistoryQuery()) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(MySosHistoryError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let mySosHistory = graphQLResult.data?.getMySosHistory else {
                    resultHandler(.failure(MySosHistoryError.baseError(message: "Problem with my sos history 1")))
                    
                    return
                }
                
                let mySoses: [Sos] = mySosHistory.compactMap { mySosHistoryItem in
                    let sosBasicInfo = mySosHistoryItem.fragments.sosBasicInfo
                    
                    var buttonPressedAt: Date? = nil
                    var buttonReleasedAt: Date? = nil
                    var pinEnteredAt: Date? = nil
                    var guardiansContactedAt: Date? = nil
                    let createdAt = DateUtil.shared.format(date: sosBasicInfo.createdAt)!
                    
                    if let buttonPressedAtString = sosBasicInfo.buttonPressedAt {
                        buttonPressedAt = DateUtil.shared.format(date: buttonPressedAtString)!
                    }
                    
                    if let buttonReleasedAtString = sosBasicInfo.buttonReleasedAt {
                        buttonReleasedAt = DateUtil.shared.format(date: buttonReleasedAtString)!
                    }
                    
                    if let pinEnteredAtString = sosBasicInfo.pinEnteredAt {
                        pinEnteredAt = DateUtil.shared.format(date: pinEnteredAtString)!
                    }
                    
                    if let guardiansContactedAtString = sosBasicInfo.guardiansContactedAt {
                        guardiansContactedAt = DateUtil.shared.format(date: guardiansContactedAtString)!
                    }
                    
                    return Sos(
                        id: sosBasicInfo.id,
                        idFromClient: sosBasicInfo.idFromClient,
                        account: nil,
                        state: sosBasicInfo.state,
                        initialLatitude: sosBasicInfo.initialLatitude,
                        initialLongitude: sosBasicInfo.initialLongitude,
                        buttonPressedAt: buttonPressedAt,
                        buttonReleasedAt: buttonReleasedAt,
                        pinEnteredAt: pinEnteredAt,
                        guardiansContactedAt: guardiansContactedAt,
                        createdAt: createdAt
                    )
                }
                
                resultHandler(.success(mySoses))
                
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(MySosHistoryError.baseError(message: "Problem with my sos history 2")))
            }
        }
    }
    
    func getLatestSosByAccount(
        accountId: String,
        resultHandler: @escaping (ApiResult<Sos, LatestSosByAccountError>) -> Void
    ) -> Void {
        
    }
    
    func getSosPositions(
        sosId: String,
        resultHandler: @escaping (ApiResult<[SosPosition], SosPositionsError>) -> Void
    ) -> Void {
        Network.shared.apollo.fetch(query: SosPositionsQuery(sosId: sosId), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SosPositionsError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let sosPositionItems = graphQLResult.data?.getSosPositions else {
                    resultHandler(.failure(SosPositionsError.baseError(message: "Problem with sos positions 1")))
                    
                    return
                }
                
                let sosPositions: [SosPosition] = sosPositionItems.compactMap { sosPositionItem in
                    let sosPositionBasicInfo = sosPositionItem.fragments.sosPositionBasicInfo
                                        
                    let createdAt = DateUtil.shared.format(date: sosPositionBasicInfo.createdAt)!
                    
                    return SosPosition(
                        id: sosPositionBasicInfo.id,
                        latitude: sosPositionBasicInfo.latitude,
                        longitude: sosPositionBasicInfo.longitude,
                        createdAt: createdAt
                    )
                }
                
                resultHandler(.success(sosPositions))
                
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SosPositionsError.baseError(message: "Problem with sos positions 2")))
            }
        }
    }
    
    func getSosesOfAccountsIAmGuardianOf(
        resultHandler: @escaping (ApiResult<[Sos], SosesOfAccountsIAmGuardianOfError>) -> Void
    ) -> Void {
        Network.shared.apollo.fetch(query: SosOfAccountsIAmGuardianOfQuery(), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SosesOfAccountsIAmGuardianOfError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let sosesItems = graphQLResult.data?.getSosesOfAccountsIAmGuardianOf else {
                    resultHandler(.failure(SosesOfAccountsIAmGuardianOfError.baseError(message: "Problem with soses of I follow 1")))
                    
                    return
                }
                
                let soses: [Sos] = sosesItems.compactMap { sosItem in
                    let sosBasicInfo = sosItem.fragments.sosBasicInfo
                    let sosAccountInfo = sosItem.fragments.sosAccountInfo
                    let accountBasicInfo = sosAccountInfo.account.fragments.accountBasicInfo
                    let accountProfilePicture = sosAccountInfo.account.fragments.accountProfilePicture
                    
                    // Dates
                    var buttonPressedAt: Date? = nil
                    var buttonReleasedAt: Date? = nil
                    var pinEnteredAt: Date? = nil
                    var guardiansContactedAt: Date? = nil
                    let createdAt = DateUtil.shared.format(date: sosBasicInfo.createdAt)!
                    
                    if let buttonPressedAtString = sosBasicInfo.buttonPressedAt {
                        buttonPressedAt = DateUtil.shared.format(date: buttonPressedAtString)!
                    }
                    
                    if let buttonReleasedAtString = sosBasicInfo.buttonReleasedAt {
                        buttonReleasedAt = DateUtil.shared.format(date: buttonReleasedAtString)!
                    }
                    
                    if let pinEnteredAtString = sosBasicInfo.pinEnteredAt {
                        pinEnteredAt = DateUtil.shared.format(date: pinEnteredAtString)!
                    }
                    
                    if let guardiansContactedAtString = sosBasicInfo.guardiansContactedAt {
                        guardiansContactedAt = DateUtil.shared.format(date: guardiansContactedAtString)!
                    }
                    
                    // Account
                    var profilePicture: File? = nil
                    
                    if let profilePictureFile = accountProfilePicture.profilePicture?.fragments.fileBasicInfo {
                        profilePicture = File(id: profilePictureFile.id, path: profilePictureFile.path)
                    }
                    
                    let account = Account(
                        id: accountBasicInfo.id,
                        username: accountBasicInfo.username,
                        email: accountBasicInfo.email,
                        profilePicture: profilePicture
                    )
                    
                    return Sos(
                        id: sosBasicInfo.id,
                        idFromClient: sosBasicInfo.idFromClient,
                        account: account,
                        state: sosBasicInfo.state,
                        initialLatitude: sosBasicInfo.initialLatitude,
                        initialLongitude: sosBasicInfo.initialLongitude,
                        buttonPressedAt: buttonPressedAt,
                        buttonReleasedAt: buttonReleasedAt,
                        pinEnteredAt: pinEnteredAt,
                        guardiansContactedAt: guardiansContactedAt,
                        createdAt: createdAt
                    )
                }
                
                resultHandler(.success(soses))
                
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SosesOfAccountsIAmGuardianOfError.baseError(message: "Problem with soses I follow 2")))
            }
        }
    }
}
