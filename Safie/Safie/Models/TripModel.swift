//
//  TripModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 26.03.2022.
//

import Foundation
import UIKit

enum GetActiveTripError: Error {
    case baseError(message: String)
}

enum GetTripError: Error {
    case baseError(message: String)
}

enum GetMyTripsError: Error {
    case baseError(message: String)
}

enum GetMyGuardiansTripsError: Error {
    case baseError(message: String)
}

enum CreateTripError: Error {
    case baseError(message: String)
}

enum EndTripError: Error {
    case baseError(message: String)
}

enum ActivitiesByTripError: Error {
    case baseError(message: String)
}

enum AddMessageToTripError: Error {
    case baseError(message: String)
}

enum AddPhotoToTripError: Error {
    case baseError(message: String)
}

enum AddQuickActionToTripError: Error {
    case baseError(message: String)
}

enum MyTripDestinationsError: Error {
    case baseError(message: String)
}

enum ToggleFavouriteTripDestinationError: Error {
    case baseError(message: String)
}

protocol TripModelProtocol {
    func getActiveTrip(resultHandler: @escaping (ApiResult<Trip?, GetActiveTripError>) -> Void) -> Void
    
    func getTrip(tripId: String, resultHandler: @escaping (ApiResult<Trip, GetTripError>) -> Void) -> Void
    
    func getMyTrips(resultHandler: @escaping (ApiResult<[Trip], GetMyTripsError>) -> Void) -> Void
    
    func getMyGuardiansTrips(resultHandler: @escaping (ApiResult<[Trip], GetMyGuardiansTripsError>) -> Void) -> Void
    
    func createTrip(
        shouldArriveAt: Date?,
        tripDestinationId: String?,
        tripDestinationCreateDto: TripDestinationCreateDto?,
        quickActions: [QuickAction],
        resultHandler: @escaping (ApiResult<Trip, CreateTripError>) -> Void
    ) -> Void
    
    func endTrip(tripId: String, resultHandler: @escaping (ApiResult<Void?, EndTripError>) -> Void) -> Void
    
    func getActivitiesByTrip(tripId: String, resultHandler: @escaping (ApiResult<[TripActivity], ActivitiesByTripError>) -> Void) -> Void
    
    func addMessageToTrip(
        tripId: String,
        message: String,
        resultHandler: @escaping (ApiResult<TripActivity, AddMessageToTripError>) -> Void
    ) -> Void
    
    func addPhotoToTrip(
        tripId: String,
        photo: String,
        resultHandler: @escaping (ApiResult<TripActivity, AddPhotoToTripError>) -> Void
    ) -> Void
    
    func addQuickActionToTrip(
        tripId: String,
        name: String,
        icon: String,
        currentQuickActionIndex: Int?,
        resultHandler: @escaping (ApiResult<TripActivity, AddQuickActionToTripError>) -> Void
    ) -> Void
    
    func getMyTripDestinations(resultHandler: @escaping (ApiResult<[TripDestination], MyTripDestinationsError>) -> Void) -> Void
    
    func toggleFavouriteTripDestination(
        tripDestinationId: String,
        resultHandler: @escaping (ApiResult<Void?, ToggleFavouriteTripDestinationError>) -> Void
    ) -> Void
}

final class TripModel: TripModelProtocol {
    private var locationModel: LocationModel
    
    init() {
        self.locationModel = DI.shared.container.resolve(LocationModel.self)!
    }
    
    func getActiveTrip(resultHandler: @escaping (ApiResult<Trip?, GetActiveTripError>) -> Void) {
        Network.shared.apollo.fetch(query: ActiveTripQuery(), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(GetActiveTripError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let activeTripData = graphQLResult.data?.getActiveTrip else {
                    resultHandler(.failure(GetActiveTripError.baseError(message: "Problem with getting active trip 1")))
                    
                    return
                }
                                
                let activeTrip = self.mapTrip(
                    tripBasicInfo: activeTripData.fragments.tripBasicInfo,
                    tripAccountInfo: nil
                )
                
                resultHandler(.success(activeTrip))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(GetActiveTripError.baseError(message: "Problem with getting active trip 2")))
            }
        }
    }
    
    func getTrip(tripId: String, resultHandler: @escaping (ApiResult<Trip, GetTripError>) -> Void) -> Void {
        Network.shared.apollo.fetch(query: TripQuery(tripId: tripId), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(GetTripError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let tripData = graphQLResult.data?.getTrip else {
                    resultHandler(.failure(GetTripError.baseError(message: "Problem with getting trip 1")))
                    
                    return
                }
                                
                let trip = self.mapTrip(
                    tripBasicInfo: tripData.fragments.tripBasicInfo,
                    tripAccountInfo: tripData.fragments.tripAccountInfo
                )
                
                resultHandler(.success(trip))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(GetTripError.baseError(message: "Problem with getting trip 2")))
            }
        }
    }
    
    func getMyTrips(resultHandler: @escaping (ApiResult<[Trip], GetMyTripsError>) -> Void) -> Void {
        Network.shared.apollo.fetch(query: MyTripsQuery(), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(GetMyTripsError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let tripItems = graphQLResult.data?.getMyTrips else {
                    resultHandler(.failure(GetMyTripsError.baseError(message: "Problem with getting my trips 1")))
                    
                    return
                }
                
                let trips = tripItems.compactMap { tripItem in
                    return self.mapTrip(
                        tripBasicInfo: tripItem.fragments.tripBasicInfo,
                        tripAccountInfo: nil
                    )
                }
                
                resultHandler(.success(trips))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(GetMyTripsError.baseError(message: "Problem with getting my trips 2")))
            }
        }
    }
    
    func getMyGuardiansTrips(resultHandler: @escaping (ApiResult<[Trip], GetMyGuardiansTripsError>) -> Void) -> Void {
        Network.shared.apollo.fetch(query: MyGuardiansTripsQuery(), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(GetMyGuardiansTripsError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let tripItems = graphQLResult.data?.getMyGuardiansTrips else {
                    resultHandler(.failure(GetMyGuardiansTripsError.baseError(message: "Problem with getting my guardians trips 1")))
                    
                    return
                }
                
                let trips = tripItems.compactMap { tripItem in
                    return self.mapTrip(
                        tripBasicInfo: tripItem.fragments.tripBasicInfo,
                        tripAccountInfo: tripItem.fragments.tripAccountInfo
                    )
                }
                
                resultHandler(.success(trips))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(GetMyGuardiansTripsError.baseError(message: "Problem with getting my guardians trips 2")))
            }
        }
    }
    
    func createTrip(
        shouldArriveAt: Date?,
        tripDestinationId: String?,
        tripDestinationCreateDto: TripDestinationCreateDto?,
        quickActions: [QuickAction],
        resultHandler: @escaping (ApiResult<Trip, CreateTripError>) -> Void
    ) {
        var shouldArriveAtString: String? = nil
        
        if let shouldArriveAt = shouldArriveAt {
            shouldArriveAtString = DateUtil.shared.toISO8601(date: shouldArriveAt)
        }
        
        let quickActions = quickActions.map{ quickAction in
            TripQuickActionDto(
                id: quickAction.id,
                name: quickAction.name,
                icon: quickAction.icon
            )
        }
        
        let tripCreate = TripCreateDto(
            shouldArriveAt: shouldArriveAtString,
            tripDestinationId: tripDestinationId,
            tripDestinationCreateDto: tripDestinationCreateDto,
            quickActions: quickActions
        )
        
        Network.shared.apollo.perform(mutation: CreateTripMutation(tripCreate: tripCreate)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(CreateTripError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let tripData = graphQLResult.data?.createTrip else {
                    resultHandler(.failure(CreateTripError.baseError(message: "Problem with creating trip 1")))
                    
                    return
                }
                
                let trip = self.mapTrip(
                    tripBasicInfo: tripData.fragments.tripBasicInfo,
                    tripAccountInfo: nil
                )
                
                resultHandler(.success(trip))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(CreateTripError.baseError(message: "Problem with creating trip 2")))
            }
        }
    }
    
    func endTrip(tripId: String, resultHandler: @escaping (ApiResult<Void?, EndTripError>) -> Void) {
        Network.shared.apollo.perform(mutation: EndTripMutation(tripId: tripId)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(EndTripError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.endTrip else {
                    resultHandler(.failure(EndTripError.baseError(message: "Problem with ending trip 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(EndTripError.baseError(message: "Problem with ending trip 2")))
            }
        }
    }
    
    func getActivitiesByTrip(tripId: String, resultHandler: @escaping (ApiResult<[TripActivity], ActivitiesByTripError>) -> Void) -> Void {
        Network.shared.apollo.fetch(query: ActivitiesByTripQuery(tripId: tripId), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(ActivitiesByTripError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let activityItems = graphQLResult.data?.getActivitiesByTrip else {
                    resultHandler(.failure(ActivitiesByTripError.baseError(message: "Problem with getting activities by trip 1")))
                    
                    return
                }
                
                let activities: [TripActivity] = activityItems.compactMap { activityItem in
                    return self.mapActivity(tripActivityBasicInfo: activityItem.fragments.tripActivityBasicInfo)
                }
                
                resultHandler(.success(activities))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(ActivitiesByTripError.baseError(message: "Problem with getting activities by trip 2")))
            }
        }
    }
    
    func addMessageToTrip(
        tripId: String,
        message: String,
        resultHandler: @escaping (ApiResult<TripActivity, AddMessageToTripError>) -> Void
    ) {
        let activityMessage = ActivityMessagePayload(message: message)
        let activityCreate = self.createActivityCreateDto()
        
        let addMessageToTripMutation = AddMessageToTripMutation(
            tripId: tripId,
            activityCreate: activityCreate,
            activityMessage: activityMessage
        )
        
        Network.shared.apollo.perform(mutation: addMessageToTripMutation) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(AddMessageToTripError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let tripActivityData = graphQLResult.data?.addMessageToTrip else {
                    resultHandler(.failure(AddMessageToTripError.baseError(message: "Problem with adding message to trip 1")))
                    
                    return
                }
                
                let tripActivity = self.mapActivity(tripActivityBasicInfo: tripActivityData.fragments.tripActivityBasicInfo)
                
                resultHandler(.success(tripActivity))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(AddMessageToTripError.baseError(message: "Problem with adding message to trip 2")))
            }
        }
    }
    
    func addPhotoToTrip(
        tripId: String,
        photo: String,
        resultHandler: @escaping (ApiResult<TripActivity, AddPhotoToTripError>) -> Void
    ) {
        let activityPhoto = ActivityPhotoPayload(photo: photo)
        let activityCreate = self.createActivityCreateDto()
        
        let addPhotoToTripMutation = AddPhotoToTripMutation(
            tripId: tripId,
            activityCreate: activityCreate,
            activityPhoto: activityPhoto
        )
        
        Network.shared.apollo.perform(mutation: addPhotoToTripMutation) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(AddPhotoToTripError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let tripActivityData = graphQLResult.data?.addPhotoToTrip else {
                    resultHandler(.failure(AddPhotoToTripError.baseError(message: "Problem with adding photo to trip 1")))
                    
                    return
                }
                
                let tripActivity = self.mapActivity(tripActivityBasicInfo: tripActivityData.fragments.tripActivityBasicInfo)
                
                resultHandler(.success(tripActivity))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(AddPhotoToTripError.baseError(message: "Problem with adding photo to trip 2")))
            }
        }
    }
    
    func addQuickActionToTrip(
        tripId: String,
        name: String,
        icon: String,
        currentQuickActionIndex: Int?,
        resultHandler: @escaping (ApiResult<TripActivity, AddQuickActionToTripError>) -> Void
    ) {
        let activityQuickAction = ActivityQuickActionPayload(
            name: name,
            icon: icon,
            currentQuickActionIndex: currentQuickActionIndex
        )
        let activityCreate = self.createActivityCreateDto()
        
        let addQuickActionToTripMutation = AddQuickActionToTripMutation(
            tripId: tripId,
            activityCreate: activityCreate,
            activityQuickAction: activityQuickAction
        )
        
        Network.shared.apollo.perform(mutation: addQuickActionToTripMutation) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(AddQuickActionToTripError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let tripActivityData = graphQLResult.data?.addQuickActionToTrip else {
                    resultHandler(.failure(AddQuickActionToTripError.baseError(message: "Problem with adding quick action to trip 1")))
                    
                    return
                }
                
                let tripActivity = self.mapActivity(tripActivityBasicInfo: tripActivityData.fragments.tripActivityBasicInfo)
                
                resultHandler(.success(tripActivity))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(AddQuickActionToTripError.baseError(message: "Problem with adding quick action to trip 2")))
            }
        }
    }
    
    func getMyTripDestinations(resultHandler: @escaping (ApiResult<[TripDestination], MyTripDestinationsError>) -> Void) -> Void {
        Network.shared.apollo.fetch(query: MyTripDestinationsQuery(), cachePolicy: .fetchIgnoringCacheData) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(MyTripDestinationsError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let destinationItems = graphQLResult.data?.getMyTripDestinations else {
                    resultHandler(.failure(MyTripDestinationsError.baseError(message: "Problem with getting my trip destinations 1")))
                    
                    return
                }
                
                let destinations: [TripDestination] = destinationItems.compactMap { destinationItem in
                    let tripDestinationBasicInfo = destinationItem.fragments.tripDestinationBasicInfo
                    
                    let createdAt = DateUtil.shared.format(date: tripDestinationBasicInfo.createdAt)!
                    
                    return TripDestination(
                        id: tripDestinationBasicInfo.id,
                        name: tripDestinationBasicInfo.name,
                        latitude: tripDestinationBasicInfo.latitude,
                        longitude: tripDestinationBasicInfo.longitude,
                        favourite: tripDestinationBasicInfo.favourite,
                        createdAt: createdAt
                    )
                }
                
                resultHandler(.success(destinations))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(MyTripDestinationsError.baseError(message: "Problem with getting my trip destinations 2")))
            }
        }
    }
    
    func toggleFavouriteTripDestination(
        tripDestinationId: String,
        resultHandler: @escaping (ApiResult<Void?, ToggleFavouriteTripDestinationError>) -> Void
    ) -> Void {
        Network.shared.apollo.perform(mutation: ToggleFavouriteTripDestinationMutation(tripDestinationId: tripDestinationId)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(ToggleFavouriteTripDestinationError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.toggleFavouriteTripDestination else {
                    resultHandler(.failure(ToggleFavouriteTripDestinationError.baseError(message: "Problem with toggling favourite trip destination 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(ToggleFavouriteTripDestinationError.baseError(message: "Problem with toggling favourite trip destination 2")))
            }
        }
    }
    
    private func createActivityCreateDto() -> ActivityCreateDto {
        let latitude = self.locationModel.latitude
        let longitude = self.locationModel.longitude
        let batteryLevel = Double(UIDevice.current.batteryLevel)
        
        return ActivityCreateDto(
            latitude: latitude,
            longitude: longitude,
            batteryLevel: batteryLevel
        )
    }
    
    private func mapTrip(tripBasicInfo: TripBasicInfo, tripAccountInfo: TripAccountInfo?) -> Trip {
        var destination: TripDestination? = nil
                                 
        if let tripDestinationBasicInfo = tripBasicInfo.destination?.fragments.tripDestinationBasicInfo {
            let tripDestinationCreatedAt = DateUtil.shared.format(date: tripDestinationBasicInfo.createdAt)!
            
            destination = TripDestination(
                id: tripDestinationBasicInfo.id,
                name: tripDestinationBasicInfo.name,
                latitude: tripDestinationBasicInfo.latitude,
                longitude: tripDestinationBasicInfo.longitude,
                favourite: tripDestinationBasicInfo.favourite,
                createdAt: tripDestinationCreatedAt
            )
        }
        
        var shouldArriveAt: Date? = nil
        let createdAt = DateUtil.shared.format(date: tripBasicInfo.createdAt)!
        
        if let shouldArriveAtString = tripBasicInfo.shouldArriveAt {
            shouldArriveAt = DateUtil.shared.format(date: shouldArriveAtString)
        }
        
        let quickActions: [QuickAction] = tripBasicInfo.quickActions.compactMap { quickAction in
            let tripQuickActionBasicInfo = quickAction.fragments.tripQuickActionBasicInfo
            
            return QuickAction(
                id: tripQuickActionBasicInfo.id,
                name: tripQuickActionBasicInfo.name,
                icon: tripQuickActionBasicInfo.icon
            )
        }
        
        var account: Account? = nil
        
        if let tripAccountInfo = tripAccountInfo {
            let accountBasicInfo = tripAccountInfo.account.fragments.accountBasicInfo
            let accountProfilePicture = tripAccountInfo.account.fragments.accountProfilePicture
            
            var profilePicture: File? = nil
            
            if let profilePictureFile = accountProfilePicture.profilePicture?.fragments.fileBasicInfo {
                profilePicture = File(id: profilePictureFile.id, path: profilePictureFile.path)
            }
            
            account = Account(
                id: accountBasicInfo.id,
                username: accountBasicInfo.username,
                email: accountBasicInfo.email,
                profilePicture: profilePicture
            )
        }
        
        return Trip(
            id: tripBasicInfo.id,
            account: account,
            state: tripBasicInfo.state,
            shouldArriveAt: shouldArriveAt,
            destination: destination,
            quickActions: quickActions,
            currentQuickActionIndex: tripBasicInfo.currentQuickActionIndex,
            createdAt: createdAt
        )
    }
    
    private func mapActivity(tripActivityBasicInfo: TripActivityBasicInfo) -> TripActivity {
        let createdAt = DateUtil.shared.format(date: tripActivityBasicInfo.createdAt)!
        
        var attachmentFile: File? = nil
        
        if let attachmentBasicInfo = tripActivityBasicInfo.attachment?.fragments.fileBasicInfo {
            attachmentFile = File(id: attachmentBasicInfo.id, path: attachmentBasicInfo.path)
        }
        
        var payloadMessage: ActivityPayloadMessage? = nil
        var payloadQuickAction: ActivityPayloadQuickAction? = nil
        
        let data = tripActivityBasicInfo.payload.data(using: .utf8)!
        let payload = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: String]
        
        switch tripActivityBasicInfo.key {
        case .message:
            payloadMessage = ActivityPayloadMessage(message: payload["message"]!)
        case .quickAction:
            payloadQuickAction = ActivityPayloadQuickAction(
                name: payload["name"]!,
                icon: payload["icon"]!
            )
        default:
            break
        }
        
        return TripActivity(
            id: tripActivityBasicInfo.id,
            key: tripActivityBasicInfo.key,
            latitude: tripActivityBasicInfo.latitude,
            longitude: tripActivityBasicInfo.longitude,
            batteryLevel: tripActivityBasicInfo.batteryLevel,
            payloadMessage: payloadMessage,
            payloadQuickAction: payloadQuickAction,
            attachment: attachmentFile,
            createdAt: createdAt
        )
    }
}
