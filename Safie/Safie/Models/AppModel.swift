//
//  AppModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 19.08.2021.
//

import Foundation

protocol AppModelProtocol: ObservableObject {
    var onboardingShowed: Bool { get set }
    
    func setOnboardingShowed() -> Void
}

final class AppModel: AppModelProtocol {
    @Published var onboardingShowed: Bool {
        didSet {
            UserDefaults.standard.set(self.onboardingShowed, forKey: "onboardingShowed")
        }
    }
    
    init() {
        self.onboardingShowed = UserDefaults.standard.bool(forKey: "onboardingShowed")
        //self.onboardingShowed = false
    }
    
    func setOnboardingShowed() -> Void {
        self.onboardingShowed = true
    }
}
