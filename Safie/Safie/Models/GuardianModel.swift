//
//  GuardianModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import Foundation

enum AddGuardianError: Error {
    case baseError(message: String)
}

enum AcceptGuardianError: Error {
    case baseError(message: String)
}

enum RemoveGuardianError: Error {
    case baseError(message: String)
}

protocol GuardianModelProtocol: ObservableObject {
    var refreshingGuardians: Bool { get set }
    var guardians: [Guardian] { get set }
    
    var refreshingGuardianRequests: Bool { get set }
    var guardianRequests: [Guardian] { get set }
        
    func refreshGuardians() -> Void
    func refreshGuardianRequests() -> Void
        
    func addGuardian(username: String, resultHandler: @escaping (ApiResult<Void?, AddGuardianError>) -> Void) -> Void
    func acceptGuardian(guardianId: String, resultHandler: @escaping (ApiResult<Void?, AcceptGuardianError>) -> Void) -> Void
    func removeGuardian(guardianId: String, resultHandler: @escaping (ApiResult<Void?, RemoveGuardianError>) -> Void) -> Void
}

final class GuardianModel: GuardianModelProtocol {
    @Published var refreshingGuardians: Bool = false
    @Published var guardians: [Guardian] = []
    
    @Published var refreshingGuardianRequests: Bool = false
    @Published var guardianRequests: [Guardian] = []
    
    func refreshGuardians() {
        guard !self.refreshingGuardians else {
            return
        }
        
        self.refreshingGuardians = true
        
        Network.shared.apollo.fetch(query: MyGuardiansQuery(), cachePolicy: .fetchIgnoringCacheData) { result in
            defer {
                self.refreshingGuardians = false
            }

            switch result {
            case .success(let graphQLResult):
                guard let tmpGuardians = graphQLResult.data?.getMyGuardians else {
                    return
                }

                self.guardians = tmpGuardians.compactMap({ guardian in
                    let guardianBasicInfo = guardian.fragments.guardianBasicInfo
                    let accountBasicInfo = guardianBasicInfo.guardian.fragments.accountBasicInfo
                    let accountProfilePicture = guardianBasicInfo.guardian.fragments.accountProfilePicture
                    
                    var profilePicture: File? = nil
                    
                    if let profilePictureFile = accountProfilePicture.profilePicture?.fragments.fileBasicInfo {
                        profilePicture = File(id: profilePictureFile.id, path: profilePictureFile.path)
                    }
                    
                    let account = Account(
                        id: accountBasicInfo.id,
                        username: accountBasicInfo.username,
                        email: accountBasicInfo.email,
                        profilePicture: profilePicture
                    )
                    
                    let createdAt = DateUtil.shared.format(date: guardianBasicInfo.createdAt)!
                    
                    return Guardian(
                        id: guardianBasicInfo.id,
                        account: nil,
                        guardian: account,
                        acceptedAt: nil,
                        createdAt: createdAt
                    )
                })
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
            }
        }
    }
    
    func refreshGuardianRequests() {
        guard !self.refreshingGuardianRequests else {
            return
        }
        
        self.refreshingGuardianRequests = true
        
        Network.shared.apollo.fetch(query: GuardianRequestsQuery(), cachePolicy: .fetchIgnoringCacheData) { result in
            defer {
                self.refreshingGuardianRequests = false
            }

            switch result {
            case .success(let graphQLResult):
                if let _ = graphQLResult.errors {
                    return
                }
                
                guard let tmpGuardians = graphQLResult.data?.getGuardiansRequests else {
                    return
                }

                self.guardianRequests = tmpGuardians.compactMap({ guardian in
                    let guardianAccountInfo = guardian.fragments.guardianAccountInfo
                    let accountBasicInfo = guardianAccountInfo.account.fragments.accountBasicInfo
                    let accountProfilePicture = guardianAccountInfo.account.fragments.accountProfilePicture
                    
                    var profilePicture: File? = nil
                    
                    if let profilePictureFile = accountProfilePicture.profilePicture?.fragments.fileBasicInfo {
                        profilePicture = File(id: profilePictureFile.id, path: profilePictureFile.path)
                    }
                    
                    let account = Account(
                        id: accountBasicInfo.id,
                        username: accountBasicInfo.username,
                        email: accountBasicInfo.email,
                        profilePicture: profilePicture
                    )
                    
                    let createdAt = DateUtil.shared.format(date: guardianAccountInfo.createdAt)!
                    
                    return Guardian(
                        id: guardianAccountInfo.id,
                        account: account,
                        guardian: nil,
                        acceptedAt: nil,
                        createdAt: createdAt
                    )
                })
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
            }
        }
    }
    
    func addGuardian(username: String, resultHandler: @escaping (ApiResult<Void?, AddGuardianError>) -> Void) -> Void {
        let guardianAdd = GuardianAddDto(username: username)
        
        Network.shared.apollo.perform(mutation: AddGuardianMutation(guardianAdd: guardianAdd)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(AddGuardianError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.addGuardian else {
                    resultHandler(.failure(AddGuardianError.baseError(message: "Problem with adding guardian 1")))
                    
                    return
                }
                
                self.refreshGuardians()
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(AddGuardianError.baseError(message: "Problem with adding guardian 2")))
            }
        }
    }
    
    func acceptGuardian(guardianId: String, resultHandler: @escaping (ApiResult<Void?, AcceptGuardianError>) -> Void) -> Void {
        let guardianAccept = GuardianAcceptDto(guardianId: guardianId)
        
        Network.shared.apollo.perform(mutation: AcceptGuardianMutation(guardianAccept: guardianAccept)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(AcceptGuardianError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.acceptGuardian else {
                    resultHandler(.failure(AcceptGuardianError.baseError(message: "Problem with accepting guardian 1")))
                    
                    return
                }
                
                self.refreshGuardians()
                self.refreshGuardianRequests()
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(AcceptGuardianError.baseError(message: "Problem with accepting guardian 2")))
            }
        }
    }
    
    func removeGuardian(guardianId: String, resultHandler: @escaping (ApiResult<Void?, RemoveGuardianError>) -> Void) -> Void {
        let guardianRemove = GuardianRemoveDto(guardianId: guardianId)
        
        Network.shared.apollo.perform(mutation: RemoveGuardianMutation(guardianRemove: guardianRemove)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(RemoveGuardianError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.removeGuardian else {
                    resultHandler(.failure(RemoveGuardianError.baseError(message: "Problem with removing guardian 1")))
                    
                    return
                }
                
                self.refreshGuardians()
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(RemoveGuardianError.baseError(message: "Problem with removing guardian 2")))
            }
        }
    }
}
