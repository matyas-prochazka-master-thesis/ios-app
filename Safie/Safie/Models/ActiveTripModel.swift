//
//  ActiveTripModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 07.04.2022.
//

import Foundation

// User can have 1 active trip max
protocol ActiveTripModelProtocol: ObservableObject {
    var activeTrip: Trip? { get }
    var activities: [TripActivity] { get }
        
    var checkingIfTripStarted: Bool { get }
    var refreshingActivities: Bool { get }
    var startingTrip: Bool { get }
    var endingTrip: Bool { get }
    
    func startTrip(
        shouldArriveAt: Date?,
        tripDestinationId: String?,
        tripDestinationCreateDto: TripDestinationCreateDto?,
        quickActions: [QuickAction]
    ) -> Void
    func endTrip() -> Void
    func refreshActivities() -> Void
    func addMessage(message: String, resultHandler: @escaping (ApiResult<Bool, AddMessageToTripError>) -> Void) -> Void
    func addPhoto(photo: String, resultHandler: @escaping (ApiResult<Bool, AddPhotoToTripError>) -> Void) -> Void
    func addQuickAction(name: String, icon: String, currentQuickActionIndex: Int?) -> Void
    
    func clear() -> Void
}

final class ActiveTripModel: ActiveTripModelProtocol {
    private var tripModel: TripModel
    
    @Published var activeTrip: Trip? = nil
    @Published var activities: [TripActivity] = []
        
    @Published var checkingIfTripStarted: Bool = true
    @Published var refreshingActivities: Bool = false
    @Published var startingTrip: Bool = false
    @Published var endingTrip: Bool = false
    
    init() {
        self.tripModel = DI.shared.container.resolve(TripModel.self)!
        
        self.checkActiveTrip()
    }
    
    // Check if user has active trip
    private func checkActiveTrip() -> Void {
        self.tripModel.getActiveTrip { result in
            defer {
                self.checkingIfTripStarted = false
            }
            
            switch result {
            case .success(let activeTrip):
                self.activeTrip = activeTrip
                self.refreshActivities()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func startTrip(
        shouldArriveAt: Date?,
        tripDestinationId: String?,
        tripDestinationCreateDto: TripDestinationCreateDto?,
        quickActions: [QuickAction]
    ) -> Void {
        guard self.activeTrip == nil else { return }
        
        guard !self.startingTrip else { return }
        
        self.startingTrip = true
        
        self.tripModel.createTrip(
            shouldArriveAt: shouldArriveAt,
            tripDestinationId: tripDestinationId,
            tripDestinationCreateDto: tripDestinationCreateDto,
            quickActions: quickActions
        ) { result in
            defer {
                self.startingTrip = false
            }
            
            switch result {
            case .success(let trip):
                self.activeTrip = trip
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func endTrip() -> Void {
        guard let activeTrip = self.activeTrip else { return }

        guard !self.endingTrip else { return }
        
        self.endingTrip = true
        
        self.tripModel.endTrip(tripId: activeTrip.id) { result in
            defer {
                self.endingTrip = false
            }
            
            switch result {
            case .success(_):
                self.activeTrip?.state = .finished
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func refreshActivities() -> Void {
        guard let activeTrip = self.activeTrip else { return }
        
        guard !self.refreshingActivities else { return }
        
        self.refreshingActivities = true
        
        self.tripModel.getActivitiesByTrip(tripId: activeTrip.id) { result in
            defer {
                self.refreshingActivities = false
            }
            
            switch result {
            case .success(let activities):
                self.activities = activities
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func addMessage(message: String, resultHandler: @escaping (ApiResult<Bool, AddMessageToTripError>) -> Void) -> Void {
        guard let activeTrip = self.activeTrip else { return }
        
        self.tripModel.addMessageToTrip(
            tripId: activeTrip.id,
            message: message
        ) { result in
            switch result {
            case .success(let tripActivity):
                self.activities.append(tripActivity)
                
                resultHandler(.success(true))
            case .failure(let error):
                print(error)
                
                resultHandler(.failure(error))
            }
        }
    }
    
    func addPhoto(photo: String, resultHandler: @escaping (ApiResult<Bool, AddPhotoToTripError>) -> Void) -> Void {
        guard let activeTrip = self.activeTrip else { return }
        
        self.tripModel.addPhotoToTrip(
            tripId: activeTrip.id,
            photo: photo
        ) { result in
            switch result {
            case .success(let tripActivity):
                self.activities.append(tripActivity)
                
                resultHandler(.success(true))
            case .failure(let error):
                print(error)
                
                resultHandler(.failure(error))
            }
        }
    }
    
    func addQuickAction(name: String, icon: String, currentQuickActionIndex: Int?) -> Void {
        guard let activeTrip = self.activeTrip else { return }
        
        if let currentQuickActionIndex = currentQuickActionIndex {
            self.activeTrip?.currentQuickActionIndex = currentQuickActionIndex
        }
        
        self.tripModel.addQuickActionToTrip(
            tripId: activeTrip.id,
            name: name,
            icon: icon,
            currentQuickActionIndex: currentQuickActionIndex
        ) { result in
            switch result {
            case .success(let tripActivity):
                self.activities.append(tripActivity)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func clear() -> Void {
        self.activeTrip = nil
        self.activities = []
    }
}
