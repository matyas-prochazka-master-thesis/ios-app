//
//  ErrorModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 07.04.2022.
//

import Foundation

protocol ErrorModelProtocol {
    static var shared: Self { get set }
    
    func logError(error: String) -> Void
    func logError(error: Error) -> Void
}

final class ErrorModel: ErrorModelProtocol {
    static var shared = ErrorModel()
    
    func logError(error: String) {
        print(error)
    }
    
    func logError(error: Error) {
        print(error)
    }
}
