//
//  DIContainer.swift
//  Safie
//
//  Created by Matyáš Procházka on 10.08.2021.
//

import Foundation
import Swinject

protocol DIProtocol {
    static var shared: Self { get set }

    var container: Container { get }
    
    static func reset() -> Void
}

final class DI: DIProtocol {
    static var shared = DI()
    
    var container: Container
    
    init() {
        self.container = Container()
        
        self.container.register(AppModel.self) { _ in AppModel() }.inObjectScope(.container)
        self.container.register(AccountModel.self) { _ in AccountModel() }.inObjectScope(.container)
        self.container.register(AuthModel.self) { _ in AuthModel() }.inObjectScope(.container)
        self.container.register(NotificationModel.self) { _ in NotificationModel() }.inObjectScope(.container)
        self.container.register(GuardianModel.self) { _ in GuardianModel() }.inObjectScope(.container)
        self.container.register(PinModel.self) { _ in PinModel() }.inObjectScope(.container)
        self.container.register(SosModel.self) { _ in SosModel() }.inObjectScope(.container)
        self.container.register(TripModel.self) { _ in TripModel() }.inObjectScope(.container)
        self.container.register(ActiveTripModel.self) { _ in ActiveTripModel() }.inObjectScope(.container)
        self.container.register(LocationModel.self) { _ in LocationModel() }.inObjectScope(.container)
        self.container.register(CheckinModel.self) { _ in CheckinModel() }.inObjectScope(.container)
        self.container.register(NotificationOpenModel.self) { _ in NotificationOpenModel() }.inObjectScope(.container)
    }
    
    static func reset() {        
        DI.shared.container.register(AccountModel.self) { _ in AccountModel() }.inObjectScope(.container)        
        DI.shared.container.register(GuardianModel.self) { _ in GuardianModel() }.inObjectScope(.container)
        DI.shared.container.register(PinModel.self) { _ in PinModel() }.inObjectScope(.container)
    }
}
