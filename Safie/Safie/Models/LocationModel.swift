//
//  LocationModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 26.03.2022.
//

import Foundation
import MapKit

protocol LocationModelProtocol: ObservableObject {
    var locationAuthorized: Bool { get set }
    
    var latitude: Double? { get set }
    var longitude: Double? { get set }
    
    func requestLocationAuthorzation() -> Void
}

final class LocationModel: NSObject, CLLocationManagerDelegate, LocationModelProtocol {
    @Published var locationAuthorized: Bool = false
    
    @Published var latitude: Double?
    @Published var longitude: Double?
    
    private var locationManager = CLLocationManager()    
    private var locationStatus: CLAuthorizationStatus?
    
    override init() {
        super.init()
                
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    func requestLocationAuthorzation() {
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.locationManager.startUpdatingLocation()
    }
    
    // Delegate function
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.locationStatus = status
        
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            self.locationAuthorized = true
        } else {
            self.locationAuthorized = false
        }
    }

    // Delegate function
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
    }
}
