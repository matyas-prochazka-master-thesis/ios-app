//
//  AccountModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 16.08.2021.
//

import Foundation

enum UpdateUsernameError: Error {
    case baseError(message: String)
}

enum UpdateProfilePictureError: Error {
    case baseError(message: String)
}

protocol AccountModelProtocol: ObservableObject {
    var refreshing: Bool { get set }
    var account: Account? { get set }
        
    func refresh() -> Void
    
    func updateUsername(username: String, resultHandler: @escaping (ApiResult<Void?, UpdateUsernameError>) -> Void) -> Void
    func updateProfilePicture(
        profilePictureBase64: String,
        resultHandler: @escaping (ApiResult<Void?, UpdateProfilePictureError>) -> Void
    ) -> Void
}

final class AccountModel: AccountModelProtocol {
    @Published var refreshing: Bool = false
    @Published var account: Account?
    
    init() {
        self.refresh()
    }
    
    // Refresh my account info
    func refresh() {
        guard !self.refreshing else {
            return
        }
        
        self.refreshing = true
        
        Network.shared.apollo.fetch(query: MyAccountQuery(), cachePolicy: .fetchIgnoringCacheData) { result in
            defer {
                self.refreshing = false
            }
            
            switch result {
            case .success(let graphQLResult):
                guard let me = graphQLResult.data?.me.fragments.accountBasicInfo else {
                    return
                }
                
                var profilePicture: File? = nil
                
                if let profilePictureFrament = graphQLResult.data?.me.fragments.accountProfilePicture,
                   let profilePictureFile = profilePictureFrament.profilePicture?.fragments.fileBasicInfo {
                    profilePicture = File(id: profilePictureFile.id, path: profilePictureFile.path)                    
                }
                
                self.account = Account(
                    id: me.id,
                    username: me.username,
                    email: me.email,
                    profilePicture: profilePicture
                )
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
            }
        }
    }
    
    func updateUsername(username: String, resultHandler: @escaping (ApiResult<Void?, UpdateUsernameError>) -> Void) -> Void {
        let usernameUpdate = UsernameUpdateDto(username: username)
        
        Network.shared.apollo.perform(mutation: UpdateUsernameMutation(usernameUpdate: usernameUpdate)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(UpdateUsernameError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.updateUsername else {
                    resultHandler(.failure(UpdateUsernameError.baseError(message: "Problem with updating username 1")))
                    
                    return
                }
                
                self.refresh()
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(UpdateUsernameError.baseError(message: "Problem with updating username 2")))
            }
        }
    }
    
    func updateProfilePicture(
        profilePictureBase64: String,
        resultHandler: @escaping (ApiResult<Void?, UpdateProfilePictureError>) -> Void
    ) -> Void {
        let profilePictureUpdate = ProfilePictureUpdateDto(profilePictureBase64: profilePictureBase64)
        
        Network.shared.apollo.perform(mutation: UpdateProfilePictureMutation(profilePictureUpdate: profilePictureUpdate)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(UpdateProfilePictureError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.updateProfilePicture else {
                    resultHandler(.failure(UpdateProfilePictureError.baseError(message: "Problem with updating profile picture 1")))
                    
                    return
                }
                
                self.refresh()
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(UpdateProfilePictureError.baseError(message: "Problem with updating profile picture 2")))
            }
        }
    }
}
