//
//  AuthRepository.swift
//  Safie
//
//  Created by Matyáš Procházka on 25.06.2021.
//

import Foundation
import Combine
import Security
import KeychainSwift
import FacebookLogin
import GoogleSignIn
import SwiftUI

enum SignInError: Error {
    case baseError(message: String)
}

enum SignUpError: Error {
    case baseError(message: String)
}

enum ForgottenPasswordError: Error {
    case baseError(message: String)
}

enum ResetPasswordError: Error {
    case baseError(message: String)
}

enum SignInAppleError: Error {
    case baseError(message: String)
}

enum SignInFacebookError: Error {
    case baseError(message: String)
}

enum SignInGoogleError: Error {
    case baseError(message: String)
}

protocol AuthModelProtocol: ObservableObject {
    var isUserSignedIn: Bool { get }
    
    func signIn(email: String, password: String, resultHandler: @escaping (ApiResult<Void?, SignInError>) -> Void) -> Void
    func signUp(email: String, password: String, resultHandler: @escaping (ApiResult<Void?, SignUpError>) -> Void) -> Void
    func forgottenPassword(email: String, resultHandler: @escaping (ApiResult<Void?, ForgottenPasswordError>) -> Void) -> Void
    func resetPassword(email: String, code: String, password: String, resultHandler: @escaping (ApiResult<Void?, ResetPasswordError>) -> Void) -> Void
    
    func signInApple(code: String, resultHandler: @escaping (ApiResult<Void?, SignInAppleError>) -> Void) -> Void
    func signInFacebook(resultHandler: @escaping (ApiResult<Void?, SignInFacebookError>) -> Void) -> Void
    func signInGoogle(resultHandler: @escaping (ApiResult<Void?, SignInGoogleError>) -> Void) -> Void
    
    func refresh(callback: @escaping () -> Void) -> Void
    func logOut() -> Void
}

final class AuthModel: AuthModelProtocol {
    static let keychain = KeychainSwift()
    static let apiTokenKeychainKey = "api_token"
    static let refreshTokenKeychainKey = "refresh_token"
        
    @Published var isUserSignedIn: Bool = false
    
    var token: String?
    var refreshToken: String?
    
    private var refreshing: Bool = false
    private var refreshCallbacks: [() -> Void] = []
    
    init() {
        self.loadTokens()
    }
    
    func signIn(email: String, password: String, resultHandler: @escaping (ApiResult<Void?, SignInError>) -> Void) -> Void {
        Network.shared.apollo.perform(mutation: SignInMutation(email: email, password: password)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SignInError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let token = graphQLResult.data?.login.accessToken, let refreshToken = graphQLResult.data?.login.refreshToken else {
                    resultHandler(.failure(SignInError.baseError(message: "Problem with sign in 1")))
                    
                    return
                }
                
                guard self.saveTokens(token: token, refreshToken: refreshToken) else {
                    resultHandler(.failure(SignInError.baseError(message: "Problem with saving sign in tokens")))
                    
                    return
                }
                                                
                self.isUserSignedIn = true
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SignInError.baseError(message: "Problem with sign in 2")))
            }
        }
    }
    
    func signUp(email: String, password: String, resultHandler: @escaping (ApiResult<Void?, SignUpError>) -> Void) -> Void {
        Network.shared.apollo.perform(mutation: SignUpMutation(email: email, password: password)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SignUpError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let token = graphQLResult.data?.register.accessToken, let refreshToken = graphQLResult.data?.register.refreshToken else {
                    resultHandler(.failure(SignUpError.baseError(message: "Problem with sign up 1")))
                    
                    return
                }
                
                guard self.saveTokens(token: token, refreshToken: refreshToken) else {
                    resultHandler(.failure(SignUpError.baseError(message: "Problem with saving sign up tokens")))
                    
                    return
                }
                                                
                self.isUserSignedIn = true
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SignUpError.baseError(message: "Problem with sign up 2")))
            }
        }
    }
    
    func forgottenPassword(email: String, resultHandler: @escaping (ApiResult<Void?, ForgottenPasswordError>) -> Void) -> Void {
        Network.shared.apollo.perform(mutation: ForgottenPasswordMutation(email: email)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(ForgottenPasswordError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(ForgottenPasswordError.baseError(message: "Problem with forgotten password")))
            }
        }
    }
    
    func resetPassword(email: String, code: String, password: String, resultHandler: @escaping (ApiResult<Void?, ResetPasswordError>) -> Void) -> Void {
        Network.shared.apollo.perform(mutation: ResetPasswordMutation(email: email, code: code, password: password)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(ResetPasswordError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(ResetPasswordError.baseError(message: "Problem with reset password")))
            }
        }
    }
    
    func signInApple(code: String, resultHandler: @escaping (ApiResult<Void?, SignInAppleError>) -> Void) -> Void {
        Network.shared.apollo.perform(mutation: SignInAppleMutation(code: code)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(SignInAppleError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard
                    let token = graphQLResult.data?.appleAuth.accessToken,
                    let refreshToken = graphQLResult.data?.appleAuth.refreshToken
                else {
                    resultHandler(.failure(SignInAppleError.baseError(message: "Problem with apple sign in 1")))
                    
                    return
                }
                
                guard self.saveTokens(token: token, refreshToken: refreshToken) else {
                    resultHandler(.failure(SignInAppleError.baseError(message: "Problem with saving apple sign in tokens")))
                    
                    return
                }
                                                
                self.isUserSignedIn = true
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(SignInAppleError.baseError(message: "Problem with apple sign in 2")))
            }
        }
    }
    
    func signInFacebook(resultHandler: @escaping (ApiResult<Void?, SignInFacebookError>) -> Void) -> Void {
        let manager = LoginManager()
        
        manager.logIn(permissions: ["public_profile", "email"], from: nil) { (result, error) in
            if error != nil {
                resultHandler(.failure(SignInFacebookError.baseError(message: "Problem with facebook sign in 1")))
                
                return
            }
            
            guard let accessToken = result!.token?.tokenString else {
                resultHandler(.failure(SignInFacebookError.baseError(message: "Problem with facebook sign in 2")))
                
                return
            }
            
            manager.logOut()
            
            Network.shared.apollo.perform(mutation: SignInFacebookMutation(accessToken: accessToken)) { result in
                switch result {
                case .success(let graphQLResult):
                    if let errors = graphQLResult.errors {
                        resultHandler(.failure(SignInFacebookError.baseError(message: formatError(graphqlErrors: errors))))

                        return
                    }

                    guard
                        let token = graphQLResult.data?.facebookAuthV2.accessToken,
                        let refreshToken = graphQLResult.data?.facebookAuthV2.refreshToken
                    else {
                        resultHandler(.failure(SignInFacebookError.baseError(message: "Problem with facebook sign in 3")))

                         return
                    }

                    guard self.saveTokens(token: token, refreshToken: refreshToken) else {
                        resultHandler(.failure(SignInFacebookError.baseError(message: "Problem with saving facebook sign in tokens")))

                        return
                    }

                    self.isUserSignedIn = true

                    resultHandler(.success(nil))
                case .failure(let error):
                    ErrorModel.shared.logError(error: error)

                    resultHandler(.failure(SignInFacebookError.baseError(message: "Problem with facebook sign in 4")))
                }
            }
        }
    }
    
    func signInGoogle(resultHandler: @escaping (ApiResult<Void?, SignInGoogleError>) -> Void) {
        let signInConfig = GIDConfiguration.init(clientID: AppConfig.googleClientId, serverClientID: AppConfig.googleServerClientId)
        
        GIDSignIn.sharedInstance.signIn(
            with: signInConfig,
            presenting: (UIApplication.shared.windows.first?.rootViewController)!
        ) { user, error in
            guard error == nil else { return }
            guard let user = user else { return }

            user.authentication.do { authentication, error in
                guard error == nil else { return }
                guard let authentication = authentication else { return }
                guard let idToken = authentication.idToken else { return }
                
                Network.shared.apollo.perform(mutation: SignInGoogleMutation(idToken: idToken)) { result in
                    switch result {
                    case .success(let graphQLResult):
                        if let errors = graphQLResult.errors {
                            resultHandler(.failure(SignInGoogleError.baseError(message: formatError(graphqlErrors: errors))))

                            return
                        }

                        guard
                            let token = graphQLResult.data?.googleAuthV2.accessToken,
                            let refreshToken = graphQLResult.data?.googleAuthV2.refreshToken
                        else {
                            resultHandler(.failure(SignInGoogleError.baseError(message: "Problem with google sign in 1")))

                             return
                        }

                        guard self.saveTokens(token: token, refreshToken: refreshToken) else {
                            resultHandler(.failure(SignInGoogleError.baseError(message: "Problem with saving google sign in tokens")))

                            return
                        }

                        self.isUserSignedIn = true

                        resultHandler(.success(nil))                
                    case .failure(let error):
                        ErrorModel.shared.logError(error: error)

                        resultHandler(.failure(SignInGoogleError.baseError(message: "Problem with google sign in 2")))
                    }
                }
            }
        }
    }
    
    func logOut() {
        self.isUserSignedIn = false
        self.token = nil
        self.refreshToken = nil
        self.refreshing = false
        self.refreshCallbacks.removeAll()                
        
        AuthModel.keychain.clear()
        
        DI.reset()
        Network.reset()
    }
    
    private func saveTokens(token: String, refreshToken: String) -> Bool {
        if AuthModel.keychain.set(token, forKey: AuthModel.apiTokenKeychainKey)
            && AuthModel.keychain.set(refreshToken, forKey: AuthModel.refreshTokenKeychainKey)
        {
            self.token = token
            self.refreshToken = refreshToken
            
            return true;
        }
        
        return false
    }
    
    private func loadTokens() {
        let token = AuthModel.keychain.get(AuthModel.apiTokenKeychainKey)
        let refreshToken = AuthModel.keychain.get(AuthModel.refreshTokenKeychainKey)

        if token != nil && refreshToken != nil {
            self.token = token
            self.refreshToken = refreshToken
            self.isUserSignedIn = true
        }
    }
    
    // Refresh tokens
    // Logout if refresh fails
    func refresh(callback: @escaping () -> Void) {
        // Save the initial request
        refreshCallbacks.append(callback)
        
        if self.refreshing {
            return
        }
        
        self.refreshing = true
        
        Network.shared.apolloRefresh.perform(mutation: RefreshTokensMutation()) { [self] result in
            defer {
                self.refreshing = false
            }
            
            switch result {
            case .success(let graphQLResult):
                if let token = graphQLResult.data?.refresh.accessToken, let refreshToken = graphQLResult.data?.refresh.refreshToken {
                    
                    if self.saveTokens(token: token, refreshToken: refreshToken) {
                        self.afterRefresh()
                    } else {
                        self.logOut()
                    }
                } else {
                    self.logOut()
                }
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                self.logOut()
            }
        }
    }
    
    // Run callkbacks after refresh
    private func afterRefresh() {
        let tmpRefreshCallbacks = self.refreshCallbacks
        
        self.refreshCallbacks.removeAll()
        
        tmpRefreshCallbacks.forEach { callback in
            callback()
        }
    }
}
