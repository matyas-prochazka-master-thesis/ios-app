//
//  PinModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import Foundation
import KeychainSwift

protocol PinModelProtocol: ObservableObject {
    var hasPin: Bool { get set }
    
    func savePin(pin: String) -> Bool
    func checkPin(pin: String) -> Bool
}

final class PinModel: PinModelProtocol {
    static let keychain = KeychainSwift()
    static let pinKey = "pinXY2"
    
    @Published var hasPin: Bool = false
    
    init() {
        let loadedPin = AuthModel.keychain.get(PinModel.pinKey)
    
        if loadedPin != nil {
            self.hasPin = true
        }
    }

    func savePin(pin: String) -> Bool {
        if PinModel.keychain.set(pin, forKey: PinModel.pinKey) {
            self.hasPin = true
            
            return true;
        }
        
        return false
    }
    
    func checkPin(pin: String) -> Bool {
        let loadedPin = AuthModel.keychain.get(PinModel.pinKey)
    
        if loadedPin != nil {
            return loadedPin == pin
        }
        
        return false
    }
}
