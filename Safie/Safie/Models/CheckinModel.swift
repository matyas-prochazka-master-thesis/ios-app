//
//  CheckinModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.04.2022.
//

import Foundation

enum RequestCheckinError: Error {
    case baseError(message: String)
}

enum RequestPeriodcalCheckinError: Error {
    case baseError(message: String)
}

enum CheckinError: Error {
    case baseError(message: String)
}

enum RequestsForMeError: Error {
    case baseError(message: String)
}

enum RequestsIFollowError: Error {
    case baseError(message: String)
}

enum UncheckedCheckinsForMeError: Error {
    case baseError(message: String)
}

enum CheckinRequestError: Error {
    case baseError(message: String)
}

enum CheckinsByRequestError: Error {
    case baseError(message: String)
}

protocol CheckinModelProtocol: ObservableObject {
    var uncheckedCheckinsForMe: [Checkin] { get }
    var refreshingUncheckedCheckinsForMe: Bool { get }
    
    var requestsForMe: [CheckinRequest] { get }
    var refreshingRequestsForMe: Bool { get }
    
    var requestsIFollow: [CheckinRequest] { get }
    var refreshingRequestsIFollow: Bool { get }
    
    func requestCheckin(
        targetAccountId: String,
        resultHandler: @escaping (ApiResult<Void?, RequestCheckinError>) -> Void
    ) -> Void
    
    func requestPeriodicalCheckin(
        targetAccountId: String,
        startsAt: Date,
        endsAt: Date,
        periodInMinutes: Int,
        guardianAccountIds: [String],
        resultHandler: @escaping (ApiResult<Void?, RequestPeriodcalCheckinError>) -> Void
    ) -> Void
    
    func checkin(
        checkinId: String,
        resultHandler: @escaping (ApiResult<Void?, CheckinError>) -> Void
    ) -> Void
    
    func refreshRequestsForMe() -> Void
    func refreshRequestsIFollow() -> Void
    func refreshUncheckedCheckinsForMe() -> Void
    
    func getCheckinRequest(
        checkinRequestId: String,
        resultHandler: @escaping (ApiResult<CheckinRequest, CheckinRequestError>) -> Void
    ) -> Void
    
    func getCheckinsByRequest(
        checkinRequestId: String,
        resultHandler: @escaping (ApiResult<[Checkin], CheckinsByRequestError>) -> Void
    ) -> Void
}

final class CheckinModel: CheckinModelProtocol {
    private var locationModel: LocationModel
    
    @Published var uncheckedCheckinsForMe: [Checkin] = []
    @Published var refreshingUncheckedCheckinsForMe: Bool = false
    
    @Published var requestsForMe: [CheckinRequest] = []
    @Published var refreshingRequestsForMe: Bool = false
    
    @Published var requestsIFollow: [CheckinRequest] = []
    @Published var refreshingRequestsIFollow: Bool = false
    
    init() {
        self.locationModel = DI.shared.container.resolve(LocationModel.self)!
    }
    
    func requestCheckin(
        targetAccountId: String,
        resultHandler: @escaping (ApiResult<Void?, RequestCheckinError>) -> Void
    ) -> Void {
        let checkinRequest = CheckinRequestDto(targetAccountId: targetAccountId)
        
        Network.shared.apollo.perform(mutation: RequestCheckinMutation(checkinRequest: checkinRequest)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(RequestCheckinError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.requestCheckin else {
                    resultHandler(.failure(RequestCheckinError.baseError(message: "Problem with requesting checkin 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(RequestCheckinError.baseError(message: "Problem with requesting checkin 2")))
            }
        }
    }
    
    func requestPeriodicalCheckin(
        targetAccountId: String,
        startsAt: Date,
        endsAt: Date,
        periodInMinutes: Int,
        guardianAccountIds: [String],
        resultHandler: @escaping (ApiResult<Void?, RequestPeriodcalCheckinError>) -> Void
    ) -> Void {
        let periodicalCheckinRequest = CheckinRequestPeriodicalDto(
            targetAccountId: targetAccountId,
            startsAt: DateUtil.shared.toISO8601(date: startsAt)!,
            endsAt: DateUtil.shared.toISO8601(date: endsAt)!,
            periodInMinutes: Double(periodInMinutes),
            guardianAccountIds: guardianAccountIds
        )
        
        Network.shared.apollo.perform(mutation: RequestPeriodicalCheckinMutation(periodicalCheckinRequest: periodicalCheckinRequest)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(RequestPeriodcalCheckinError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.requestPeriodicalCheckin else {
                    resultHandler(.failure(RequestPeriodcalCheckinError.baseError(message: "Problem with requesting periodical checkin 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(RequestPeriodcalCheckinError.baseError(message: "Problem with requesting periodical checkin 2")))
            }
        }
    }
    
    func checkin(
        checkinId: String,
        resultHandler: @escaping (ApiResult<Void?, CheckinError>) -> Void
    ) -> Void {
        let latitude = self.locationModel.latitude
        let longitude = self.locationModel.longitude
        
        let checkin = CheckinDto(checkinId: checkinId, latitude: latitude, longitude: longitude)
        
        Network.shared.apollo.perform(mutation: CheckinMutation(checkin: checkin)) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(CheckinError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let _ = graphQLResult.data?.checkin else {
                    resultHandler(.failure(CheckinError.baseError(message: "Problem with checkin check 1")))
                    
                    return
                }
                
                resultHandler(.success(nil))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(CheckinError.baseError(message: "Problem with checkin check 2")))
            }
        }
    }
    
    func refreshRequestsForMe() -> Void {
        Network.shared.apollo.fetch(query: CheckinRequestsForMeQuery(), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let _ = graphQLResult.errors {
                    return
                }
                
                guard let requestItems = graphQLResult.data?.getRequestsForMe else {
                    return
                }
                
                let requests: [CheckinRequest] = requestItems.compactMap { requestItem in
                    let checkinRequestBasicInfo = requestItem.fragments.checkinRequestBasicInfo
                    let checkinRequestGuardiansInfo = requestItem.fragments.checkinRequestGuardiansInfo
                    let checkinRequestRequesterInfo = requestItem.fragments.checkinRequestRequesterInfo
                    let requesterBasicInfo = checkinRequestRequesterInfo.requester.fragments.accountBasicInfo
                    let requesterProfilePicture = checkinRequestRequesterInfo.requester.fragments.accountProfilePicture
                    
                    var startsAt: Date? = nil
                    var endsAt: Date? = nil
                    var nextCheckinAt: Date? = nil
                    let createdAt = DateUtil.shared.format(date: checkinRequestBasicInfo.createdAt)!
                    
                    if let startsAtString = checkinRequestBasicInfo.startsAt {
                        startsAt = DateUtil.shared.format(date: startsAtString)!
                    }
                    
                    if let endsAtString = checkinRequestBasicInfo.endsAt {
                        endsAt = DateUtil.shared.format(date: endsAtString)!
                    }
                    
                    if let nextCheckinAtString = checkinRequestBasicInfo.nextCheckinAt {
                        nextCheckinAt = DateUtil.shared.format(date: nextCheckinAtString)!
                    }
                    
                    var profilePicture: File? = nil
                    
                    if let profilePictureFile = requesterProfilePicture.profilePicture?.fragments.fileBasicInfo {
                        profilePicture = File(id: profilePictureFile.id, path: profilePictureFile.path)
                    }
                    
                    let requester = Account(
                        id: requesterBasicInfo.id,
                        username: requesterBasicInfo.username,
                        email: requesterBasicInfo.email,
                        profilePicture: profilePicture
                    )
                    
                    let guardians: [Account] = checkinRequestGuardiansInfo.guardians.compactMap { guardian in
                        let guardianBasicInfo = guardian.account.fragments.accountBasicInfo
                        let guardianProfilePicture = guardian.account.fragments.accountProfilePicture
                        
                        var guardianProfilePictureFile: File? = nil
                        
                        if let profilePictureFile = guardianProfilePicture.profilePicture?.fragments.fileBasicInfo {
                            guardianProfilePictureFile = File(id: profilePictureFile.id, path: profilePictureFile.path)
                        }
                        
                        return Account(
                            id: guardianBasicInfo.id,
                            username: guardianBasicInfo.username,
                            email: guardianBasicInfo.email,
                            profilePicture: guardianProfilePictureFile
                        )
                    }
                    
                    return CheckinRequest(
                        id: checkinRequestBasicInfo.id,
                        requester: requester,
                        targetAccount: nil,
                        guardians: guardians,
                        type: checkinRequestBasicInfo.type,
                        state: checkinRequestBasicInfo.state,
                        startsAt: startsAt,
                        endsAt: endsAt,
                        periodInMinutes: checkinRequestBasicInfo.periodInMinutes,
                        nextCheckinAt: nextCheckinAt,
                        createdAt: createdAt
                    )
                }
                
                self.requestsForMe = requests
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
            }
        }
    }
    
    func refreshRequestsIFollow() -> Void {
        Network.shared.apollo.fetch(query: CheckinRequestsIFollowQuery(), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let _ = graphQLResult.errors {
                    return
                }
                
                guard let requestItems = graphQLResult.data?.getRequestsIFollow else {
                    return
                }
                
                let requests: [CheckinRequest] = requestItems.compactMap { requestItem in
                    let checkinRequestBasicInfo = requestItem.fragments.checkinRequestBasicInfo
                    let checkinRequestTargetInfo = requestItem.fragments.checkinRequestTargetInfo
                    let targetBasicInfo = checkinRequestTargetInfo.targetAccount.fragments.accountBasicInfo
                    let targetProfilePicture = checkinRequestTargetInfo.targetAccount.fragments.accountProfilePicture
                    
                    var startsAt: Date? = nil
                    var endsAt: Date? = nil
                    var nextCheckinAt: Date? = nil
                    let createdAt = DateUtil.shared.format(date: checkinRequestBasicInfo.createdAt)!
                    
                    if let startsAtString = checkinRequestBasicInfo.startsAt {
                        startsAt = DateUtil.shared.format(date: startsAtString)!
                    }
                    
                    if let endsAtString = checkinRequestBasicInfo.endsAt {
                        endsAt = DateUtil.shared.format(date: endsAtString)!
                    }
                    
                    if let nextCheckinAtString = checkinRequestBasicInfo.nextCheckinAt {
                        nextCheckinAt = DateUtil.shared.format(date: nextCheckinAtString)!
                    }
                    
                    var profilePicture: File? = nil
                    
                    if let profilePictureFile = targetProfilePicture.profilePicture?.fragments.fileBasicInfo {
                        profilePicture = File(id: profilePictureFile.id, path: profilePictureFile.path)
                    }
                    
                    let target = Account(
                        id: targetBasicInfo.id,
                        username: targetBasicInfo.username,
                        email: targetBasicInfo.email,
                        profilePicture: profilePicture
                    )
                    
                    return CheckinRequest(
                        id: checkinRequestBasicInfo.id,
                        requester: nil,
                        targetAccount: target,
                        guardians: [],
                        type: checkinRequestBasicInfo.type,
                        state: checkinRequestBasicInfo.state,
                        startsAt: startsAt,
                        endsAt: endsAt,
                        periodInMinutes: checkinRequestBasicInfo.periodInMinutes,
                        nextCheckinAt: nextCheckinAt,
                        createdAt: createdAt
                    )
                }
                
                self.requestsIFollow = requests                
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
            }
        }
    }
    
    func refreshUncheckedCheckinsForMe() -> Void {
        Network.shared.apollo.fetch(query: UncheckedCheckinsForMeQuery(), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let _ = graphQLResult.errors {
                    return
                }
                
                guard let checkinItems = graphQLResult.data?.getUncheckedCheckinsForMe else {
                    return
                }
                
                let checkins: [Checkin] = checkinItems.compactMap { checkinItem in
                    let checkinBasicInfo = checkinItem.fragments.checkinBasicInfo
                    let checkinRequestInfo = checkinItem.fragments.checkinRequestInfo
                    let checkinRequestBasicInfo = checkinRequestInfo.request.fragments.checkinRequestBasicInfo
                    let checkinRequestRequesterInfo = checkinRequestInfo.request.fragments.checkinRequestRequesterInfo
                    let requesterBasicInfo = checkinRequestRequesterInfo.requester.fragments.accountBasicInfo
                    let requesterProfilePicture = checkinRequestRequesterInfo.requester.fragments.accountProfilePicture
                         
                    var checkedAt: Date? = nil
                    let createdAt = DateUtil.shared.format(date: checkinBasicInfo.createdAt)!
                    
                    if let checkedAtString = checkinBasicInfo.checkedAt {
                        checkedAt = DateUtil.shared.format(date: checkedAtString)!
                    }
                    
                    var profilePicture: File? = nil
                    
                    if let profilePictureFile = requesterProfilePicture.profilePicture?.fragments.fileBasicInfo {
                        profilePicture = File(id: profilePictureFile.id, path: profilePictureFile.path)
                    }
                    
                    let requester = Account(
                        id: requesterBasicInfo.id,
                        username: requesterBasicInfo.username,
                        email: requesterBasicInfo.email,
                        profilePicture: profilePicture
                    )
                    
                    var requestStartsAt: Date? = nil
                    var requestEndsAt: Date? = nil
                    var requestNextCheckinAt: Date? = nil
                    let requestCreatedAt = DateUtil.shared.format(date: checkinRequestBasicInfo.createdAt)!
                    
                    if let startsAtString = checkinRequestBasicInfo.startsAt {
                        requestStartsAt = DateUtil.shared.format(date: startsAtString)!
                    }
                    
                    if let endsAtString = checkinRequestBasicInfo.endsAt {
                        requestEndsAt = DateUtil.shared.format(date: endsAtString)!
                    }
                    
                    if let nextCheckinAtString = checkinRequestBasicInfo.nextCheckinAt {
                        requestNextCheckinAt = DateUtil.shared.format(date: nextCheckinAtString)!
                    }
                    
                    let request = CheckinRequest(
                        id: checkinRequestBasicInfo.id,
                        requester: requester,
                        targetAccount: nil,
                        guardians: [],
                        type: checkinRequestBasicInfo.type,
                        state: checkinRequestBasicInfo.state,
                        startsAt: requestStartsAt,
                        endsAt: requestEndsAt,
                        periodInMinutes: checkinRequestBasicInfo.periodInMinutes,
                        nextCheckinAt: requestNextCheckinAt,
                        createdAt: requestCreatedAt
                    )
                    
                    return Checkin(
                        id: checkinBasicInfo.id,
                        request: request,
                        state: checkinBasicInfo.state,
                        latitude: checkinBasicInfo.latitude,
                        longitude: checkinBasicInfo.longitude,
                        checkedAt: checkedAt,
                        createdAt: createdAt
                    )
                }
                
                self.uncheckedCheckinsForMe = checkins
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
            }
        }
    }
    
    func getCheckinRequest(
        checkinRequestId: String,
        resultHandler: @escaping (ApiResult<CheckinRequest, CheckinRequestError>) -> Void
    ) -> Void {
        Network.shared.apollo.fetch(query: CheckinRequestQuery(checkinRequestId: checkinRequestId), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(CheckinRequestError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let requestItem = graphQLResult.data?.getRequest else {
                    resultHandler(.failure(CheckinRequestError.baseError(message: "Problem with getting checkin request 1")))
                    
                    return
                }
                
                let checkinRequestBasicInfo = requestItem.fragments.checkinRequestBasicInfo
                let checkinRequestTargetInfo = requestItem.fragments.checkinRequestTargetInfo
                let checkinRequestRequesterInfo = requestItem.fragments.checkinRequestRequesterInfo
                let checkinRequestGuardiansInfo = requestItem.fragments.checkinRequestGuardiansInfo
                
                let targetBasicInfo = checkinRequestTargetInfo.targetAccount.fragments.accountBasicInfo
                let targetProfilePicture = checkinRequestTargetInfo.targetAccount.fragments.accountProfilePicture
                
                let requesterBasicInfo = checkinRequestRequesterInfo.requester.fragments.accountBasicInfo
                let requesterProfilePicture = checkinRequestRequesterInfo.requester.fragments.accountProfilePicture
                
                var startsAt: Date? = nil
                var endsAt: Date? = nil
                var nextCheckinAt: Date? = nil
                let createdAt = DateUtil.shared.format(date: checkinRequestBasicInfo.createdAt)!
                
                if let startsAtString = checkinRequestBasicInfo.startsAt {
                    startsAt = DateUtil.shared.format(date: startsAtString)!
                }
                
                if let endsAtString = checkinRequestBasicInfo.endsAt {
                    endsAt = DateUtil.shared.format(date: endsAtString)!
                }
                
                if let nextCheckinAtString = checkinRequestBasicInfo.nextCheckinAt {
                    nextCheckinAt = DateUtil.shared.format(date: nextCheckinAtString)!
                }
                
                var targetProfilePictureFile: File? = nil
                
                if let profilePictureFile = targetProfilePicture.profilePicture?.fragments.fileBasicInfo {
                    targetProfilePictureFile = File(id: profilePictureFile.id, path: profilePictureFile.path)
                }
                
                let target = Account(
                    id: targetBasicInfo.id,
                    username: targetBasicInfo.username,
                    email: targetBasicInfo.email,
                    profilePicture: targetProfilePictureFile
                )
                
                var requesterProfilePictureFile: File? = nil
                
                if let profilePictureFile = requesterProfilePicture.profilePicture?.fragments.fileBasicInfo {
                    requesterProfilePictureFile = File(id: profilePictureFile.id, path: profilePictureFile.path)
                }
                
                let requester = Account(
                    id: requesterBasicInfo.id,
                    username: requesterBasicInfo.username,
                    email: requesterBasicInfo.email,
                    profilePicture: requesterProfilePictureFile
                )
                
                let guardians: [Account] = checkinRequestGuardiansInfo.guardians.compactMap { guardian in
                    let guardianAccountBasicInfo = guardian.account.fragments.accountBasicInfo
                    let guardianAccountProfilePicture = guardian.account.fragments.accountProfilePicture
                    
                    var guardianProfilePictureFile: File? = nil
                    
                    if let profilePictureFile = guardianAccountProfilePicture.profilePicture?.fragments.fileBasicInfo {
                        guardianProfilePictureFile = File(id: profilePictureFile.id, path: profilePictureFile.path)
                    }
                    
                    return Account(
                        id: guardianAccountBasicInfo.id,
                        username: guardianAccountBasicInfo.username,
                        email: guardianAccountBasicInfo.email,
                        profilePicture: guardianProfilePictureFile
                    )
                }
                
                let checkinRequest = CheckinRequest(
                    id: checkinRequestBasicInfo.id,
                    requester: requester,
                    targetAccount: target,
                    guardians: guardians,
                    type: checkinRequestBasicInfo.type,
                    state: checkinRequestBasicInfo.state,
                    startsAt: startsAt,
                    endsAt: endsAt,
                    periodInMinutes: checkinRequestBasicInfo.periodInMinutes,
                    nextCheckinAt: nextCheckinAt,
                    createdAt: createdAt
                )
                
                
                resultHandler(.success(checkinRequest))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(CheckinRequestError.baseError(message: "Problem with getting checkin request 2")))
            }
        }
    }
    
    func getCheckinsByRequest(
        checkinRequestId: String,
        resultHandler: @escaping (ApiResult<[Checkin], CheckinsByRequestError>) -> Void
    ) -> Void {
        Network.shared.apollo.fetch(query: CheckinsByRequestQuery(checkinRequestId: checkinRequestId), cachePolicy: .fetchIgnoringCacheCompletely) { result in
            switch result {
            case .success(let graphQLResult):
                if let errors = graphQLResult.errors {
                    resultHandler(.failure(CheckinsByRequestError.baseError(message: formatError(graphqlErrors: errors))))
                    
                    return
                }
                
                guard let checkinItems = graphQLResult.data?.getCheckinsByRequest else {
                    resultHandler(.failure(CheckinsByRequestError.baseError(message: "Problem with getting checkins by request 1")))
                    
                    return
                }
                
                let checkins: [Checkin] = checkinItems.compactMap { checkinItem in
                    let checkinBasicInfo = checkinItem.fragments.checkinBasicInfo
                         
                    var checkedAt: Date? = nil
                    let createdAt = DateUtil.shared.format(date: checkinBasicInfo.createdAt)!
                    
                    if let checkedAtString = checkinBasicInfo.checkedAt {
                        checkedAt = DateUtil.shared.format(date: checkedAtString)!
                    }
                    
                    return Checkin(
                        id: checkinBasicInfo.id,
                        request: nil,
                        state: checkinBasicInfo.state,
                        latitude: checkinBasicInfo.latitude,
                        longitude: checkinBasicInfo.longitude,
                        checkedAt: checkedAt,
                        createdAt: createdAt
                    )
                }
                
                
                resultHandler(.success(checkins))
            case .failure(let error):
                ErrorModel.shared.logError(error: error)
                
                resultHandler(.failure(CheckinsByRequestError.baseError(message: "Problem with getting checkins by request 2")))
            }
        }
    }
}
