//
//  AppConfig.swift
//  Safie
//
//  Created by Matyáš Procházka on 16.08.2021.
//

import Foundation

// FILL THE RIGHT INFO
enum AppConfig {
    static let prod: Bool = true
    
    // GQL endpoint for requests
    static let gqlUrl: URL = URL(string: AppConfig.prod ? "https://mattproch-safie.herokuapp.com/graphql" : "http://192.168.1.173:3000/graphql")!
    // Public S3 files
    static let staticUrl: String = "https://mattproch-safie.s3.eu-central-1.amazonaws.com/"
    
    static let fbAppId = ""
    static let googleClientId = ""
    static let googleServerClientId = ""
}
