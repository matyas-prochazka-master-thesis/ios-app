//
//  OnboardingScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 27.02.2022.
//

import SwiftUI

struct OnboardingScreen: View {
    @StateObject var onboardingViewModel = OnboardingViewModel()
    
    @State var tab: Int = 0
    
    var body: some View {
        VStack(spacing: 0) {
            Logo()
                .frame(height: 20)
                .padding(.top, .extraLarge)
            
            TabView(selection: self.$tab) {
                OnboardinGuardiansStep(
                    next: { withAnimation { self.tab = 1 } }
                )
                .tag(0)
                
                OnboardingSosStep(
                    next: { withAnimation { self.tab = 2 } },
                    previous: { withAnimation { self.tab = 0 } }
                )
                .tag(1)
                
                OnboardingNotificationsStep(
                    next: {
                        self.onboardingViewModel.askForNotificationAuthorization()
                        withAnimation { self.tab = 3 }
                    },
                    previous: { withAnimation { self.tab = 1 } },
                    askForNotificationAuthorization: {
                        self.onboardingViewModel.askForNotificationAuthorization()
                    }
                )
                .tag(2)
                
                OnboardingLocationStep(
                    done: {
                        self.onboardingViewModel.locationModel.requestLocationAuthorzation()
                        withAnimation { self.onboardingViewModel.appModel.setOnboardingShowed() }
                    },
                    previous: { withAnimation { self.tab = 2 } },
                    askForLocationAuthorization: {
                        self.onboardingViewModel.locationModel.requestLocationAuthorzation()
                    }
                )
                .tag(3)
            }
            .tabViewStyle(.page(indexDisplayMode: .never))
            .introspectPagedTabView { collectionView, scrollView in
                collectionView.isScrollEnabled = false
                scrollView.isScrollEnabled = false
                collectionView.clipsToBounds = false
            }
            .padding(.top, .large)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            
            HStack {
                ForEach(0..<4) { index in
                    RoundedRectangle(cornerRadius: 3)
                        .fill(self.tab == index ? .white : Color("Label"))
                        .frame(width: self.tab == index ? 20 : 6, height: 6)
                }
            }
            .padding(.top, .small)
        }
    }
}

struct OnboardingScreen_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingScreen()
    }
}
