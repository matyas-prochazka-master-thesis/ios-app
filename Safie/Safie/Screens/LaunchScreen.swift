//
//  LaunchScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 27.02.2022.
//

import SwiftUI

struct LaunchScreen: View {    
    var body: some View {
        VStack(alignment: .center) {
            Logo()
                .frame(width: 150, height: 100)
            
            Text("launch.motto")
                .bodyTextStyle()
                .padding(.top, .extraLarge)
            
            LottieAnimation(
                filename: "lottie-check",
                animationSpeed: 1,
                loop: false
            )
                .frame(width: 200, height: 150)
                .padding(.top, .large)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct LaunchScreen_Previews: PreviewProvider {
    static var previews: some View {
        LaunchScreen()
    }
}
