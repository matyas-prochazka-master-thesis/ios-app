//
//  GuardianAddScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import SwiftUI

struct GuardianAddScreen: View {
    @StateObject var guardianAddViewModel: GuardianAddViewModel = GuardianAddViewModel()
    
    @Environment(\.presentationMode) var presentationMode
    
    enum Field: Hashable {
        case none
        case username
    }
    
    @FocusState var focusedField: Field?
    
    var body: some View {
        VStack {
            Spacer()
            
            Text("Add by username")
                .h1TextStyle()
                .padding(.bottom, .large)
            
            if let error = self.guardianAddViewModel.globalError {
                FormError(error: Text(error))
            }
                        
            FormLabel(label: Text("Username"))
                .padding(.top, .normal)
            
            TextField("Username", text: .init(
                get: { [guardianAddViewModel] in guardianAddViewModel.username.value },
                set: { [guardianAddViewModel] in guardianAddViewModel.username.value = $0 }
            ))
            .focused($focusedField, equals: .username)
            .textFieldStyle(BasicTextFieldStyle())
            .padding(.top, .extraSmall)
            
            if self.guardianAddViewModel.username.touched {
                ForEach(self.guardianAddViewModel.username.errors, id: \.self) { error in
                    FormError(error: Text(error))
                }
            }
            
            Spacer()
            
            ActionButton(
                title: Text("Add"),
                disabled: !self.guardianAddViewModel.username.valid
            ) {
                self.guardianAddViewModel.addGuardian()
            }
            .animation(.default, value: self.guardianAddViewModel.username.value)
        }
        .padding(.all, .normal)
        .onChange(of: self.guardianAddViewModel.addSuccess) { _ in
            self.presentationMode.wrappedValue.dismiss()
        }
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)) {
                self.focusedField = .username
            }
        }
        .navigationTitle("Add guardian")
    }
}

struct GuardianAddScreen_Previews: PreviewProvider {
    static var previews: some View {
        GuardianAddScreen()
    }
}
