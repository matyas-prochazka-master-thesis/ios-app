//
//  GuardianRequestsScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import SwiftUI

struct GuardianRequestsScreen: View {
    @StateObject var guardianRequestsViewModel: GuardianRequestsViewModel = GuardianRequestsViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                ForEach(self.guardianRequestsViewModel.guardianModel.guardianRequests) { guardianRequest in
                    if let account = guardianRequest.account {
                        HStack {
                            Text(account.username ?? account.email)
                                .bodyTextStyle()
                            
                            Spacer()
                            
                            Text("Accept")
                                .body2TextStyle()
                            
                            Button(action: {
                                self.guardianRequestsViewModel.acceptGuardian(guardian: guardianRequest)
                            }) {
                                ZStack {
                                    Circle()
                                        .fill(Color("Success"))
                                        .frame(width: 40, height: 40)
                                    
                                    if self.guardianRequestsViewModel.acceptingGuardians.contains(guardianRequest.id) {
                                        ProgressView()
                                    } else {
                                        Image(systemName: "checkmark")
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 20, height: 20)
                                    }
                                }
                            }
                        }
                        .foregroundColor(.white)
                        .padding(.all, .normal)
                        .background(Color("DarkGray"))
                        .cornerRadius(.small)
                        .padding(.top, .small)
                    }
                }
            }
            .padding(.all, .normal)
        }
        .navigationTitle("Guardian requests")
    }
}

struct GuardianRequestsScreen_Previews: PreviewProvider {
    static var previews: some View {
        GuardianRequestsScreen()
    }
}
