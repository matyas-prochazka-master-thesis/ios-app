//
//  GuardiansScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import SwiftUI

struct GuardiansScreen: View {
    @StateObject var guardiansViewModel: GuardiansViewModel = GuardiansViewModel()
    
    var body: some View {
        VStack {
            List {
                NavigationLink(destination: GuardianRequestsScreen()) {
                    GuardianListLinkItem(
                        icon: "person.fill.questionmark",
                        label: Text("Guardian requests (\(self.guardiansViewModel.guardianModel.guardianRequests.count))")
                    )
                }
                
                ForEach(self.guardiansViewModel.guardianModel.guardians) { guardian in
                    GuardianListItem(guardian: guardian)                    
                }
                .onDelete(perform: self.guardiansViewModel.removeGuardians)
                
                NavigationLink(destination: GuardianAddScreen()) {
                    GuardianListLinkItem(icon: "plus", label: Text("Add guardian"))
                }
            }
            .padding(.top, .normal)
        }
        .navigationTitle("Guardians")
    }
}

struct GuardiansScreen_Previews: PreviewProvider {
    static var previews: some View {
        GuardiansScreen()
    }
}
