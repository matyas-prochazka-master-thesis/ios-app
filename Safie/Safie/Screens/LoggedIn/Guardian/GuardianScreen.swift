//
//  GuardianScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import SwiftUI
import Kingfisher

// Not used right now
struct GuardianScreen: View {
    @StateObject var guardianViewModel: GuardianViewModel
    
    init(guardianId: String, guardian: Guardian? = nil) {
        self._guardianViewModel = StateObject(wrappedValue: GuardianViewModel(guardianId: guardianId, guardian: guardian))
        
        UINavigationBar.appearance().tintColor = .white
    }
    
    var body: some View {
        GeometryReader { geo in
            ScrollView {
                self.header
                    .frame(maxWidth: .infinity)
                    .frame(height: 250 + geo.safeAreaInsets.top)
                
                if let guardian = self.guardianViewModel.guardian {
                    VStack {
                        HStack {
                            Text("Guardian since:")
                                .bodyTextStyle()
                            
                            Spacer()
                            
                            Text(DateUtil.shared.relative(date: guardian.acceptedAt!) ?? "")
                                .bodyTextStyle()
                        }
                    }
                    .padding(.all, .normal)
                }
            }
            .coordinateSpace(name: "SCROLL")
            .ignoresSafeArea(.container, edges: .vertical)
        }
        //.navigationBarBackButtonHidden(true)
        .navigationBarTitleDisplayMode(.inline)
    }
    
    var header: some View {
        GeometryReader { geo in
            let minY = geo.frame(in: .named("SCROLL")).minY
            let size = geo.size
            let height = (size.height + minY)
            
            ZStack {
                if let guardian = self.guardianViewModel.guardian?.guardian {
                    if let profilePicture = guardian.profilePicture {
                        KFImage(URL(string: profilePicture.absoluteUrl)!)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: size.width, height: height, alignment: .center)
                            .clipped()
                    }
                    
                    LinearGradient(gradient: Gradient(colors: [.black.opacity(0.2), .black.opacity(0.8)]), startPoint: .top, endPoint: .bottom)
                
                    VStack {
                        Spacer()
                        
                        HStack {
                            Text(guardian.username ?? guardian.email)
                                .h2TextStyle()
                            
                            Spacer()
                        }
                        .padding(.all, .normal)
                    }
                }
            }
            .cornerRadius(.normal)
            .frame(width: size.width, height: height)
            .clipped()
            .offset(y: -minY)
        }
    }
}

//struct GuardianScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        GuardianScreen()
//    }
//}
