//
//  PinChangeScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import SwiftUI

struct PinChangeScreen: View {
    @StateObject var pinChangeViewModel: PinChangeViewModel = PinChangeViewModel()
    
    @Environment(\.presentationMode) var presentationMode
    
    enum Field: Hashable {
        case none
        case oldPin
        case pin
    }
    
    @FocusState var focusedField: Field?
    
    var body: some View {
        if !self.pinChangeViewModel.oldPinMatched {
            VStack {
                Spacer()
                
                Text("Enter your current PIN")
                    .h1TextStyle()
                    .padding(.bottom, .large)

                HStack {
                    PinLetter(letter: self.pinChangeViewModel.oldPin[0])
                    PinLetter(letter: self.pinChangeViewModel.oldPin[1])
                    PinLetter(letter: self.pinChangeViewModel.oldPin[2])
                    PinLetter(letter: self.pinChangeViewModel.oldPin[3])
                }
                .onTapGesture {
                    self.focusedField = .pin
                }
                .padding(.top, .normal)
                
                TextField("Pin", text: self.$pinChangeViewModel.oldPin)
                    .focused($focusedField, equals: .oldPin)
                    .keyboardType(.numberPad)
                    .frame(height: 0)
                    .opacity(0)
                
                if let error = self.pinChangeViewModel.oldPinError {
                    FormError(error: Text(error))
                }
                        
                Spacer()
                
                ActionButton(
                    title: Text("Check"),
                    disabled: self.pinChangeViewModel.oldPin.count != 4
                ) {
                    self.pinChangeViewModel.checkOldPin()                    
                }
                .animation(.default, value: self.pinChangeViewModel.oldPin.count)
            }
            .padding(.all, .normal)
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)) {
                    self.focusedField = .oldPin
                }
            }
        } else {
            VStack {
                Spacer()
                
                Text("Set your new unique PIN")
                    .h1TextStyle()
                    .padding(.bottom, .large)

                HStack {
                    PinLetter(letter: self.pinChangeViewModel.pin[0])
                    PinLetter(letter: self.pinChangeViewModel.pin[1])
                    PinLetter(letter: self.pinChangeViewModel.pin[2])
                    PinLetter(letter: self.pinChangeViewModel.pin[3])
                }
                .onTapGesture {
                    self.focusedField = .pin
                }
                .padding(.top, .normal)
                
                TextField("Pin", text: self.$pinChangeViewModel.pin)
                    .focused($focusedField, equals: .pin)
                    .keyboardType(.numberPad)
                    .frame(height: 0)
                    .opacity(0)
                
                if let error = self.pinChangeViewModel.pinError {
                    FormError(error: Text(error))
                }
                        
                Spacer()
                
                ActionButton(
                    title: Text("Set"),
                    disabled: self.pinChangeViewModel.pin.count != 4
                ) {
                    self.pinChangeViewModel.setPin()
                }
                .animation(.default, value: self.pinChangeViewModel.pin.count)
            }
            .padding(.all, .normal)
            .onAppear {
                self.focusedField = .pin
            }
            .onChange(of: self.pinChangeViewModel.changeSuccess) { _ in
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

struct PinChangeScreen_Previews: PreviewProvider {
    static var previews: some View {
        PinChangeScreen()
    }
}
