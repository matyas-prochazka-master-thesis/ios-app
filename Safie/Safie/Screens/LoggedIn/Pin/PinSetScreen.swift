//
//  PinSetScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import SwiftUI

struct PinSetScreen: View {
    @StateObject var pinSetViewModel: PinSetViewModel = PinSetViewModel()
    
    enum Field: Hashable {
        case none
        case pin
    }
    
    @FocusState var focusedField: Field?
    
    var body: some View {
        VStack {
            Spacer()
            
            Text("Set your unique PIN")
                .h1TextStyle()
                .padding(.bottom, .large)

            HStack {
                PinLetter(letter: self.pinSetViewModel.pin[0])
                PinLetter(letter: self.pinSetViewModel.pin[1])
                PinLetter(letter: self.pinSetViewModel.pin[2])
                PinLetter(letter: self.pinSetViewModel.pin[3])
            }
            .onTapGesture {
                self.focusedField = .pin
            }
            .padding(.top, .normal)            
            
            TextField("Pin", text: self.$pinSetViewModel.pin)
                .focused($focusedField, equals: .pin)
                .keyboardType(.numberPad)
                .frame(height: 0)
                .opacity(0)
            
            if let error = self.pinSetViewModel.pinError {
                FormError(error: Text(error))
            }
                    
            Spacer()
            
            ActionButton(
                title: Text("Set"),
                disabled: self.pinSetViewModel.pin.count != 4
            ) {
                self.pinSetViewModel.setPin()
            }
            .animation(.default, value: self.pinSetViewModel.pin.count)
        }
        .padding(.all, .normal)
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)) {
                self.focusedField = .pin
            }
        }
    }
}

struct PinSetScreen_Previews: PreviewProvider {
    static var previews: some View {
        PinSetScreen()
    }
}
