//
//  CheckinsScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import SwiftUI
import Kingfisher

struct CheckinsScreen: View {
    @StateObject var checkinsViewModel: CheckinsViewModel = CheckinsViewModel()
    
    @State var tab: Int = 1
    
    var body: some View {
        GeometryReader { geo in
            VStack(spacing: 0) {
                HStack(spacing: 0) {
                    VStack {
                        Text("Requests\nfor me")
                            .h4TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.small)
                            .frame(maxWidth: .infinity)
                            .background(
                                RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                    .fill(self.tab == 0 ? Color("PrimaryAction") : Color("DarkGray"))
                            )
                    }
                    .onTapGesture {
                        withAnimation {
                            self.tab = 0
                        }
                    }
                    .padding(.small)
                    .frame(width: geo.size.width / 3)
                    
                    VStack {
                        Text("Unchecked\ncheckins")
                            .h4TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.small)
                            .frame(maxWidth: .infinity)
                            .background(
                                RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                    .fill(self.tab == 1 ? Color("PrimaryAction") : Color("DarkGray"))
                            )
                    }
                    .onTapGesture {
                        withAnimation {
                            self.tab = 1
                        }
                    }
                    .padding(.small)
                    .frame(width: geo.size.width / 3)
                    
                    VStack {
                        Text("Requests\nI Follow")
                            .h4TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.small)
                            .frame(maxWidth: .infinity)
                            .background(
                                RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                    .fill(self.tab == 2 ? Color("PrimaryAction") : Color("DarkGray"))
                            )
                    }
                    .onTapGesture {
                        withAnimation {
                            self.tab = 2
                        }
                    }
                    .padding(.small)
                    .frame(width: geo.size.width / 3)
                }
                
                TabView(selection: self.$tab) {
                    ScrollView {
                        VStack {
                            ForEach(self.checkinsViewModel.checkinModel.requestsForMe) { request in
                                NavigationLink(destination: CheckinRequestDetailScreen(checkinRequestId: request.id, checkinRequest: request)) {
                                    RequestForMeItem(
                                        request: request
                                    )
                                }
                                .padding(.top, .small)
                            }
                        }
                        .padding(.normal)
                    }
                    .tag(0)
                    
                    VStack {
                        if self.checkinsViewModel.checkinModel.uncheckedCheckinsForMe.isEmpty {
                            ZStack {
                                Circle()
                                    .fill(Color("Success"))
                                    .frame(width: 100, height: 100)
                                                                
                                Image(systemName: "checkmark")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 30, height: 30)
                                    .foregroundColor(.white)
                            }
                            
                            Text("No unchecked checkins")
                                .bodyTextStyle()
                                .padding(.top, .normal)
                        } else {
                            ScrollView {
                                VStack {
                                    ForEach(self.checkinsViewModel.checkinModel.uncheckedCheckinsForMe) { checkin in
                                        UncheckedCheckinItem(
                                            checkin: checkin,
                                            checkCheckin: {
                                                self.checkinsViewModel.checkin(checkin: checkin)
                                            },
                                            checking: self.checkinsViewModel.checkingCheckins.contains(checkin.id)
                                        )
                                        .padding(.top, .small)
                                    }
                                }
                                .padding(.normal)
                            }
                        }
                    }
                    .tag(1)
                    
                    ScrollView {
                        VStack {
                            ForEach(self.checkinsViewModel.checkinModel.requestsIFollow) { request in
                                NavigationLink(destination: CheckinRequestDetailScreen(checkinRequestId: request.id, checkinRequest: request)) {
                                    RequestIFollowItem(
                                        request: request
                                    )
                                }
                                .padding(.top, .small)
                            }
                        }
                        .padding(.normal)                        
                    }
                    .tag(2)
                    
                    VStack {
                        Text("Coming soon")
                    }
                    .tag(2)
                }
                .tabViewStyle(.page(indexDisplayMode: .never))
                .introspectPagedTabView { collectionView, scrollView in
                    collectionView.isScrollEnabled = false
                    scrollView.isScrollEnabled = false
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
        }
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct CheckinsScreen_Previews: PreviewProvider {
    static var previews: some View {
        CheckinsScreen()
    }
}
