//
//  RequestCheckinScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.04.2022.
//

import SwiftUI
import Kingfisher

struct RequestCheckinScreen: View {
    @StateObject var requestCheckinViewModel: RequestCheckinViewModel = RequestCheckinViewModel()
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        GeometryReader { geo in
            VStack(spacing: 0) {
                Spacer()
                
                LazyVGrid(
                    columns: [
                        GridItem(.flexible()),
                        GridItem(.flexible()),
                        GridItem(.flexible())
                    ]
                ) {
                    ForEach(self.requestCheckinViewModel.guardianModel.guardians) { guardian in
                        let selected = self.requestCheckinViewModel.selectedGuardian == guardian
                        
                        VStack {
                            Group {
                                if let profilePicture = guardian.guardian?.profilePicture {
                                    KFImage(URL(string: profilePicture.absoluteUrl)!)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                } else {
                                    Circle()
                                        .fill(Color("Label"))
                                }
                            }
                            .frame(
                                width: geo.size.width / 4,
                                height: geo.size.width / 4
                            )
                            .cornerRadius(geo.size.width / 6)
                            .clipped()
                            .grayscale(selected ? 0 : 1)
                            
                            Text(formatDisplayName(guardian.guardian!))
                                .body2TextStyle()
                                .multilineTextAlignment(.center)
                                .padding(.top, .small)
                                .foregroundColor(.white)
                                .colorMultiply(selected ? Color("PrimaryAction") : .white)
                        }
                        .padding(.bottom, .small)
                        .onTapGesture {
                            hhaptic()
                            
                            withAnimation {
                                self.requestCheckinViewModel.toggleGuardian(guardian: guardian)
                            }
                        }
                    }
                }
                .padding([.top, .bottom], .normal)
                
                Spacer()
                
                HStack(spacing: 0) {
                    VStack {
                        Text("One time")
                            .h4TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.small)
                            .frame(maxWidth: .infinity)
                            .background(
                                RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                    .fill(self.requestCheckinViewModel.checkinRequestType == .oneTime ? Color("PrimaryAction") : Color("DarkGray"))
                            )
                    }
                    .onTapGesture {
                        withAnimation {
                            self.requestCheckinViewModel.checkinRequestType = .oneTime
                        }
                    }
                    .padding(.small)
                    .frame(width: geo.size.width / 3)
                    
                    VStack {
                        Text("Periodical")
                            .h4TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.small)
                            .frame(maxWidth: .infinity)
                            .background(
                                RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                    .fill(self.requestCheckinViewModel.checkinRequestType == .periodical ? Color("PrimaryAction") : Color("DarkGray"))
                            )
                    }
                    .onTapGesture {
                        withAnimation {
                            self.requestCheckinViewModel.checkinRequestType = .periodical
                        }
                    }
                    .padding(.small)
                    .frame(width: geo.size.width / 3)
                }
                
                if self.requestCheckinViewModel.checkinRequestType == .periodical {
                    VStack {
                        VStack(alignment: .leading) {
                            let startsRange = Date()...Calendar.current.date(byAdding: .day, value: 2, to: Date())!
                            let endsRange = self.requestCheckinViewModel.startsAt...Calendar.current.date(byAdding: .hour, value: 12, to: self.requestCheckinViewModel.startsAt)!
                            
                            DatePicker(
                                selection: self.$requestCheckinViewModel.startsAt,
                                in: startsRange,
                                label: {
                                    Text("Starting at")
                                        .body2TextStyle()
                                }
                            )
                            
                            DatePicker(
                                selection: self.$requestCheckinViewModel.endsAt,
                                in: endsRange,
                                label: {
                                    Text("Ending at")
                                        .body2TextStyle()
                                }
                            )
                            
                            Text("Max 12 hours in next 24 hours")
                                .body2LabelTextStyle()
                        }
                        .padding([.leading, .trailing], .normal)
                        
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack {
                                ForEach([5, 10, 15, 30, 60], id: \.self) { minutes in
                                    RequestCheckinPeriodItem(
                                        minutes: minutes,
                                        active: self.requestCheckinViewModel.periodInMinutes == minutes
                                    )
                                    .onTapGesture {
                                        withAnimation {
                                            self.requestCheckinViewModel.periodInMinutes = minutes
                                        }
                                    }
                                }
                            }
                            .padding([.leading, .trailing], .normal)
                        }
                        .padding(.top, .small)
                        
                        if let error = self.requestCheckinViewModel.periodicalRequestError {
                            Text(error)
                                .body2DangerTextStyle()
                                .padding(.top, .small)
                        }
                    }
                    .padding(.top, .normal)
                }
                
                VStack {
                    ActionButton(
                        title: Text("Request"),
                        disabled: self.requestCheckinViewModel.selectedGuardian == nil
                    ) {
                        self.requestCheckinViewModel.requestCheckin()
                    }
                }
                .padding(.large)
            }
        }
        .onChange(of: self.requestCheckinViewModel.requestSuccess) { _ in
            self.presentationMode.wrappedValue.dismiss()
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("Request Checkin")
    }
}

struct RequestCheckinScreen_Previews: PreviewProvider {
    static var previews: some View {
        RequestCheckinScreen()
    }
}
