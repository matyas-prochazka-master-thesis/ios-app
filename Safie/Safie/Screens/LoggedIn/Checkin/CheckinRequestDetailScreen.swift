//
//  CheckinRequestDetail.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import SwiftUI

struct CheckinRequestDetailScreen: View {
    @StateObject var checkinRequestDetailViewModel: CheckinRequestDetailViewModel
    
    init(checkinRequestId: String, checkinRequest: CheckinRequest? = nil) {
        self._checkinRequestDetailViewModel = StateObject(
            wrappedValue: CheckinRequestDetailViewModel(checkinRequestId: checkinRequestId, checkinRequest: checkinRequest)
        )
    }
    
    var body: some View {
        GeometryReader { geo in
            VStack {
                if let request = self.checkinRequestDetailViewModel.checkinRequest {
                    HStack {
                        VStack {
                            Text("Requester")
                                .body2LabelTextStyle()
                            
                            if let requester = request.requester {
                                AccountAvatar(account: requester)
                                    .padding(.top, .small)
                                
                                Text(formatDisplayName(requester))
                                    .bodyTextStyle()
                                    .padding(.top, .small)
                            } else {
                                ProgressView()
                            }
                        }
                        .frame(width: geo.size.width / 2)
                        
                        VStack {
                            Text("Target")
                                .body2LabelTextStyle()
                            
                            if let targetAccount = request.targetAccount {
                                AccountAvatar(account: targetAccount)
                                    .padding(.top, .small)
                                
                                Text(formatDisplayName(targetAccount))
                                    .bodyTextStyle()
                                    .padding(.top, .small)
                            } else {
                                ProgressView()
                            }
                        }
                        .frame(width: geo.size.width / 2)
                    }
                    .padding(.top, .large)
                    
                    Text("Created at \(DateUtil.shared.dateTime(date: request.createdAt)!)")
                        .body2LabelTextStyle()
                        .padding(.top, .normal)
                    
                    switch request.type {
                    case .oneTime:
                        Text("One time")
                            .body2TextStyle()
                    case .periodical:
                        Text("Periodical")
                            .body2TextStyle()
                    default:
                        EmptyView()
                    }
                    
                    ScrollView {
                        VStack {
                            ForEach(self.checkinRequestDetailViewModel.checkins) { checkin in
                                CheckinItem(checkin: checkin)
                                    .padding(.top, .small)
                            }
                        }
                        .padding(.normal)
                    }
                } else {
                    ProgressView()
                }
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("Request detail")
    }
}

//struct CheckinRequestDetailScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        CheckinRequestDetailScreen()
//    }
//}
