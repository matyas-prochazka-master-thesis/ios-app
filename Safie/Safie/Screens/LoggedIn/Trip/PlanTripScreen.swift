//
//  PlanTripScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import SwiftUI

struct PlanTripScreen: View {
    @StateObject var planTripViewModel: PlanTripViewModel = PlanTripViewModel()
    
    var body: some View {
        VStack(alignment: .leading) {
            Spacer()
                        
            let shouldArriveAtRange = Date()...Calendar.current.date(byAdding: .hour, value: 12, to: Date())!
            
            // Arrival time
            ToggleInput(
                label: Text("Arrival time?"),
                isOn: self.planTripViewModel.withShouldArriveAt,
                onSelect: {
                    withAnimation {
                        self.planTripViewModel.withShouldArriveAt = true
                    }
                },
                onDeselect: {
                    withAnimation {
                        self.planTripViewModel.withShouldArriveAt = false
                    }
                }
            )
            
            if self.planTripViewModel.withShouldArriveAt {
                DatePicker(
                    selection: self.$planTripViewModel.shouldArriveAt,
                    in: shouldArriveAtRange,
                    label: {
                        Text("Should arrive at:")
                            .body2LabelTextStyle()
                    }
                )
            }
            
            // Destination
            ToggleInput(
                label: Text("Destination?"),
                isOn: self.planTripViewModel.withDestination,
                onSelect: {
                    withAnimation {
                        self.planTripViewModel.withDestination = true
                    }
                },
                onDeselect: {
                    withAnimation {
                        self.planTripViewModel.withDestination = false
                        self.planTripViewModel.selectedDestination = nil
                        self.planTripViewModel.selectedCreateDestination = nil
                    }
                }
            )
            .padding(.top, .normal)
            
            if self.planTripViewModel.withDestination {
                NavigationLink(destination: SelectDestinationScreen(
                    selectedDestination: self.$planTripViewModel.selectedDestination,
                    selectedCreateDestination: self.$planTripViewModel.selectedCreateDestination
                )) {
                    if let selectedDestination = self.planTripViewModel.selectedDestination {
                        ChevronLink(label: Text(selectedDestination.name), icon: "mappin")
                    } else if let selectedCreateDestination = self.planTripViewModel.selectedCreateDestination {
                        ChevronLink(label: Text(selectedCreateDestination.name), icon: "mappin")
                    } else {
                        ChevronLink(label: Text("Select destination"), icon: "plus")
                    }
                }
            }
            
            // Plan trip
            NavigationLink(destination: SelectQuickActionsScreen(selectedQuickActions: self.$planTripViewModel.routePlan)) {
                if self.planTripViewModel.routePlan.count == 0 {
                    ChevronLink(label: Text("Plan your route"), icon: "point.topleft.down.curvedto.point.bottomright.up")
                } else {
                    ChevronLink(label: Text("\(self.planTripViewModel.routePlan.count) actions planned"), icon: "point.topleft.down.curvedto.point.bottomright.up")
                }
            }
            .padding(.top, .small)
                
//                ScrollView(.horizontal, showsIndicators: false) {
//                    HStack {
//                        ForEach(self.tripViewModel.destinations) { destination in
//                            Text(destination.name)
//                                .foregroundColor(.white)
//                                .padding(.normal)
//                                .background(
//                                    RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
//                                        .fill(selectedDestination == destination ? Color("PrimaryAction") : Color("DarkGray"))
//                                )
//                                .onTapGesture {
//                                    self.tripViewModel.destination = destination
//                                }
//                                .animation(.default, value: self.tripViewModel.destination)
//                        }
//                    }
//                    .padding([.leading, .trailing], .normal)
//                }
            
            Spacer()
            
            ActionButton(title: Text("Start trip")) {
                self.planTripViewModel.startTrip()
            }
        }
        .padding(.normal)
        .navigationTitle("Trip preparation")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct PlanTripScreen_Previews: PreviewProvider {
    static var previews: some View {
        PlanTripScreen()
    }
}
