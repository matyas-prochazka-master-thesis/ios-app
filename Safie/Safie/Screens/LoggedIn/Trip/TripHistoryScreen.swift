//
//  TripHistoryScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import SwiftUI

struct TripHistoryScreen: View {
    @StateObject var tripHistoryViewModel: TripHistoryViewModel = TripHistoryViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                ForEach(self.tripHistoryViewModel.trips) { trip in
                    NavigationLink(destination: TripDetailScreen(tripId: trip.id)) {
                        MyTripHistoryItem(trip: trip)
                    }
                }
            }
            .padding(.normal)
        }
        .navigationTitle("Trip history")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct TripHistoryScreen_Previews: PreviewProvider {
    static var previews: some View {
        TripHistoryScreen()
    }
}
