//
//  SelectQuickActionsScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 08.04.2022.
//

import SwiftUI

struct SelectQuickActionsScreen: View {
    @Binding var selectedQuickActions: [QuickAction]
    
    var transport1QuickActions: [QuickAction] = [
        QuickAction(id: "walking", name: "Walking", icon: "figure.walk"),
        QuickAction(id: "bus", name: "Bus", icon: "bus"),
        QuickAction(id: "tram", name: "Tram", icon: "tram"),
        QuickAction(id: "taxi", name: "Taxi", icon: "figure.wave"),
        QuickAction(id: "change", name: "Change", icon: "arrow.triangle.swap"),
        QuickAction(id: "waiting", name: "Waiting", icon: "person.badge.clock")
    ]
    
    var transport2QuickActions: [QuickAction] = [
        QuickAction(id: "train", name: "Train", icon: "train.side.front.car"),
        QuickAction(id: "bicycle", name: "Bicycle", icon: "bicycle"),
        QuickAction(id: "car", name: "Car", icon: "car"),
        QuickAction(id: "scooter", name: "Scooter", icon: "scooter"),
        QuickAction(id: "ferry", name: "Ferry", icon: "ferry"),
    ]
    
    var quickActions: [QuickAction] {
        [] + self.transport1QuickActions + transport2QuickActions
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Plan your route")
                .h1TextStyle()
                        
            Group {
                Text("**Double tap**")
                    .foregroundColor(Color("Warning")) +
                Text(" to remove action:")
            }
            .body2LabelTextStyle()
            
            ScrollView(.horizontal, showsIndicators: false) {
                ScrollViewReader { value in
                    HStack {
                        ForEach(0..<self.selectedQuickActions.count, id: \.self) { quickActionIndex in
                            let quickAction = self.selectedQuickActions[quickActionIndex]
                            
                            VStack {
                                Text("\(quickActionIndex + 1).")
                                    .body2TextStyle()
                                
                                QuickActionItem(quickAction: quickAction)
                            }
                            .onTapGesture(count: 2) {
                                let _ = self.selectedQuickActions.remove(at: quickActionIndex)
                            }
                            .id(quickAction.id)
                            
                            Spacer()
                                                            
                            Image(systemName: "arrow.right")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 20, height: 20)
                                .foregroundColor(.white)
                            
                            Spacer()
                        }
                        .onChange(of: self.selectedQuickActions) { _ in
                            withAnimation {
                                value.scrollTo("end", anchor: .center)
                            }
                        }
                        
                        VStack {
                            Text(" ")
                                .body2TextStyle()
                            
                            ZStack {
                                Circle()
                                    .fill(Color("DarkGray"))
                                                        
                                Image(systemName: "questionmark")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 20, height: 20)
                                    .foregroundColor(.white)
                            }
                            .frame(width: 60, height: 60)
                            
                            Text(" ")
                                .body2TextStyle()
                        }
                        .padding(.trailing, 100)
                        .id("end")
                    }
                    .animation(.default, value: self.selectedQuickActions)
                }
            }
            .onDrop(
                of: [.url],
                isTargeted: .constant(false)
            ) { providers in
                if let first = providers.first {
                    let _ = first.loadObject(ofClass: URL.self) { value, error in
                        guard let url = value else { return }
                        
                        let quickActions = self.quickActions.filter { $0.id == "\(url)" }
                        
                        withAnimation {
                            quickActions.forEach { self.selectedQuickActions.append($0) }
                        }
                    }
                }
                
                return false
            }
            .padding(.top, .normal)
            
            Rectangle()
                .fill(Color("DarkGray"))
                .frame(height: 2)
                .padding([.top, .bottom], .normal)
            
            TabView {
                VStack {
                    Text("Transport 1")
                    
                    Group {
                        Text("Select next action by ") +
                        Text("**tapping**")
                            .foregroundColor(Color("Warning")) +
                        Text(" on it:")
                    }
                    .body2LabelTextStyle()
                
                    LazyVGrid(
                        columns: [
                            GridItem(.flexible()),
                            GridItem(.flexible()),
                            GridItem(.flexible()),
                            GridItem(.flexible())
                        ]
                    ) {
                        ForEach(self.transport1QuickActions, id: \.self) { quickAction in
                            VStack {
                                QuickActionItem(quickAction: quickAction)
                            }
                            .padding(.bottom, .small)
                            .onDrag {
                                return .init(contentsOf: URL(string: quickAction.id))!
                            }
                            .onTapGesture {
                                self.selectedQuickActions.append(quickAction)
                            }
                        }
                    }
                    .padding(.top, .small)
                }
                .tag(0)
                
                VStack {
                    Text("Transport 2")
                    
                    Text("Select next action by clicking on it:")
                        .body2LabelTextStyle()
                
                    LazyVGrid(
                        columns: [
                            GridItem(.flexible()),
                            GridItem(.flexible()),
                            GridItem(.flexible()),
                            GridItem(.flexible())
                        ]
                    ) {
                        ForEach(self.transport2QuickActions, id: \.self) { quickAction in
                            VStack {
                                ZStack {
                                    Circle()
                                        .fill(Color("DarkGray"))
                                                        
                                    Image(systemName: quickAction.icon)
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 30, height: 30)
                                        .foregroundColor(.white)
                                }
                                .frame(width: 60, height: 60)
                                
                                Text(quickAction.name)
                                    .body2TextStyle()
                            }
                            .padding(.bottom, .small)
                            .onDrag {
                                return .init(contentsOf: URL(string: quickAction.id))!
                            }
                            .onTapGesture {
                                self.selectedQuickActions.append(quickAction)
                            }
                        }
                    }
                    .padding(.top, .small)
                }
                .tag(1)
            }
            .tabViewStyle(PageTabViewStyle())
            
            Spacer()
        }
        .padding(.normal)
        .navigationBarTitleDisplayMode(.inline)
    }
}

//struct SelectQuickActionsScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        SelectQuickActionsScreen()
//    }
//}
