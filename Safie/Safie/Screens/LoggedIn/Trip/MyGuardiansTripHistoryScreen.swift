//
//  MyGuardiansTripHistoryScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import SwiftUI

struct MyGuardiansTripHistoryScreen: View {
    @StateObject var myGuardiansTripHistoryViewModel: MyGuardiansTripHistoryViewModel = MyGuardiansTripHistoryViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                ForEach(self.myGuardiansTripHistoryViewModel.trips) { trip in
                    NavigationLink(destination: TripDetailScreen(tripId: trip.id)) {
                        TripHistoryItem(trip: trip)
                    }
                }
            }
            .padding(.normal)
        }
        .navigationTitle("Guardians Trip history")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct MyGuardiansTripHistoryScreen_Previews: PreviewProvider {
    static var previews: some View {
        MyGuardiansTripHistoryScreen()
    }
}
