//
//  ActiveTripScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import SwiftUI

struct ActiveTripScreen: View {
    @StateObject var activeTripViewModel: ActiveTripViewModel = ActiveTripViewModel()
    
    @State var showingEndTrip: Bool = false
    @State var showingPhotoPicker: Bool = false
    
    var mainQuickActions: [QuickAction] = [
        QuickAction(id: "weird_group", name: "Weird group", icon: "person.3"),
        QuickAction(id: "okay", name: "I'm okay", icon: "person.fill.checkmark"),
        QuickAction(id: "delay", name: "Delay", icon: "hourglass.badge.plus"),
        QuickAction(id: "waiting", name: "Waiting", icon: "person.badge.clock")
    ]
    
    var otherQuickActions: [QuickAction] = [
        QuickAction(id: "weird_noice", name: "Weird noice", icon: "speaker.wave.3"),
        QuickAction(id: "feeling_ill", name: "Feeling ill", icon: "heart.text.square")
    ]
    
    var body: some View {
        GeometryReader { geo in
            ZStack(alignment: .bottom) {
                VStack {
                    if let activeTrip = self.activeTripViewModel.activeTripModel.activeTrip {
                        let activities = self.activeTripViewModel.activeTripModel.activities
                        
                        HStack {
                            VStack(alignment: .leading) {
                                if let destination = activeTrip.destination {
                                    Text("Going to **\(destination.name)**")
                                        .body2TextStyle()
                                }
                                
                                if let shouldArriveAt = activeTrip.shouldArriveAt {
                                    Text("Should arrive **\(DateUtil.shared.relativeWithTime(date: shouldArriveAt)!)**")
                                        .body2TextStyle()
                                }
                            }
                            
                            Spacer()
                            
                            Text("End trip")
                                .bodyTextStyle()
                                .padding(.small)
                                .background(
                                    RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                        .fill(Color("Danger"))
                                )
                                .onTapGesture {
                                    self.showingEndTrip = true
                                }
                        }
                        
                        HStack {
                            if activities.count > 0 {
                                let latestActivity = activities[activities.count - 1]
                                
                                VStack {
                                    HStack {
                                        Text("Last action")
                                            .body2LabelTextStyle()
                                        
                                        if self.activeTripViewModel.activeTripModel.refreshingActivities {
                                            ProgressView()
                                        }
                                    }
                                    
                                    TripActivityItem(tripActivity: latestActivity)
                                }
                            } else {
                                VStack {
                                    Text("No action yet")
                                        .body2LabelTextStyle()
                                }
                            }
                            
                            Spacer()
                            
                            Image(systemName: "arrow.right")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 20, height: 20)
                                .foregroundColor(.white)
                            
                            Spacer()
                                
                            if activeTrip.currentQuickActionIndex < activeTrip.quickActions.count {
                                let quickAction = activeTrip.quickActions[activeTrip.currentQuickActionIndex]
                                
                                VStack {
                                    VStack(spacing: 0) {
                                        Text("Next planned action")
                                            .body2LabelTextStyle()
                                        
                                        Group {
                                            Text("**Double tap**")
                                                .foregroundColor(Color("Warning")) +
                                            Text(" to select:")
                                        }
                                        .body2LabelTextStyle()
                                    }
                                    
                                    QuickActionItem(quickAction: quickAction)
                                        .onTapGesture(count: 2) {
                                            self.activeTripViewModel.activeTripModel.addQuickAction(
                                                name: quickAction.name,
                                                icon: quickAction.icon,
                                                currentQuickActionIndex: activeTrip.currentQuickActionIndex + 1
                                            )
                                        }
                                }
                            } else {
                                VStack {
                                    Text("No next planned action")
                                        .body2LabelTextStyle()
                                }
                            }
                        }
                        .padding(.top, .normal)
                        
                        Spacer()
                        
                        SosButton()
                            .padding(.top, .normal)
                        
                        Spacer()
                        
                        if let photo = self.activeTripViewModel.photo {
                            GeometryReader { geo in
                                Image(uiImage: photo)
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(width: geo.size.width, height: geo.size.height)
                                    .cornerRadius(.small)
                                    .clipped()
                            }
                            .padding(.top, .normal)
                        } else {
                            TabView {
                                VStack {
                                    Text("Main actions")
                                    
                                    Group {
                                        Text("**Double tap**")
                                            .foregroundColor(Color("Warning")) +
                                        Text(" to select:")
                                    }
                                    .body2LabelTextStyle()
                                
                                    LazyVGrid(
                                        columns: [
                                            GridItem(.flexible()),
                                            GridItem(.flexible()),
                                            GridItem(.flexible()),
                                            GridItem(.flexible())
                                        ]
                                    ) {
                                        ForEach(self.mainQuickActions, id: \.self) { quickAction in
                                            VStack {
                                                QuickActionItem(quickAction: quickAction)
                                            }
                                            .padding(.bottom, .small)
                                            .onTapGesture(count: 2) {
                                                self.activeTripViewModel.activeTripModel.addQuickAction(
                                                    name: quickAction.name,
                                                    icon: quickAction.icon,
                                                    currentQuickActionIndex: nil
                                                )
                                            }
                                        }
                                    }
                                    .padding(.top, .small)
                                }
                                .tag(0)
                                
                                VStack {
                                    Text("Other actions")
                                    
                                    Group {
                                        Text("**Double tap**")
                                            .foregroundColor(Color("Warning")) +
                                        Text(" to select:")
                                    }
                                    .body2LabelTextStyle()
                                
                                    LazyVGrid(
                                        columns: [
                                            GridItem(.flexible()),
                                            GridItem(.flexible()),
                                            GridItem(.flexible()),
                                            GridItem(.flexible())
                                        ]
                                    ) {
                                        ForEach(self.otherQuickActions, id: \.self) { quickAction in
                                            VStack {
                                                QuickActionItem(quickAction: quickAction)
                                            }
                                            .padding(.bottom, .small)
                                            .onTapGesture(count: 2) {
                                                self.activeTripViewModel.activeTripModel.addQuickAction(
                                                    name: quickAction.name,
                                                    icon: quickAction.icon,
                                                    currentQuickActionIndex: nil
                                                )
                                            }
                                        }
                                    }
                                    .padding(.top, .small)
                                }
                                .tag(1)
                            }
                            .tabViewStyle(PageTabViewStyle())
                        }
                        
                        VStack {
                        }
                        .frame(height: 100)
                    }
                }
                .padding(.normal)
                                        
                HStack {
                    if let _ = self.activeTripViewModel.photo {
                        Button(action: {
                            self.activeTripViewModel.photo = nil
                        }) {
                            ZStack {
                                Circle()
                                    .fill(Color("Danger"))
                                                                
                                Image(systemName: "xmark")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 20, height: 20)
                                    .foregroundColor(.white)
                            }
                            .frame(width: 40, height: 40)
                        }
                        .haptic()
                        
                        Spacer()
                        
                        Button(action: {
                            self.activeTripViewModel.sendPhoto()
                        }) {
                            ZStack {
                                Circle()
                                    .fill(Color("PrimaryAction"))
                                
                                if !self.activeTripViewModel.sendingPhoto {
                                    Image(systemName: "arrow.up")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 15, height: 15)
                                        .foregroundColor(.white)
                                } else {
                                    ProgressView()
                                }
                            }
                            .frame(width: 40, height: 40)
                        }
                        .haptic()
                    } else {
                        Button(action: {
                            self.showingPhotoPicker = true
                        }) {
                            ZStack {
                                Circle()
                                    .fill(Color("DarkGray"))
                                
                                if !self.activeTripViewModel.sendingPhoto {
                                    Image(systemName: "photo")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 20, height: 20)
                                        .foregroundColor(.white)
                                } else {
                                    ProgressView()
                                }
                            }
                            .frame(width: 40, height: 40)
                        }
                        .haptic()
                        
                        TextField("Message", text: .init(
                            get: { [activeTripViewModel] in activeTripViewModel.message.value },
                            set: { [activeTripViewModel] in activeTripViewModel.message.value = $0 }
                        ))
                        .textFieldStyle(BasicTextFieldStyle())
                        
                        Button(action: {
                            self.activeTripViewModel.sendMessage()
                        }) {
                            ZStack {
                                Circle()
                                    .fill(self.activeTripViewModel.message.valid ? Color("PrimaryAction") : Color("DarkGray"))
                                    .animation(.default, value: self.activeTripViewModel.message.valid)
                                
                                if !self.activeTripViewModel.sendingMessage {
                                    Image(systemName: "arrow.up")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 15, height: 15)
                                        .foregroundColor(.white)
                                } else {
                                    ProgressView()
                                }
                            }
                            .frame(width: 40, height: 40)
                        }
                        .haptic()
                    }
                }
                .padding(.normal)
                .padding(.bottom, geo.safeAreaInsets.bottom)
                .background(.black)
            }
            .edgesIgnoringSafeArea(.bottom)
            .ignoresSafeArea(.keyboard)
        }
        .fullScreenCover(isPresented: self.$showingEndTrip) {
            PinEnter(
                success: {
                    self.activeTripViewModel.activeTripModel.endTrip()
                },
                afterPin: AnyView(
                    Button(action: {
                        self.showingEndTrip = false
                    }) {
                        Text("Cancel")
                            .bodyTextStyle()
                    }
                    .padding(.top, .small)
                )
            )
            .padding(.normal)
        }
        .sheet(isPresented: self.$showingPhotoPicker) {
            ImagePicker(sourceType: .photoLibrary) { image in
                self.activeTripViewModel.photo = image
                self.showingPhotoPicker = false
            }
        }
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct ActiveTripScreen_Previews: PreviewProvider {
    static var previews: some View {
        ActiveTripScreen()
    }
}
