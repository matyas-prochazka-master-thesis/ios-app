//
//  SelectQuickActionsScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 08.04.2022.
//

import SwiftUI

struct SelectQuickActions2Screen: View {
    @State var selectedQuickActions: [String?] = [nil, nil, nil, nil, nil, nil]
    @State var quickActions: [String] = ["tram", "bus", "figure.wave", "figure.walk", "train.side.front.car", "bicycle", "scooter", "car", "ferry", "person.3", "person.fill.checkmark", "swap"]
    
    var body: some View {
        VStack {
            LazyVGrid(
                columns: [
                    GridItem(.flexible()),
                    GridItem(.flexible()),
                    GridItem(.flexible())
                ]
            ) {
                ForEach(0..<6) { index in
                    VStack {
                        ZStack {
                            Circle()
                                .fill(Color("DarkGray"))
                            
                            if let selectedQuickAction = self.selectedQuickActions[index] {
                                Image(systemName: selectedQuickAction)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 30, height: 30)
                                    .foregroundColor(.white)
                            }
                        }
                    }
                    .frame(width: 60, height: 60)
                    .onDrop(
                        of: [.url],
                        isTargeted: .constant(false)
                    ) { providers in
                        if let first = providers.first {
                            let _ = first.loadObject(ofClass: URL.self) { value, error in
                                guard let url = value else { return }
                                
                                withAnimation {
                                    self.selectedQuickActions[index] = "\(url)"
                                }
                            }
                        }
                        
                        return false
                    }
                }
            }
            
            Rectangle()
                .fill(Color("DarkGray"))
                .frame(height: 2)
                .padding(.top, .normal)
            
            LazyVGrid(
                columns: [
                    GridItem(.flexible()),
                    GridItem(.flexible()),
                    GridItem(.flexible()),
                    GridItem(.flexible())
                ]
            ) {
                ForEach(self.quickActions, id: \.self) { quickAction in
                    VStack {
                        ZStack {
                            Circle()
                                .fill(Color("DarkGray"))
                                                
                            Image(systemName: quickAction)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 30, height: 30)
                                .foregroundColor(.white)
                        }
                    }
                    .frame(width: 60, height: 60)
                    .onDrag {
                        return .init(contentsOf: URL(string: quickAction))!
                    }
                }
            }
            .padding(.top, .normal)
        }
        .padding(.normal)
    }
}

//struct SelectQuickActionsScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        SelectQuickActionsScreen()
//    }
//}
