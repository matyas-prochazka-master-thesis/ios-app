//
//  SelectDestinationScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 08.04.2022.
//

import SwiftUI
import MapKit

struct SelectDestinationScreen: View {
    @StateObject var selectDestinationViewModel: SelectDestinationViewModel = SelectDestinationViewModel()
    
    @Environment(\.presentationMode) var presentationMode
    
    @Binding var selectedDestination: TripDestination?
    @Binding var selectedCreateDestination: TripDestinationCreateDto?
    
    @State var tab: Int = 1
    @State var showingNewDestinationSheet: Bool = false
    
    var body: some View {
        GeometryReader { geo in
            VStack {
                HStack(spacing: 0) {
                    VStack {
                        Text("History")
                            .h4TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.small)
                            .frame(maxWidth: .infinity)
                            .background(
                                RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                    .fill(self.tab == 0 ? Color("PrimaryAction") : Color("DarkGray"))
                            )
                    }
                    .onTapGesture {
                        withAnimation {
                            self.tab = 0
                        }
                    }
                    .padding(.small)
                    .frame(width: geo.size.width / 3)
                    
                    VStack {
                        Text("Favourites")
                            .h4TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.small)
                            .frame(maxWidth: .infinity)
                            .background(
                                RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                    .fill(self.tab == 1 ? Color("PrimaryAction") : Color("DarkGray"))
                            )
                    }
                    .onTapGesture {
                        withAnimation {
                            self.tab = 1
                        }
                    }
                    .padding(.small)
                    .frame(width: geo.size.width / 3)
                    
                    VStack {
                        Text("New")
                            .h4TextStyle()
                            .multilineTextAlignment(.center)
                            .padding(.small)
                            .frame(maxWidth: .infinity)
                            .background(
                                RoundedRectangle(cornerRadius: RadiusSize.small.rawValue)
                                    .fill(self.showingNewDestinationSheet ? Color("PrimaryAction") : Color("DarkGray"))
                            )
                    }
                    .onTapGesture {
                        withAnimation {
                            self.showingNewDestinationSheet = true
                        }
                    }
                    .padding(.small)
                    .frame(width: geo.size.width / 3)
                }
                
                TabView(selection: self.$tab) {
                    ScrollView {
                        HStack {
                            VStack(alignment: .leading) {
                                Text("History")
                                    .h2TextStyle()
                                
                                Text("Long press to add to favourites")
                                    .body2LabelTextStyle()
                            }
                            
                            Spacer()
                            
                            if self.selectDestinationViewModel.refreshingDestinations {
                                ProgressView()
                            }
                        }
                        
                        ForEach(self.selectDestinationViewModel.historyDestinations) { destination in
                            HStack {
                                Text(destination.name)
                                    .bodyTextStyle()
                                
                                Spacer()
                                
                                if self.selectedDestination == destination {
                                    Text("Selected")
                                        .body2TextStyle()
                                }
                                
                                if destination.favourite {
                                    Image(systemName: "star.fill")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 18, height: 18)
                                        .foregroundColor(Color("Warning"))
                                }
                            }
                            .foregroundColor(.white)
                            .padding(.all, .normal)
                            .background(self.selectedDestination == destination ? Color("PrimaryAction") : Color("DarkGray"))
                            .cornerRadius(.small)
                            .onTapGesture {
                                self.selectedDestination = destination
                                self.presentationMode.wrappedValue.dismiss()
                            }
                            .onLongPressGesture {
                                self.selectDestinationViewModel.favouriteDestination(destination: destination)
                                hhaptic()
                            }
                            .padding(.top, .small)
                        }
                        .padding(.top, .normal)
                    }
                    .padding(.normal)
                    .tag(0)
                    
                    ScrollView {
                        HStack {
                            VStack(alignment: .leading) {
                                Text("Favourites")
                                    .h2TextStyle()
                                
                                Text("Long press to remove from favourites")
                                    .body2LabelTextStyle()
                            }
                            
                            Spacer()
                            
                            if self.selectDestinationViewModel.refreshingDestinations {
                                ProgressView()
                            }
                        }
                        
                        ForEach(self.selectDestinationViewModel.favouriteDestinations) { destination in
                            HStack {
                                Text(destination.name)
                                    .bodyTextStyle()
                                
                                Spacer()
                                
                                if self.selectedDestination == destination {
                                    Text("Selected")
                                        .body2TextStyle()
                                }
                                
                                if destination.favourite {
                                    Image(systemName: "star.fill")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 18, height: 18)
                                        .foregroundColor(Color("Warning"))
                                }
                            }
                            .foregroundColor(.white)
                            .padding(.all, .normal)
                            .background(self.selectedDestination == destination ? Color("PrimaryAction") : Color("DarkGray"))
                            .cornerRadius(.small)
                            .onTapGesture {
                                self.selectedDestination = destination
                                self.presentationMode.wrappedValue.dismiss()
                            }
                            .onLongPressGesture {
                                self.selectDestinationViewModel.favouriteDestination(destination: destination)
                                hhaptic()
                            }
                            .padding(.top, .small)
                        }
                        .padding(.top, .normal)
                    }
                    .padding(.normal)
                    .tag(1)
                }
                .tabViewStyle(.page(indexDisplayMode: .never))
                .introspectPagedTabView { collectionView, scrollView in
                    collectionView.isScrollEnabled = false
                    scrollView.isScrollEnabled = false
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
        }
        .sheet(isPresented: self.$showingNewDestinationSheet) {
            NewDestinationScreen(onSelect: { location in
                guard let latitude = location.latitude, let longitude = location.longitude else { return }
                
                self.selectedDestination = nil
                self.selectedCreateDestination = TripDestinationCreateDto(
                    name: location.title,
                    latitude: latitude,
                    longitude: longitude
                )
                self.showingNewDestinationSheet = false
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)) {
                    self.presentationMode.wrappedValue.dismiss()
                }
            })
        }
        .navigationBarTitleDisplayMode(.inline)
    }
}

//struct SelectDestinationScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        SelectDestinationScreen()
//    }
//}
