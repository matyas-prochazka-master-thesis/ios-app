//
//  TripScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 23.03.2022.
//

import SwiftUI

struct TripScreen: View {
    @StateObject var tripViewModel: TripViewModel = TripViewModel()
    
    var body: some View {
        if self.tripViewModel.activeTripModel.checkingIfTripStarted {
            VStack {
                Text("Checking if you have already started a trip")
                    .h2TextStyle()
                
                ProgressView()
                    .padding(.top, .normal)
            }
        } else if let activeTrip = self.tripViewModel.activeTripModel.activeTrip {
            switch activeTrip.state {
            case .started:
                ActiveTripScreen()
            case .finished:
                VStack {
                    Text("Trip ended")
                        .h2TextStyle()
                    
                    ActionButton(title: Text("Okay")) {
                        self.tripViewModel.activeTripModel.clear()
                    }
                    .padding(.top, .normal)
                }
                .padding(.normal)
            default:
                EmptyView()
            }
        } else {
            if self.tripViewModel.activeTripModel.startingTrip {
                VStack {
                    Text("Starting trip")
                        .h2TextStyle()
                    
                    ProgressView()
                        .padding(.top, .normal)
                }
            } else {
                PlanTripScreen()
            }
        }
    }
}

struct TripScreen_Previews: PreviewProvider {
    static var previews: some View {
        TripScreen()
    }
}
