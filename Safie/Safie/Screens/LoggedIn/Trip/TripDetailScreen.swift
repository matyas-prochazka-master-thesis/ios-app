//
//  TripDetailScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import SwiftUI
import MapKit
import SwiftUIPager
import Kingfisher

struct TripDetailScreen: View {
    @StateObject var tripDetailViewModel: TripDetailViewModel
    
    @State var showingMap: Bool = false
    
    init(tripId: String) {
        self._tripDetailViewModel = StateObject(wrappedValue: TripDetailViewModel(tripId: tripId))
    }
    
    var body: some View {
        GeometryReader { geo in
            VStack(spacing: 0) {
                if let trip = self.tripDetailViewModel.trip {
                    HStack {
                        Group {
                            if let profilePicture = trip.account?.profilePicture {
                                KFImage(URL(string: profilePicture.absoluteUrl)!)
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                            } else {
                                Circle()
                                    .fill(Color("Label"))
                            }
                        }
                        .frame(width: 50, height: 50)
                        .cornerRadius(25)
                        .clipped()
                        
                        VStack(alignment: .leading) {
                            if let destination = trip.destination {
                                Text("Going to **\(destination.name)**")
                                    .body2TextStyle()
                            }
                            
                            if let shouldArriveAt = trip.shouldArriveAt {
                                Text("Should arrive **\(DateUtil.shared.relativeWithTime(date: shouldArriveAt)!)**")
                                    .body2TextStyle()
                            }
                        }
                    
                        Spacer()
                        
                        switch trip.state {
                        case .started:
                            Text("Started")
                                .body2TextStyle()
                                .foregroundColor(Color("Warning"))
                        case .finished:
                            Text("Finished")
                                .body2TextStyle()
                                .foregroundColor(Color("Success"))
                        default:
                            EmptyView()
                        }
                    }
                    .padding(.normal)
                    .background(Color("DarkGray"))
                }
                
                ScrollView {
                    Text("Activities update every 30 seconds")
                        .body2LabelTextStyle()
                        .padding(.top, .normal)
                    
                    VStack {
                        ForEach(self.tripDetailViewModel.activities) { activity in
                            VStack {
                                HStack {
                                    TripActivityItem(tripActivity: activity)
                                    
                                    Spacer()
                                    
                                    VStack(alignment: .trailing, spacing: 0) {
                                        Group {
                                            Text(DateUtil.shared.relativeWithTime(date: activity.createdAt)!)
                                            
                                            Text("\(activity.latitude ?? 0)")
                                            
                                            Text("\(activity.longitude ?? 0)")
                                        }
                                        .body2LabelTextStyle()
                                    }
                                }
                                .padding(.normal)
                                
                                if let attachment = activity.attachment {
                                    KFImage(URL(string: attachment.absoluteUrl)!)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(
                                            width: geo.size.width - PaddingSize.normal.rawValue * 2,
                                            height: geo.size.width - PaddingSize.normal.rawValue * 2
                                        )
                                        .clipped()
                                }
                                
                                if let payloadMessage = activity.payloadMessage {
                                    Text(payloadMessage.message)
                                        .bodyTextStyle()
                                        .alignContent(.leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading, .trailing, .bottom], .normal)
                                }
                            }
                            .background(Color("DarkGray"))
                            .cornerRadius(.small)
                            .padding([.leading, .trailing], .normal)
                            .padding(.top, .small)
                            
                            Divider()
                                .opacity(0)
                        }
                    }
                    .padding(.top, .small)
                }
                
                ActionButton(title: Text("Show on map")) {
                    self.showingMap = true
                }
                .padding(.normal)
            }
        }
        .sheet(isPresented: self.$showingMap) {
            GeometryReader { geo in
                VStack(spacing: 0) {
                    HStack {
                        Spacer()
                        
                        Button(action: {
                            self.showingMap = false
                        }) {
                            Text("Hide")
                                .bodyTextStyle()
                        }
                    }
                    .padding(.normal)
                
                    ZStack(alignment: .bottom) {
                        Map(
                            coordinateRegion: self.$tripDetailViewModel.region,
                            annotationItems: self.tripDetailViewModel.activities.filter { $0.coordinates != nil }
                        ) { place in
                            MapMarker(
                                coordinate: place.coordinates!,
                                tint: place.id == self.tripDetailViewModel.shownActivityId ? Color("PrimaryAction") : Color("Label")
                            )
                        }
                        
                        if !self.tripDetailViewModel.regionReady {
                            Color.black.opacity(0.5)
                            
                            VStack {
                                Spacer()
                                
                                ProgressView()
                                
                                Spacer()
                            }
                        }
                        
                        VStack {
                            Text("Activities update every 30 seconds")
                                .body2LabelTextStyle()

                            Pager(
                                page: self.tripDetailViewModel.page,
                                data: self.tripDetailViewModel.activities,
                                id: \.id,
                                content: { activity in
                                    HStack {
                                        TripActivityItem(tripActivity: activity)

                                        Spacer()

                                        VStack(alignment: .trailing, spacing: 0) {
                                            Group {
                                                Text(DateUtil.shared.relativeWithTime(date: activity.createdAt)!)

                                                Text("\(activity.latitude ?? 0)")

                                                Text("\(activity.longitude ?? 0)")
                                            }
                                            .body2LabelTextStyle()
                                        }
                                    }
                                    .padding(.all, .normal)
                                    .background(Color.black)
                                    .cornerRadius(.small)
                                    .padding(.all, .small)
                                    .frame(width: geo.size.width * 3/4, height: 150)

                            })
                            .onPageWillChange({ pageIndex in
                                withAnimation {
                                    self.tripDetailViewModel.viewIndex(pageIndex)
                                }
                            })
                            .sensitivity(.high)
                            .preferredItemSize(CGSize(width: geo.size.width * 3/4, height: 150))
                            .frame(width: geo.size.width, height: 150)
                            .padding(.bottom, .normal)
                        }
                    }
                }
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("Trip Detail")
    }
}

//struct TripDetailScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        TripDetailScreen()
//    }
//}
