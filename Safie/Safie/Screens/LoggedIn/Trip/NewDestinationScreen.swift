//
//  NewDestinationScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 08.04.2022.
//

import SwiftUI
import MapKit

struct NewDestinationScreen: View {
    @StateObject var newDestinationViewModel: NewDestinationViewModel
    
    @State var search: String = ""
    
    @State var showingNameAlert: Bool = false
    @State var locationTitle: String = ""
    
    init(onSelect: @escaping (_ location: SearchLocation) -> Void) {
        self._newDestinationViewModel = StateObject(wrappedValue: NewDestinationViewModel(onSelect: onSelect))
    }
    
    var body: some View {
        GeometryReader { geo in
            VStack(spacing: 0) {
                HStack(spacing: 16) {
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 15, height: 15)
                            .foregroundColor(.white)
                        
                        TextField("Start typing address", text: $search)
                            .textFieldStyle(BasicTextFieldStyle())
                            .foregroundColor(.white)
                            .onChange(of: self.search, perform: { value in
                                self.newDestinationViewModel.search(search: self.search)
                            })                            
                    }
                    .padding([.top, .bottom], .extraSmall)
                    .padding([.leading, .trailing], .small)
                    .background(Color("DarkGray"))
                    .cornerRadius(.extraSmall)
                    
                    if self.newDestinationViewModel.searching || self.newDestinationViewModel.locationToConfirm != nil {
                        Button(action: {
                            hideKeyboard()
                            
                            self.newDestinationViewModel.startSelectingFromMap()
                        }) {
                            Text("Cancel")
                                .bodyActionTextStyle()
                        }
                        .haptic()
                    }
                }
                .padding(.top, .large)
                .padding([.bottom, .leading, .trailing], .normal)
                                 
                if !self.newDestinationViewModel.searching {
                    ZStack {
                        Map(
                            coordinateRegion: self.$newDestinationViewModel.region,
                            showsUserLocation: true,
                            annotationItems: self.newDestinationViewModel.markers
                        ) { location in
                            MapMarker(coordinate: location.coordinates)
                        }
                        
                        if let _ = self.newDestinationViewModel.locationToConfirm {
                            VStack {
                                Spacer()
                                
                                ActionButton(title: Text("Confirm selected location"), action: {
                                    self.newDestinationViewModel.confirmSelectionFromMap()
                                })
                                .padding(.normal)
                            }
                            .padding(.bottom, max(geo.safeAreaInsets.bottom, PaddingSize.normal.rawValue))
                        }
                        
                        if self.newDestinationViewModel.selectingFromMap {
                            Image("mapmarker")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 60, height: 60)
                                .foregroundColor(Color("Danger"))
                                .offset(y: -30)
                            
                            VStack {
                                Spacer()
                                
                                ActionButton(title: Text("Confirm map selection"), action: {
                                    //self.locationModel.selectFromMap()
                                    //self.isPresented = false
                                    
                                    withAnimation {
                                        self.showingNameAlert = true
                                    }
                                })
                                .padding(.normal)
                            }
                            .padding(.bottom, max(geo.safeAreaInsets.bottom, PaddingSize.normal.rawValue))
                        }
                    }
                    .edgesIgnoringSafeArea(.bottom)
                    .ignoresSafeArea(.keyboard)
                }
                
                if self.newDestinationViewModel.searching {
                    VStack(spacing: 0) {
                        ForEach(self.newDestinationViewModel.searchResults) { location in
                            Button(action: {
                                self.newDestinationViewModel.selectLocationFromAutocomplete(location)
                                
                                hideKeyboard()
                            }) {
                                VStack {
                                    Text(location.title)
                                        .bodyTextStyle()
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                    
                                    if let subtitle = location.subtitle {
                                        Text(subtitle)
                                            .body2LabelTextStyle()
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                    }
                                }
                            }
                            .padding(.top, .normal)
                            
                            Divider()
                                .padding(.top, .normal)
                        }
                        
                        Spacer()
                    }
                    .padding([.top, .bottom], .small)
                    .padding([.leading, .trailing], .normal)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(Color.black)
                }
            }
        }
        .textFieldAlert(
            isShowing: $showingNameAlert,
            title: Text("Enter name of the location"),
            placeholder: "Location name",
            text: self.$locationTitle,
            onSubmit: {
                self.newDestinationViewModel.selectFromMap(self.locationTitle)
            }
        )
    }
}

//struct NewDestinationScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        NewDestinationScreen()
//    }
//}
