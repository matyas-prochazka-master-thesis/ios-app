//
//  LoggedInScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2021.
//

import SwiftUI

struct LoggedInScreen: View {
    @StateObject var loggedInViewModel: LoggedInViewModel = LoggedInViewModel()
    
    @State var showingGuardianRequestsScreen: Bool = false
    @State var showingCheckinsScreen: Bool = false
    @State var showingMyGuardiansTripHistory: Bool = false
    
    init() {
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().backgroundColor = UIColor.white
        UITabBar.appearance().clipsToBounds = true
    }
    
    var body: some View {
        Group {
            let account = self.loggedInViewModel.accountModel.account
            
            if !self.loggedInViewModel.isReady || account == nil {
                VStack {
                    ProgressView()
                    
                    Text("After long inactivity the server goes to a sleep mode. It takes around 10 seconds to wake up.")
                        .body2LabelTextStyle()
                        .padding(.top, .normal)
                }
            } else if !self.loggedInViewModel.pinModel.hasPin {
                PinSetScreen()
            } else if account?.username == nil {
                UsernameUpdateScreen()
            } else if !self.loggedInViewModel.locationModel.locationAuthorized {
                LocationAuthorizationScreen()
            } else if !self.loggedInViewModel.notificationModel.notificationAuthorized {
                NotificationAuthorizationScreen()
            } else {
                VStack {
                    Group {
                        NavigationLink(destination: GuardianRequestsScreen(), isActive: self.$showingGuardianRequestsScreen) {
                            VStack(spacing: 0) {
                            }
                            .frame(height: 0)
                        }
                        
                        NavigationLink(destination: MyGuardiansTripHistoryScreen(), isActive: self.$showingMyGuardiansTripHistory) {
                            VStack(spacing: 0) {
                            }
                            .frame(height: 0)
                        }
                    }
                    
                    if let account = self.loggedInViewModel.accountModel.account {
                        NavigationLink(destination: MyProfileScreen()) {
                            ProfileLinkRow(account: account)
                        }
                    }
                    
                    Text("Your guardians' emergencies")
                        .h3TextStyle()
                        .padding(.top, .normal)
                        .alignContent(.leading)
                    
                    Text("Updates every 20 seconds")
                        .body2LabelTextStyle()                        
                        .alignContent(.leading)
                                    
                    SosNeedHelp(soses: self.loggedInViewModel.soses)
                
                    Spacer()
                    
                    SosButton()
                    
                    Text("Press and hold 3 seconds to activate")
                        .body2LabelTextStyle()
                        .padding(.top, .small)
                
                    Spacer()
                    
                    HStack(alignment: .top) {
                        Spacer()
                        
                        NavigationLink(destination: RequestCheckinScreen()) {
                            TabMenuItem(
                                label: Text("Request\nCheckin"),
                                icon: "person.fill.questionmark"
                            )
                        }
                        
                        Spacer()
                        
                        NavigationLink(destination: TripScreen()) {
                            ZStack(alignment: .topTrailing) {
                                TabMenuItem(
                                    label: Text("Trip"),
                                    icon: "figure.walk",
                                    backgroundColor: self.loggedInViewModel.activeTripModel.activeTrip != nil ? Color("PrimaryAction") : nil
                                )
                                
//                                if self.loggedInViewModel.activeTripModel.activeTrip != nil {
//                                    ZStack {
//                                        Circle()
//                                            .fill(Color("Success"))
//
//                                        Image(systemName: "figure.walk")
//                                            .resizable()
//                                            .aspectRatio(contentMode: .fit)
//                                            .frame(width: 15, height: 15)
//                                            .foregroundColor(.white)
//                                    }
//                                    .frame(width: 20, height: 20)
//                                }
                            }
                        }
                        
                        Spacer()
                        
                        NavigationLink(destination: CheckinsScreen(), isActive: self.$showingCheckinsScreen) {
                            let count = self.loggedInViewModel.checkinModel.uncheckedCheckinsForMe.count
                            
                            TabMenuItem(
                                label: Text("Checkins"),
                                icon: "person.fill.checkmark",
                                badge: count
                            )
                        }
                        
                        Spacer()
                    }
                }
                .padding(.all, .normal)
                .onChange(of: self.loggedInViewModel.notificationOpenModel.notificationOpenKey) { notificationOpenKey in
                    // Open specific screen from notification
                    self.processNotification(notificationOpenKey: notificationOpenKey)
                }
                .onAppear {
                    self.loggedInViewModel.refresh()
                    
                    // Open specific screen from notification
                    self.processNotification(notificationOpenKey: self.loggedInViewModel.notificationOpenModel.notificationOpenKey)
                }
            }
        }
        .navigationBarHidden(true)
    }
    
    // Open specific screen from notification
    private func processNotification(notificationOpenKey: NotificationOpenKey?) -> Void {
        switch notificationOpenKey {
        case .guardianRequests:
            self.showingGuardianRequestsScreen = true
        case .checkins:
            self.showingCheckinsScreen = true
        case .myGuardiansTripHistory:
            self.showingMyGuardiansTripHistory = true
        default:
            break
        }

        self.loggedInViewModel.notificationOpenModel.notificationOpenKey = nil
    }
}

struct LoggedInScreen_Previews: PreviewProvider {
    static var previews: some View {
        LoggedInScreen()
    }
}
