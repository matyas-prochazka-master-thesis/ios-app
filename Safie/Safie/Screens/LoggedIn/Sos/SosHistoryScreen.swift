//
//  SosHistoryScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import SwiftUI

struct SosHistoryScreen: View {
    @StateObject var sosHistoryViewModel: SosHistoryViewModel = SosHistoryViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                ForEach(self.sosHistoryViewModel.soses) { sos in
                    NavigationLink(destination: SosDetailScreen(sosId: sos.id, sos: sos)) {
                        HStack {
                            Text(DateUtil.shared.dateTime(date: sos.createdAt)!)
                                .bodyLabelTextStyle()
                            
                            Spacer()
                            
                            switch sos.state {
                            case .pinEntered:
                                Text("PIN entered")
                                    .bodyTextStyle()
                                    .foregroundColor(Color("Success"))
                            case .emergency:
                                Text("EMERGENCY")
                                    .bodyTextStyle()
                                    .foregroundColor(Color("Danger"))
                            case .released:
                                Text("Released")
                                    .bodyTextStyle()
                                    .foregroundColor(Color("Warning"))
                            case .holding:
                                Text("Holding")
                                    .bodyTextStyle()
                                    .foregroundColor(Color("Warning"))
                            default:
                                Text("Unknown state")
                                    .bodyTextStyle()
                                    .foregroundColor(Color("Warning"))
                            }
                        }
                        .padding(.all, .normal)
                        .background(Color("DarkGray"))
                        .cornerRadius(.small)
                        .padding(.top, .small)
                    }
                }
            }
            .padding(.all, .normal)
        }
        .navigationTitle("SOS History")
    }
}

struct SosHistoryScreen_Previews: PreviewProvider {
    static var previews: some View {
        SosHistoryScreen()
    }
}
