//
//  SosDetailScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import SwiftUI
import MapKit
import SwiftUIPager

struct SosDetailScreen: View {
    @StateObject var sosDetailViewModel: SosDetailViewModel
    
    init(sosId: String, sos: Sos? = nil) {
        self._sosDetailViewModel = StateObject(wrappedValue: SosDetailViewModel(sosId: sosId, sos: sos))
    }
    
    var body: some View {
        GeometryReader { geo in
            ZStack(alignment: .bottom) {
                Map(
                    coordinateRegion: self.$sosDetailViewModel.region,
                    annotationItems: self.sosDetailViewModel.sosPositions
                ) { place in
                    MapMarker(
                        coordinate: place.coordinate,
                        tint: place.id == self.sosDetailViewModel.shownPositionId ? Color("PrimaryAction") : Color("Label")
                    )                                        
                }
                
                if !self.sosDetailViewModel.regionReady {
                    Color.black.opacity(0.5)
                    
                    VStack {
                        Spacer()
                        
                        ProgressView()
                        
                        Spacer()
                    }
                }
                
                VStack {
                    Text("Positions update every 30 seconds")
                        .body2LabelTextStyle()
                
                    Pager(
                        page: self.sosDetailViewModel.page,
                        data: self.sosDetailViewModel.sosPositions,
                        id: \.id,
                        content: { sosPosition in
                            HStack {
                                Text(DateUtil.shared.relativeWithTime(date: sosPosition.createdAt)!)
                                    .body2TextStyle()
                                
                                Spacer()
                                
                                VStack(alignment: .trailing, spacing: 0) {
                                    Group {
                                        Text("\(sosPosition.latitude)")
                                        
                                        Text("\(sosPosition.longitude)")
                                    }
                                    .body2LabelTextStyle()
                                }
                            }
                            .padding(.all, .normal)
                            .background(Color("DarkGray"))
                            .cornerRadius(.small)
                            .padding(.all, .small)
                            .frame(width: geo.size.width * 3/4, height: 100)
                            
                    })
                    .onPageWillChange({ pageIndex in
                        withAnimation {
                            self.sosDetailViewModel.viewIndex(pageIndex)
                        }
                    })
                    .sensitivity(.high)
                    .preferredItemSize(CGSize(width: geo.size.width * 3/4, height: 100))
                    .frame(width: geo.size.width, height: 100)
                    .padding(.bottom, .normal)
                }
            }
        }
        .edgesIgnoringSafeArea(.all)
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("SOS Detail")
    }
}

//struct SosDetailScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        SosDetailScreen()
//    }
//}
