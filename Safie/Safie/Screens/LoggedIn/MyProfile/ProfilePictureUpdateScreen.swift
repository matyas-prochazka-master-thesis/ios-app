//
//  ProfilePictureUpdateScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import SwiftUI

struct ProfilePictureUpdateScreen: View {
    @StateObject var profilePictureUpdateViewModel: ProfilePictureUpdateViewModel = ProfilePictureUpdateViewModel()
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var showImagePicker: Bool = false
    
    var body: some View {
        VStack {
            if let error = self.profilePictureUpdateViewModel.globalError {
                FormError(error: Text(error))
            }
            
            Group {
                FormLabel(label: Text("Profile Picture"))
                    .padding(.top, .normal)
                
                Group {
                    if let profilePicture = self.profilePictureUpdateViewModel.profilePicture {
                        Image(uiImage: profilePicture)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                    } else {
                        Circle()
                            .fill(.gray)
                            .onTapGesture {
                                self.showImagePicker = true
                            }
                    }
                }
                .frame(width: 100, height: 100)
                .cornerRadius(50)
                .clipped()
                .padding(.top, .normal)
            }
        }
        .sheet(isPresented: self.$showImagePicker) {
            ImagePicker(sourceType: .photoLibrary) { image in
                self.profilePictureUpdateViewModel.profilePicture = image
                self.showImagePicker = false
            }
        }
        .safeAreaInset(edge: .bottom) {
            ActionButton(title: Text("Update")) {
                self.profilePictureUpdateViewModel.updateProfilePicture()
            }
        }
        .onChange(of: self.profilePictureUpdateViewModel.updateSuccess) { _ in
            self.presentationMode.wrappedValue.dismiss()
        }
        .navigationTitle("Update profile picture")
    }
}

struct ProfilePictureUpdateScreen_Previews: PreviewProvider {
    static var previews: some View {
        ProfilePictureUpdateScreen()
    }
}
