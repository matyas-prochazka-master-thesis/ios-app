//
//  UsernameEditScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import SwiftUI

struct UsernameUpdateScreen: View {
    @StateObject var usernameUpdateViewModel: UsernameUpdateViewModel = UsernameUpdateViewModel()
    
    @Environment(\.presentationMode) var presentationMode
    
    enum Field: Hashable {
        case none
        case username
    }
    
    @FocusState var focusedField: Field?
    
    var body: some View {
        VStack {
            Spacer()
                        
            Text("Set your username")
                .h1TextStyle()
                .padding(.bottom, .large)
            
            if let error = self.usernameUpdateViewModel.globalError {
                FormError(error: Text(error))
            }
                        
            FormLabel(label: Text("Username"))
                .padding(.top, .normal)
            
            TextField("Username", text: .init(
                get: { [usernameUpdateViewModel] in usernameUpdateViewModel.username.value },
                set: { [usernameUpdateViewModel] in usernameUpdateViewModel.username.value = $0 }
            ))
            .focused($focusedField, equals: .username)
            .textFieldStyle(BasicTextFieldStyle())
            .padding(.top, .extraSmall)
            
            if self.usernameUpdateViewModel.username.touched {
                ForEach(self.usernameUpdateViewModel.username.errors, id: \.self) { error in
                    FormError(error: Text(error))
                }
            }
            
            Spacer()
            
            ActionButton(
                title: Text("Set"),
                disabled: !self.usernameUpdateViewModel.username.valid
            ) {
                self.usernameUpdateViewModel.updateUsername()
            }
            .animation(.default, value: self.usernameUpdateViewModel.username.value)
        }
        .padding(.all, .normal)
        .onChange(of: self.usernameUpdateViewModel.updateSuccess) { _ in
            self.presentationMode.wrappedValue.dismiss()
        }
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)) {
                self.focusedField = .username
            }
        }
    }
}

//struct UsernameUpdateScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        UsernameUpdateScreen()
//    }
//}
