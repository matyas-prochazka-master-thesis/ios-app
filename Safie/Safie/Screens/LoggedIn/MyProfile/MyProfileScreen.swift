//
//  ProfileScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.03.2022.
//

import SwiftUI
import Kingfisher

struct MyProfileScreen: View {
    @StateObject var myProfileViewModel: MyProfileViewModel = MyProfileViewModel()
    
    @State var showProfilePictureImagePickerSheet: Bool = false
    
    init() {
        UINavigationBar.appearance().tintColor = .white
    }
    
    var body: some View {
        GeometryReader { geo in
            ScrollView {
                self.header
                    .frame(maxWidth: .infinity)
                    .frame(height: 250 + geo.safeAreaInsets.top)
                    
                HStack {
                    Text("Your guardians")
                        .h3TextStyle()
                    
                    Spacer()
                    
                    Text("\(self.myProfileViewModel.guardianModel.guardianRequests.count) requests")
                        .body2LabelTextStyle()
                }
                .padding([.leading, .top, .trailing], .normal)
                
                NavigationLink(destination: GuardiansScreen()) {
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            ForEach(self.myProfileViewModel.guardianModel.guardians) { guardian in
                                GuardianHorizontalListItem(guardian: guardian)
                            }
                            
                            if self.myProfileViewModel.guardianModel.guardians.isEmpty {
                                GuardianAddHorizontalListItem()
                            }
                        }
                        .padding([.leading, .trailing], .normal)
                    }
                }
                        
                VStack {
                    NavigationLink(destination: SosHistoryScreen()) {
                        ChevronLink(
                            label: Text("SOS History"),
                            icon: "cross.case"
                        )
                    }
                    
                    NavigationLink(destination: TripHistoryScreen()) {
                        ChevronLink(
                            label: Text("Trip History"),
                            icon: "point.topleft.down.curvedto.point.bottomright.up"
                        )
                    }
                    
                    NavigationLink(destination: MyGuardiansTripHistoryScreen()) {
                        ChevronLink(
                            label: Text("My Guardians Trip History"),
                            icon: "point.topleft.down.curvedto.point.bottomright.up"
                        )
                    }
                    
                    NavigationLink(destination: PinChangeScreen()) {
                        ChevronLink(
                            label: Text("Change PIN"),
                            icon: "lock"
                        )
                    }
                    
                    NavigationLink(destination: UsernameUpdateScreen()) {
                        ChevronLink(
                            label: Text("Update username"),
                            icon: "pencil"
                        )
                    }
                    
                    Button(action: {
                        self.myProfileViewModel.logOut()
                    }) {
                        Text("Log out")
                            .body2DangerTextStyle()
                    }
                    .padding(.top, .large)
                }
                .padding(.all, .normal)
                .padding(.bottom, .extraLarge)                
            }
            .coordinateSpace(name: "SCROLL")
            .ignoresSafeArea(.container, edges: .vertical)
        }
        .sheet(isPresented: self.$showProfilePictureImagePickerSheet) {
            ImagePicker(sourceType: .photoLibrary) { image in
                self.myProfileViewModel.profilePicture = image
                self.myProfileViewModel.updateProfilePicture()
                self.showProfilePictureImagePickerSheet = false
            }
        }
        //.navigationBarBackButtonHidden(true)
        .navigationBarTitleDisplayMode(.inline)        
    }
    
    @ViewBuilder
    var header: some View {
        GeometryReader { geo in
            let minY = geo.frame(in: .named("SCROLL")).minY
            let size = geo.size
            let height = (size.height + minY)
            
            ZStack {
                if let account = self.myProfileViewModel.accountModel.account {
                    if let profilePicture = account.profilePicture {
                        KFImage(URL(string: profilePicture.absoluteUrl)!)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: size.width, height: height, alignment: .center)
                            .clipped()
                    } else {
                        if !self.myProfileViewModel.profilePictureUpdating {
                            Image(systemName: "plus")
                                .frame(width: 40, height: 40)
                                .foregroundColor(.white)
                        }
                    }
                    
                    LinearGradient(gradient: Gradient(colors: [.black.opacity(0.2), .black.opacity(0.8)]), startPoint: .top, endPoint: .bottom)
                    
                    if self.myProfileViewModel.profilePictureUpdating {
                        ProgressView()
                    }
                
                    VStack {
                        Spacer()
                        
                        HStack {
                            Text(account.username ?? account.email)
                                .h2TextStyle()
                            
                            Spacer()
                        }
                        .padding(.all, .normal)
                    }
                }
            }
            .cornerRadius(.normal)
            .frame(width: size.width, height: height)
            .clipped()
            .offset(y: -minY)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Change profile picture") {
                        guard !self.myProfileViewModel.profilePictureUpdating else { return }
                        
                        self.showProfilePictureImagePickerSheet = true
                    }
                    .foregroundColor(.white)
                }
            }
        }
    }
}

struct MyProfileScreen_Previews: PreviewProvider {
    static var previews: some View {
        MyProfileScreen()
    }
}
