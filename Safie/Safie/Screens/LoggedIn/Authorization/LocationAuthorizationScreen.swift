//
//  LocationAuthorizationScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import SwiftUI

struct LocationAuthorizationScreen: View {
    var body: some View {
        VStack {
            Spacer()
                        
            Text("You need to allow to track your location in settings")
                .h2TextStyle()
            
            Spacer()
            
            ActionButton(title: Text("Open settings")) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }
        }
        .padding(.all, .normal)
    }
}

struct LocationAuthorizationScreen_Previews: PreviewProvider {
    static var previews: some View {
        LocationAuthorizationScreen()
    }
}
