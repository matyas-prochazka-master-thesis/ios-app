//
//  SignContinueScreen.swift
//  Fooder
//
//  Created by Matyáš Procházka on 18.11.2021.
//

import SwiftUI
import AuthenticationServices
import FacebookLogin

struct SignContinueScreen: View {
    @StateObject var signContinueViewModel = SignContinueViewModel()    
    
    var body: some View {
        VStack(spacing: 0) {
            Spacer()
            
            Logo()
                .frame(width: 150, height: 100)
            
            Text("Keep yourself and your close ones safe.")
                .body2LabelTextStyle()                
            
            Spacer()
        
            VStack(spacing: 0) {
                if self.signContinueViewModel.signing {
                    ProgressView()
                        .padding(.bottom, .normal)
                }
                
                if let error = self.signContinueViewModel.globalError {
                    FormError(error: Text(error))
                        .padding(.bottom, .small)
                }
                                
                SignInWithAppleButton(
                    .continue,
                    onRequest: self.signContinueViewModel.signInAppleConfigure,
                    onCompletion: self.signContinueViewModel.signInAppleHandle
                )
                .frame(height: 44)
                .cornerRadius(.normal)
                .overlay(
                    RoundedRectangle(cornerRadius: RadiusSize.normal.rawValue)
                        .stroke(Color("DarkGray"), lineWidth: 2)
                )                
                //.shadow(.white, .small)
                
                /*ActionButton(
                    title: Text("Continue with Facebook"),
                    image: "facebook"
                ) {
                    self.signContinueViewModel.signInFacebook()
                }
                .padding(.top, .small)
                
                ActionButton(
                    title: Text("Continue with Google"),
                    image: "google",
                    backgroundColor: .white,
                    foregroundColor: .black,
                    shadow: .extraSmall
                ) {
                    self.signContinueViewModel.signInGoogle()
                }
                .padding(.top, .small)*/
            }
            .padding(.top, .normal)
            
            Spacer()
        }
        .padding([.top, .bottom], .small)
        .padding([.leading, .trailing], .normal)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct SignContinueScreen_Previews: PreviewProvider {
    static var previews: some View {
        SignContinueScreen()
    }
}
