//
//  RootScreen.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2021.
//

import SwiftUI

struct RootScreen: View {
    @StateObject var rootViewModel: RootViewModel = RootViewModel()

    var body: some View {
        GeometryReader { geo in
            VStack(spacing: 0) {
                VStack(spacing: 0) {
                    if !self.rootViewModel.ready {
                        LaunchScreen()
                    } else if !self.rootViewModel.appModel.onboardingShowed {
                        OnboardingScreen()
                    } else {
                        if self.rootViewModel.authModel.isUserSignedIn {
                            NavigationView {
                                LoggedInScreen()
                            }
                            .navigationViewStyle(StackNavigationViewStyle())
                        } else {
                            NavigationView {
                                SignContinueScreen()
                            }
                            .navigationViewStyle(StackNavigationViewStyle())
                        }
                    }
                }
                .padding(.top, geo.safeAreaInsets.top)
                .padding(.bottom, geo.safeAreaInsets.bottom)
            }            
            .ignoresSafeArea(.all)
        }
    }
}

struct RootScreen_Previews: PreviewProvider {
    static var previews: some View {
        RootScreen()
    }
}
