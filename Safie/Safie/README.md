#  Safie App

- Config
- Gql - GraphQL queries, mutations
- Models - MVVM Models
- ViewModels - MVVM ViewModels
- Screen - MVVM Views
    - Sign
    - LoggedIn
- Common - Common files
    - Assets - Fonts, Lottie animations
    - Components
        - UI
        - App
    - Extensions - Extensions of existing classes
    - Forms - Form validation classes
    - Structs - Entities
    - Styles - View styles
    - Utils - data formatting

## Setup

- Fill right info in Config/AppConfig
- Fill right GraphQL endpoint in Apollo CLI build phase
- Fill right info in Info.plist – for FB and Google Sign In
