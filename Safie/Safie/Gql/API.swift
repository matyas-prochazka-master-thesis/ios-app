// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public struct UsernameUpdateDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - username
  public init(username: String) {
    graphQLMap = ["username": username]
  }

  public var username: String {
    get {
      return graphQLMap["username"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "username")
    }
  }
}

public struct ProfilePictureUpdateDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - profilePictureBase64
  public init(profilePictureBase64: String) {
    graphQLMap = ["profilePictureBase64": profilePictureBase64]
  }

  public var profilePictureBase64: String {
    get {
      return graphQLMap["profilePictureBase64"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "profilePictureBase64")
    }
  }
}

public struct CheckinRequestDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - targetAccountId
  public init(targetAccountId: GraphQLID) {
    graphQLMap = ["targetAccountId": targetAccountId]
  }

  public var targetAccountId: GraphQLID {
    get {
      return graphQLMap["targetAccountId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "targetAccountId")
    }
  }
}

public struct CheckinRequestPeriodicalDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - targetAccountId
  ///   - startsAt
  ///   - endsAt
  ///   - periodInMinutes
  ///   - guardianAccountIds
  public init(targetAccountId: GraphQLID, startsAt: String, endsAt: String, periodInMinutes: Double, guardianAccountIds: [GraphQLID]) {
    graphQLMap = ["targetAccountId": targetAccountId, "startsAt": startsAt, "endsAt": endsAt, "periodInMinutes": periodInMinutes, "guardianAccountIds": guardianAccountIds]
  }

  public var targetAccountId: GraphQLID {
    get {
      return graphQLMap["targetAccountId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "targetAccountId")
    }
  }

  public var startsAt: String {
    get {
      return graphQLMap["startsAt"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "startsAt")
    }
  }

  public var endsAt: String {
    get {
      return graphQLMap["endsAt"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "endsAt")
    }
  }

  public var periodInMinutes: Double {
    get {
      return graphQLMap["periodInMinutes"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "periodInMinutes")
    }
  }

  public var guardianAccountIds: [GraphQLID] {
    get {
      return graphQLMap["guardianAccountIds"] as! [GraphQLID]
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "guardianAccountIds")
    }
  }
}

public struct CheckinDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - checkinId
  ///   - latitude
  ///   - longitude
  public init(checkinId: GraphQLID, latitude: Swift.Optional<Double?> = nil, longitude: Swift.Optional<Double?> = nil) {
    graphQLMap = ["checkinId": checkinId, "latitude": latitude, "longitude": longitude]
  }

  public var checkinId: GraphQLID {
    get {
      return graphQLMap["checkinId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "checkinId")
    }
  }

  public var latitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["latitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["longitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }
}

public struct GuardianAcceptDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - guardianId
  public init(guardianId: GraphQLID) {
    graphQLMap = ["guardianId": guardianId]
  }

  public var guardianId: GraphQLID {
    get {
      return graphQLMap["guardianId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "guardianId")
    }
  }
}

public struct GuardianAddDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - username
  public init(username: String) {
    graphQLMap = ["username": username]
  }

  public var username: String {
    get {
      return graphQLMap["username"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "username")
    }
  }
}

public struct GuardianRemoveDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - guardianId
  public init(guardianId: GraphQLID) {
    graphQLMap = ["guardianId": guardianId]
  }

  public var guardianId: GraphQLID {
    get {
      return graphQLMap["guardianId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "guardianId")
    }
  }
}

public struct SosButtonPressedDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - idFromClient
  ///   - latitude
  ///   - longitude
  public init(idFromClient: String, latitude: Swift.Optional<Double?> = nil, longitude: Swift.Optional<Double?> = nil) {
    graphQLMap = ["idFromClient": idFromClient, "latitude": latitude, "longitude": longitude]
  }

  public var idFromClient: String {
    get {
      return graphQLMap["idFromClient"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "idFromClient")
    }
  }

  public var latitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["latitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["longitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }
}

public struct SosButtonReleasedDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - idFromClient
  public init(idFromClient: String) {
    graphQLMap = ["idFromClient": idFromClient]
  }

  public var idFromClient: String {
    get {
      return graphQLMap["idFromClient"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "idFromClient")
    }
  }
}

public struct SosPinEnteredDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - idFromClient
  public init(idFromClient: String) {
    graphQLMap = ["idFromClient": idFromClient]
  }

  public var idFromClient: String {
    get {
      return graphQLMap["idFromClient"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "idFromClient")
    }
  }
}

public struct SosPositionUpdateDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - idFromClient
  ///   - latitude
  ///   - longitude
  public init(idFromClient: String, latitude: Double, longitude: Double) {
    graphQLMap = ["idFromClient": idFromClient, "latitude": latitude, "longitude": longitude]
  }

  public var idFromClient: String {
    get {
      return graphQLMap["idFromClient"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "idFromClient")
    }
  }

  public var latitude: Double {
    get {
      return graphQLMap["latitude"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Double {
    get {
      return graphQLMap["longitude"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }
}

public struct TripCreateDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - shouldArriveAt
  ///   - tripDestinationId
  ///   - tripDestinationCreateDto
  ///   - quickActions
  public init(shouldArriveAt: Swift.Optional<String?> = nil, tripDestinationId: Swift.Optional<GraphQLID?> = nil, tripDestinationCreateDto: Swift.Optional<TripDestinationCreateDto?> = nil, quickActions: [TripQuickActionDto]) {
    graphQLMap = ["shouldArriveAt": shouldArriveAt, "tripDestinationId": tripDestinationId, "tripDestinationCreateDto": tripDestinationCreateDto, "quickActions": quickActions]
  }

  public var shouldArriveAt: Swift.Optional<String?> {
    get {
      return graphQLMap["shouldArriveAt"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shouldArriveAt")
    }
  }

  public var tripDestinationId: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["tripDestinationId"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "tripDestinationId")
    }
  }

  public var tripDestinationCreateDto: Swift.Optional<TripDestinationCreateDto?> {
    get {
      return graphQLMap["tripDestinationCreateDto"] as? Swift.Optional<TripDestinationCreateDto?> ?? Swift.Optional<TripDestinationCreateDto?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "tripDestinationCreateDto")
    }
  }

  public var quickActions: [TripQuickActionDto] {
    get {
      return graphQLMap["quickActions"] as! [TripQuickActionDto]
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "quickActions")
    }
  }
}

public struct TripDestinationCreateDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - name
  ///   - latitude
  ///   - longitude
  public init(name: String, latitude: Double, longitude: Double) {
    graphQLMap = ["name": name, "latitude": latitude, "longitude": longitude]
  }

  public var name: String {
    get {
      return graphQLMap["name"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var latitude: Double {
    get {
      return graphQLMap["latitude"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Double {
    get {
      return graphQLMap["longitude"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }
}

public struct TripQuickActionDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  ///   - name
  ///   - icon
  public init(id: String, name: String, icon: String) {
    graphQLMap = ["id": id, "name": name, "icon": icon]
  }

  public var id: String {
    get {
      return graphQLMap["id"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var name: String {
    get {
      return graphQLMap["name"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var icon: String {
    get {
      return graphQLMap["icon"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "icon")
    }
  }
}

public struct ActivityCreateDto: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - latitude
  ///   - longitude
  ///   - batteryLevel
  public init(latitude: Swift.Optional<Double?> = nil, longitude: Swift.Optional<Double?> = nil, batteryLevel: Swift.Optional<Double?> = nil) {
    graphQLMap = ["latitude": latitude, "longitude": longitude, "batteryLevel": batteryLevel]
  }

  public var latitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["latitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["longitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }

  public var batteryLevel: Swift.Optional<Double?> {
    get {
      return graphQLMap["batteryLevel"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "batteryLevel")
    }
  }
}

public struct ActivityMessagePayload: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - message
  public init(message: String) {
    graphQLMap = ["message": message]
  }

  public var message: String {
    get {
      return graphQLMap["message"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "message")
    }
  }
}

public struct ActivityPhotoPayload: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - photo
  public init(photo: String) {
    graphQLMap = ["photo": photo]
  }

  public var photo: String {
    get {
      return graphQLMap["photo"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "photo")
    }
  }
}

public struct ActivityQuickActionPayload: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - name
  ///   - icon
  ///   - currentQuickActionIndex
  public init(name: String, icon: String, currentQuickActionIndex: Swift.Optional<Int?> = nil) {
    graphQLMap = ["name": name, "icon": icon, "currentQuickActionIndex": currentQuickActionIndex]
  }

  public var name: String {
    get {
      return graphQLMap["name"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var icon: String {
    get {
      return graphQLMap["icon"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "icon")
    }
  }

  public var currentQuickActionIndex: Swift.Optional<Int?> {
    get {
      return graphQLMap["currentQuickActionIndex"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "currentQuickActionIndex")
    }
  }
}

public enum CheckinRequestType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case oneTime
  case periodical
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ONE_TIME": self = .oneTime
      case "PERIODICAL": self = .periodical
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .oneTime: return "ONE_TIME"
      case .periodical: return "PERIODICAL"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: CheckinRequestType, rhs: CheckinRequestType) -> Bool {
    switch (lhs, rhs) {
      case (.oneTime, .oneTime): return true
      case (.periodical, .periodical): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [CheckinRequestType] {
    return [
      .oneTime,
      .periodical,
    ]
  }
}

public enum CheckinRequestState: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case requested
  case ended
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "REQUESTED": self = .requested
      case "ENDED": self = .ended
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .requested: return "REQUESTED"
      case .ended: return "ENDED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: CheckinRequestState, rhs: CheckinRequestState) -> Bool {
    switch (lhs, rhs) {
      case (.requested, .requested): return true
      case (.ended, .ended): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [CheckinRequestState] {
    return [
      .requested,
      .ended,
    ]
  }
}

public enum CheckinState: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case requested
  case checked
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "REQUESTED": self = .requested
      case "CHECKED": self = .checked
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .requested: return "REQUESTED"
      case .checked: return "CHECKED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: CheckinState, rhs: CheckinState) -> Bool {
    switch (lhs, rhs) {
      case (.requested, .requested): return true
      case (.checked, .checked): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [CheckinState] {
    return [
      .requested,
      .checked,
    ]
  }
}

public enum SosState: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case holding
  case released
  case pinEntered
  case emergency
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "HOLDING": self = .holding
      case "RELEASED": self = .released
      case "PIN_ENTERED": self = .pinEntered
      case "EMERGENCY": self = .emergency
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .holding: return "HOLDING"
      case .released: return "RELEASED"
      case .pinEntered: return "PIN_ENTERED"
      case .emergency: return "EMERGENCY"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: SosState, rhs: SosState) -> Bool {
    switch (lhs, rhs) {
      case (.holding, .holding): return true
      case (.released, .released): return true
      case (.pinEntered, .pinEntered): return true
      case (.emergency, .emergency): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [SosState] {
    return [
      .holding,
      .released,
      .pinEntered,
      .emergency,
    ]
  }
}

public enum TripState: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case started
  case finished
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "STARTED": self = .started
      case "FINISHED": self = .finished
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .started: return "STARTED"
      case .finished: return "FINISHED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: TripState, rhs: TripState) -> Bool {
    switch (lhs, rhs) {
      case (.started, .started): return true
      case (.finished, .finished): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [TripState] {
    return [
      .started,
      .finished,
    ]
  }
}

public enum TripActivityKey: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case message
  case photo
  case quickAction
  case position
  case check
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "MESSAGE": self = .message
      case "PHOTO": self = .photo
      case "QUICK_ACTION": self = .quickAction
      case "POSITION": self = .position
      case "CHECK": self = .check
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .message: return "MESSAGE"
      case .photo: return "PHOTO"
      case .quickAction: return "QUICK_ACTION"
      case .position: return "POSITION"
      case .check: return "CHECK"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: TripActivityKey, rhs: TripActivityKey) -> Bool {
    switch (lhs, rhs) {
      case (.message, .message): return true
      case (.photo, .photo): return true
      case (.quickAction, .quickAction): return true
      case (.position, .position): return true
      case (.check, .check): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [TripActivityKey] {
    return [
      .message,
      .photo,
      .quickAction,
      .position,
      .check,
    ]
  }
}

public final class EditPasswordMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation EditPassword($oldPassword: String!, $password: String!) {
      editPassword(password: {oldPassword: $oldPassword, password: $password})
    }
    """

  public let operationName: String = "EditPassword"

  public var oldPassword: String
  public var password: String

  public init(oldPassword: String, password: String) {
    self.oldPassword = oldPassword
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["oldPassword": oldPassword, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("editPassword", arguments: ["password": ["oldPassword": GraphQLVariable("oldPassword"), "password": GraphQLVariable("password")]], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(editPassword: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "editPassword": editPassword])
    }

    public var editPassword: Bool {
      get {
        return resultMap["editPassword"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "editPassword")
      }
    }
  }
}

public final class UpdateUsernameMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation UpdateUsername($usernameUpdate: UsernameUpdateDto!) {
      updateUsername(usernameUpdate: $usernameUpdate) {
        __typename
        ...accountBasicInfo
      }
    }
    """

  public let operationName: String = "UpdateUsername"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    return document
  }

  public var usernameUpdate: UsernameUpdateDto

  public init(usernameUpdate: UsernameUpdateDto) {
    self.usernameUpdate = usernameUpdate
  }

  public var variables: GraphQLMap? {
    return ["usernameUpdate": usernameUpdate]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("updateUsername", arguments: ["usernameUpdate": GraphQLVariable("usernameUpdate")], type: .nonNull(.object(UpdateUsername.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateUsername: UpdateUsername) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateUsername": updateUsername.resultMap])
    }

    public var updateUsername: UpdateUsername {
      get {
        return UpdateUsername(unsafeResultMap: resultMap["updateUsername"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "updateUsername")
      }
    }

    public struct UpdateUsername: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Account"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(AccountBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, username: String? = nil, email: String) {
        self.init(unsafeResultMap: ["__typename": "Account", "id": id, "username": username, "email": email])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var accountBasicInfo: AccountBasicInfo {
          get {
            return AccountBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class UpdateProfilePictureMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation UpdateProfilePicture($profilePictureUpdate: ProfilePictureUpdateDto!) {
      updateProfilePicture(profilePictureUpdate: $profilePictureUpdate) {
        __typename
        ...accountBasicInfo
      }
    }
    """

  public let operationName: String = "UpdateProfilePicture"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    return document
  }

  public var profilePictureUpdate: ProfilePictureUpdateDto

  public init(profilePictureUpdate: ProfilePictureUpdateDto) {
    self.profilePictureUpdate = profilePictureUpdate
  }

  public var variables: GraphQLMap? {
    return ["profilePictureUpdate": profilePictureUpdate]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("updateProfilePicture", arguments: ["profilePictureUpdate": GraphQLVariable("profilePictureUpdate")], type: .nonNull(.object(UpdateProfilePicture.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateProfilePicture: UpdateProfilePicture) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateProfilePicture": updateProfilePicture.resultMap])
    }

    public var updateProfilePicture: UpdateProfilePicture {
      get {
        return UpdateProfilePicture(unsafeResultMap: resultMap["updateProfilePicture"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "updateProfilePicture")
      }
    }

    public struct UpdateProfilePicture: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Account"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(AccountBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, username: String? = nil, email: String) {
        self.init(unsafeResultMap: ["__typename": "Account", "id": id, "username": username, "email": email])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var accountBasicInfo: AccountBasicInfo {
          get {
            return AccountBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class MyAccountQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query MyAccount {
      me {
        __typename
        ...accountBasicInfo
        ...accountProfilePicture
      }
    }
    """

  public let operationName: String = "MyAccount"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("me", type: .nonNull(.object(Me.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(me: Me) {
      self.init(unsafeResultMap: ["__typename": "Query", "me": me.resultMap])
    }

    public var me: Me {
      get {
        return Me(unsafeResultMap: resultMap["me"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "me")
      }
    }

    public struct Me: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Account"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(AccountBasicInfo.self),
          GraphQLFragmentSpread(AccountProfilePicture.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var accountBasicInfo: AccountBasicInfo {
          get {
            return AccountBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var accountProfilePicture: AccountProfilePicture {
          get {
            return AccountProfilePicture(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class SignInMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SignIn($email: String!, $password: String!) {
      login(login: {email: $email, password: $password}) {
        __typename
        account {
          __typename
          ...accountBasicInfo
        }
        accessToken
        refreshToken
      }
    }
    """

  public let operationName: String = "SignIn"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    return document
  }

  public var email: String
  public var password: String

  public init(email: String, password: String) {
    self.email = email
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["email": email, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("login", arguments: ["login": ["email": GraphQLVariable("email"), "password": GraphQLVariable("password")]], type: .nonNull(.object(Login.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(login: Login) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "login": login.resultMap])
    }

    public var login: Login {
      get {
        return Login(unsafeResultMap: resultMap["login"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "login")
      }
    }

    public struct Login: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AuthReturnDto"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("account", type: .nonNull(.object(Account.selections))),
          GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
          GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(account: Account, accessToken: String, refreshToken: String) {
        self.init(unsafeResultMap: ["__typename": "AuthReturnDto", "account": account.resultMap, "accessToken": accessToken, "refreshToken": refreshToken])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var account: Account {
        get {
          return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "account")
        }
      }

      public var accessToken: String {
        get {
          return resultMap["accessToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var refreshToken: String {
        get {
          return resultMap["refreshToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "refreshToken")
        }
      }

      public struct Account: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Account"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLFragmentSpread(AccountBasicInfo.self),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, username: String? = nil, email: String) {
          self.init(unsafeResultMap: ["__typename": "Account", "id": id, "username": username, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var fragments: Fragments {
          get {
            return Fragments(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public struct Fragments {
          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public var accountBasicInfo: AccountBasicInfo {
            get {
              return AccountBasicInfo(unsafeResultMap: resultMap)
            }
            set {
              resultMap += newValue.resultMap
            }
          }
        }
      }
    }
  }
}

public final class SignUpMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SignUp($email: String!, $password: String!) {
      register(register: {email: $email, password: $password, source: IOS}) {
        __typename
        account {
          __typename
          ...accountBasicInfo
        }
        accessToken
        refreshToken
      }
    }
    """

  public let operationName: String = "SignUp"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    return document
  }

  public var email: String
  public var password: String

  public init(email: String, password: String) {
    self.email = email
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["email": email, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("register", arguments: ["register": ["email": GraphQLVariable("email"), "password": GraphQLVariable("password"), "source": "IOS"]], type: .nonNull(.object(Register.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(register: Register) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "register": register.resultMap])
    }

    public var register: Register {
      get {
        return Register(unsafeResultMap: resultMap["register"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "register")
      }
    }

    public struct Register: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AuthReturnDto"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("account", type: .nonNull(.object(Account.selections))),
          GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
          GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(account: Account, accessToken: String, refreshToken: String) {
        self.init(unsafeResultMap: ["__typename": "AuthReturnDto", "account": account.resultMap, "accessToken": accessToken, "refreshToken": refreshToken])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var account: Account {
        get {
          return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "account")
        }
      }

      public var accessToken: String {
        get {
          return resultMap["accessToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var refreshToken: String {
        get {
          return resultMap["refreshToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "refreshToken")
        }
      }

      public struct Account: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Account"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLFragmentSpread(AccountBasicInfo.self),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, username: String? = nil, email: String) {
          self.init(unsafeResultMap: ["__typename": "Account", "id": id, "username": username, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var fragments: Fragments {
          get {
            return Fragments(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public struct Fragments {
          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public var accountBasicInfo: AccountBasicInfo {
            get {
              return AccountBasicInfo(unsafeResultMap: resultMap)
            }
            set {
              resultMap += newValue.resultMap
            }
          }
        }
      }
    }
  }
}

public final class RefreshTokensMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation RefreshTokens {
      refresh {
        __typename
        accessToken
        refreshToken
      }
    }
    """

  public let operationName: String = "RefreshTokens"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("refresh", type: .nonNull(.object(Refresh.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(refresh: Refresh) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "refresh": refresh.resultMap])
    }

    public var refresh: Refresh {
      get {
        return Refresh(unsafeResultMap: resultMap["refresh"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "refresh")
      }
    }

    public struct Refresh: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AuthReturnDto"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
          GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(accessToken: String, refreshToken: String) {
        self.init(unsafeResultMap: ["__typename": "AuthReturnDto", "accessToken": accessToken, "refreshToken": refreshToken])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var accessToken: String {
        get {
          return resultMap["accessToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var refreshToken: String {
        get {
          return resultMap["refreshToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "refreshToken")
        }
      }
    }
  }
}

public final class ForgottenPasswordMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ForgottenPassword($email: String!) {
      forgottenPassword(passwordForget: {email: $email, source: IOS_APP})
    }
    """

  public let operationName: String = "ForgottenPassword"

  public var email: String

  public init(email: String) {
    self.email = email
  }

  public var variables: GraphQLMap? {
    return ["email": email]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("forgottenPassword", arguments: ["passwordForget": ["email": GraphQLVariable("email"), "source": "IOS_APP"]], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(forgottenPassword: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "forgottenPassword": forgottenPassword])
    }

    public var forgottenPassword: Bool {
      get {
        return resultMap["forgottenPassword"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "forgottenPassword")
      }
    }
  }
}

public final class ResetPasswordMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ResetPassword($email: String!, $code: String!, $password: String!) {
      resetPassword(passwordReset: {email: $email, code: $code, password: $password})
    }
    """

  public let operationName: String = "ResetPassword"

  public var email: String
  public var code: String
  public var password: String

  public init(email: String, code: String, password: String) {
    self.email = email
    self.code = code
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["email": email, "code": code, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("resetPassword", arguments: ["passwordReset": ["email": GraphQLVariable("email"), "code": GraphQLVariable("code"), "password": GraphQLVariable("password")]], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(resetPassword: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "resetPassword": resetPassword])
    }

    public var resetPassword: Bool {
      get {
        return resultMap["resetPassword"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "resetPassword")
      }
    }
  }
}

public final class SignInAppleMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SignInApple($code: String!) {
      appleAuth(appleAuth: {code: $code, source: IOS}) {
        __typename
        account {
          __typename
          ...accountBasicInfo
        }
        accessToken
        refreshToken
      }
    }
    """

  public let operationName: String = "SignInApple"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    return document
  }

  public var code: String

  public init(code: String) {
    self.code = code
  }

  public var variables: GraphQLMap? {
    return ["code": code]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("appleAuth", arguments: ["appleAuth": ["code": GraphQLVariable("code"), "source": "IOS"]], type: .nonNull(.object(AppleAuth.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(appleAuth: AppleAuth) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "appleAuth": appleAuth.resultMap])
    }

    public var appleAuth: AppleAuth {
      get {
        return AppleAuth(unsafeResultMap: resultMap["appleAuth"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "appleAuth")
      }
    }

    public struct AppleAuth: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AuthReturnDto"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("account", type: .nonNull(.object(Account.selections))),
          GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
          GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(account: Account, accessToken: String, refreshToken: String) {
        self.init(unsafeResultMap: ["__typename": "AuthReturnDto", "account": account.resultMap, "accessToken": accessToken, "refreshToken": refreshToken])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var account: Account {
        get {
          return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "account")
        }
      }

      public var accessToken: String {
        get {
          return resultMap["accessToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var refreshToken: String {
        get {
          return resultMap["refreshToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "refreshToken")
        }
      }

      public struct Account: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Account"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLFragmentSpread(AccountBasicInfo.self),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, username: String? = nil, email: String) {
          self.init(unsafeResultMap: ["__typename": "Account", "id": id, "username": username, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var fragments: Fragments {
          get {
            return Fragments(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public struct Fragments {
          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public var accountBasicInfo: AccountBasicInfo {
            get {
              return AccountBasicInfo(unsafeResultMap: resultMap)
            }
            set {
              resultMap += newValue.resultMap
            }
          }
        }
      }
    }
  }
}

public final class SignInFacebookMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SignInFacebook($accessToken: String!) {
      facebookAuthV2(facebookAuth: {accessToken: $accessToken, source: IOS}) {
        __typename
        account {
          __typename
          ...accountBasicInfo
        }
        accessToken
        refreshToken
      }
    }
    """

  public let operationName: String = "SignInFacebook"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    return document
  }

  public var accessToken: String

  public init(accessToken: String) {
    self.accessToken = accessToken
  }

  public var variables: GraphQLMap? {
    return ["accessToken": accessToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("facebookAuthV2", arguments: ["facebookAuth": ["accessToken": GraphQLVariable("accessToken"), "source": "IOS"]], type: .nonNull(.object(FacebookAuthV2.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(facebookAuthV2: FacebookAuthV2) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "facebookAuthV2": facebookAuthV2.resultMap])
    }

    public var facebookAuthV2: FacebookAuthV2 {
      get {
        return FacebookAuthV2(unsafeResultMap: resultMap["facebookAuthV2"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "facebookAuthV2")
      }
    }

    public struct FacebookAuthV2: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AuthReturnDto"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("account", type: .nonNull(.object(Account.selections))),
          GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
          GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(account: Account, accessToken: String, refreshToken: String) {
        self.init(unsafeResultMap: ["__typename": "AuthReturnDto", "account": account.resultMap, "accessToken": accessToken, "refreshToken": refreshToken])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var account: Account {
        get {
          return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "account")
        }
      }

      public var accessToken: String {
        get {
          return resultMap["accessToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var refreshToken: String {
        get {
          return resultMap["refreshToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "refreshToken")
        }
      }

      public struct Account: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Account"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLFragmentSpread(AccountBasicInfo.self),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, username: String? = nil, email: String) {
          self.init(unsafeResultMap: ["__typename": "Account", "id": id, "username": username, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var fragments: Fragments {
          get {
            return Fragments(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public struct Fragments {
          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public var accountBasicInfo: AccountBasicInfo {
            get {
              return AccountBasicInfo(unsafeResultMap: resultMap)
            }
            set {
              resultMap += newValue.resultMap
            }
          }
        }
      }
    }
  }
}

public final class SignInGoogleMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SignInGoogle($idToken: String!) {
      googleAuthV2(googleAuth: {idToken: $idToken, source: IOS}) {
        __typename
        account {
          __typename
          ...accountBasicInfo
        }
        accessToken
        refreshToken
      }
    }
    """

  public let operationName: String = "SignInGoogle"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    return document
  }

  public var idToken: String

  public init(idToken: String) {
    self.idToken = idToken
  }

  public var variables: GraphQLMap? {
    return ["idToken": idToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("googleAuthV2", arguments: ["googleAuth": ["idToken": GraphQLVariable("idToken"), "source": "IOS"]], type: .nonNull(.object(GoogleAuthV2.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(googleAuthV2: GoogleAuthV2) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "googleAuthV2": googleAuthV2.resultMap])
    }

    public var googleAuthV2: GoogleAuthV2 {
      get {
        return GoogleAuthV2(unsafeResultMap: resultMap["googleAuthV2"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "googleAuthV2")
      }
    }

    public struct GoogleAuthV2: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AuthReturnDto"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("account", type: .nonNull(.object(Account.selections))),
          GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
          GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(account: Account, accessToken: String, refreshToken: String) {
        self.init(unsafeResultMap: ["__typename": "AuthReturnDto", "account": account.resultMap, "accessToken": accessToken, "refreshToken": refreshToken])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var account: Account {
        get {
          return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "account")
        }
      }

      public var accessToken: String {
        get {
          return resultMap["accessToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var refreshToken: String {
        get {
          return resultMap["refreshToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "refreshToken")
        }
      }

      public struct Account: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Account"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLFragmentSpread(AccountBasicInfo.self),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, username: String? = nil, email: String) {
          self.init(unsafeResultMap: ["__typename": "Account", "id": id, "username": username, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var fragments: Fragments {
          get {
            return Fragments(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public struct Fragments {
          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public var accountBasicInfo: AccountBasicInfo {
            get {
              return AccountBasicInfo(unsafeResultMap: resultMap)
            }
            set {
              resultMap += newValue.resultMap
            }
          }
        }
      }
    }
  }
}

public final class RequestCheckinMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation RequestCheckin($checkinRequest: CheckinRequestDto!) {
      requestCheckin(checkinRequest: $checkinRequest)
    }
    """

  public let operationName: String = "RequestCheckin"

  public var checkinRequest: CheckinRequestDto

  public init(checkinRequest: CheckinRequestDto) {
    self.checkinRequest = checkinRequest
  }

  public var variables: GraphQLMap? {
    return ["checkinRequest": checkinRequest]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("requestCheckin", arguments: ["checkinRequest": GraphQLVariable("checkinRequest")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(requestCheckin: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "requestCheckin": requestCheckin])
    }

    public var requestCheckin: Bool {
      get {
        return resultMap["requestCheckin"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "requestCheckin")
      }
    }
  }
}

public final class RequestPeriodicalCheckinMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation RequestPeriodicalCheckin($periodicalCheckinRequest: CheckinRequestPeriodicalDto!) {
      requestPeriodicalCheckin(periodicalCheckinRequest: $periodicalCheckinRequest)
    }
    """

  public let operationName: String = "RequestPeriodicalCheckin"

  public var periodicalCheckinRequest: CheckinRequestPeriodicalDto

  public init(periodicalCheckinRequest: CheckinRequestPeriodicalDto) {
    self.periodicalCheckinRequest = periodicalCheckinRequest
  }

  public var variables: GraphQLMap? {
    return ["periodicalCheckinRequest": periodicalCheckinRequest]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("requestPeriodicalCheckin", arguments: ["periodicalCheckinRequest": GraphQLVariable("periodicalCheckinRequest")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(requestPeriodicalCheckin: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "requestPeriodicalCheckin": requestPeriodicalCheckin])
    }

    public var requestPeriodicalCheckin: Bool {
      get {
        return resultMap["requestPeriodicalCheckin"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "requestPeriodicalCheckin")
      }
    }
  }
}

public final class CheckinMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation Checkin($checkin: CheckinDto!) {
      checkin(checkin: $checkin)
    }
    """

  public let operationName: String = "Checkin"

  public var checkin: CheckinDto

  public init(checkin: CheckinDto) {
    self.checkin = checkin
  }

  public var variables: GraphQLMap? {
    return ["checkin": checkin]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("checkin", arguments: ["checkin": GraphQLVariable("checkin")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(checkin: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "checkin": checkin])
    }

    public var checkin: Bool {
      get {
        return resultMap["checkin"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "checkin")
      }
    }
  }
}

public final class CheckinRequestsForMeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query CheckinRequestsForMe {
      getRequestsForMe {
        __typename
        ...checkinRequestBasicInfo
        ...checkinRequestRequesterInfo
        ...checkinRequestGuardiansInfo
      }
    }
    """

  public let operationName: String = "CheckinRequestsForMe"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + CheckinRequestBasicInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestRequesterInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestGuardiansInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getRequestsForMe", type: .nonNull(.list(.nonNull(.object(GetRequestsForMe.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getRequestsForMe: [GetRequestsForMe]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getRequestsForMe": getRequestsForMe.map { (value: GetRequestsForMe) -> ResultMap in value.resultMap }])
    }

    public var getRequestsForMe: [GetRequestsForMe] {
      get {
        return (resultMap["getRequestsForMe"] as! [ResultMap]).map { (value: ResultMap) -> GetRequestsForMe in GetRequestsForMe(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetRequestsForMe) -> ResultMap in value.resultMap }, forKey: "getRequestsForMe")
      }
    }

    public struct GetRequestsForMe: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CheckinRequest"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(CheckinRequestBasicInfo.self),
          GraphQLFragmentSpread(CheckinRequestRequesterInfo.self),
          GraphQLFragmentSpread(CheckinRequestGuardiansInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var checkinRequestBasicInfo: CheckinRequestBasicInfo {
          get {
            return CheckinRequestBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var checkinRequestRequesterInfo: CheckinRequestRequesterInfo {
          get {
            return CheckinRequestRequesterInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var checkinRequestGuardiansInfo: CheckinRequestGuardiansInfo {
          get {
            return CheckinRequestGuardiansInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class CheckinRequestsIFollowQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query CheckinRequestsIFollow {
      getRequestsIFollow {
        __typename
        ...checkinRequestBasicInfo
        ...checkinRequestTargetInfo
      }
    }
    """

  public let operationName: String = "CheckinRequestsIFollow"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + CheckinRequestBasicInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestTargetInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getRequestsIFollow", type: .nonNull(.list(.nonNull(.object(GetRequestsIFollow.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getRequestsIFollow: [GetRequestsIFollow]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getRequestsIFollow": getRequestsIFollow.map { (value: GetRequestsIFollow) -> ResultMap in value.resultMap }])
    }

    public var getRequestsIFollow: [GetRequestsIFollow] {
      get {
        return (resultMap["getRequestsIFollow"] as! [ResultMap]).map { (value: ResultMap) -> GetRequestsIFollow in GetRequestsIFollow(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetRequestsIFollow) -> ResultMap in value.resultMap }, forKey: "getRequestsIFollow")
      }
    }

    public struct GetRequestsIFollow: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CheckinRequest"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(CheckinRequestBasicInfo.self),
          GraphQLFragmentSpread(CheckinRequestTargetInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var checkinRequestBasicInfo: CheckinRequestBasicInfo {
          get {
            return CheckinRequestBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var checkinRequestTargetInfo: CheckinRequestTargetInfo {
          get {
            return CheckinRequestTargetInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class CheckinRequestQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query CheckinRequest($checkinRequestId: ID!) {
      getRequest(checkinRequestId: $checkinRequestId) {
        __typename
        ...checkinRequestBasicInfo
        ...checkinRequestRequesterInfo
        ...checkinRequestTargetInfo
        ...checkinRequestGuardiansInfo
      }
    }
    """

  public let operationName: String = "CheckinRequest"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + CheckinRequestBasicInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestRequesterInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestTargetInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestGuardiansInfo.fragmentDefinition)
    return document
  }

  public var checkinRequestId: GraphQLID

  public init(checkinRequestId: GraphQLID) {
    self.checkinRequestId = checkinRequestId
  }

  public var variables: GraphQLMap? {
    return ["checkinRequestId": checkinRequestId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getRequest", arguments: ["checkinRequestId": GraphQLVariable("checkinRequestId")], type: .nonNull(.object(GetRequest.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getRequest: GetRequest) {
      self.init(unsafeResultMap: ["__typename": "Query", "getRequest": getRequest.resultMap])
    }

    public var getRequest: GetRequest {
      get {
        return GetRequest(unsafeResultMap: resultMap["getRequest"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "getRequest")
      }
    }

    public struct GetRequest: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CheckinRequest"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(CheckinRequestBasicInfo.self),
          GraphQLFragmentSpread(CheckinRequestRequesterInfo.self),
          GraphQLFragmentSpread(CheckinRequestTargetInfo.self),
          GraphQLFragmentSpread(CheckinRequestGuardiansInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var checkinRequestBasicInfo: CheckinRequestBasicInfo {
          get {
            return CheckinRequestBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var checkinRequestRequesterInfo: CheckinRequestRequesterInfo {
          get {
            return CheckinRequestRequesterInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var checkinRequestTargetInfo: CheckinRequestTargetInfo {
          get {
            return CheckinRequestTargetInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var checkinRequestGuardiansInfo: CheckinRequestGuardiansInfo {
          get {
            return CheckinRequestGuardiansInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class CheckinsByRequestQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query CheckinsByRequest($checkinRequestId: ID!) {
      getCheckinsByRequest(checkinRequestId: $checkinRequestId) {
        __typename
        ...checkinBasicInfo
      }
    }
    """

  public let operationName: String = "CheckinsByRequest"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + CheckinBasicInfo.fragmentDefinition)
    return document
  }

  public var checkinRequestId: GraphQLID

  public init(checkinRequestId: GraphQLID) {
    self.checkinRequestId = checkinRequestId
  }

  public var variables: GraphQLMap? {
    return ["checkinRequestId": checkinRequestId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getCheckinsByRequest", arguments: ["checkinRequestId": GraphQLVariable("checkinRequestId")], type: .nonNull(.list(.nonNull(.object(GetCheckinsByRequest.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getCheckinsByRequest: [GetCheckinsByRequest]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getCheckinsByRequest": getCheckinsByRequest.map { (value: GetCheckinsByRequest) -> ResultMap in value.resultMap }])
    }

    public var getCheckinsByRequest: [GetCheckinsByRequest] {
      get {
        return (resultMap["getCheckinsByRequest"] as! [ResultMap]).map { (value: ResultMap) -> GetCheckinsByRequest in GetCheckinsByRequest(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetCheckinsByRequest) -> ResultMap in value.resultMap }, forKey: "getCheckinsByRequest")
      }
    }

    public struct GetCheckinsByRequest: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Checkin"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(CheckinBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, latitude: Double? = nil, longitude: Double? = nil, state: CheckinState, checkedAt: String? = nil, createdAt: String) {
        self.init(unsafeResultMap: ["__typename": "Checkin", "id": id, "latitude": latitude, "longitude": longitude, "state": state, "checkedAt": checkedAt, "createdAt": createdAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var checkinBasicInfo: CheckinBasicInfo {
          get {
            return CheckinBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class UncheckedCheckinsForMeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query UncheckedCheckinsForMe {
      getUncheckedCheckinsForMe {
        __typename
        ...checkinBasicInfo
        ...checkinRequestInfo
      }
    }
    """

  public let operationName: String = "UncheckedCheckinsForMe"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + CheckinBasicInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestBasicInfo.fragmentDefinition)
    document.append("\n" + CheckinRequestRequesterInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getUncheckedCheckinsForMe", type: .nonNull(.list(.nonNull(.object(GetUncheckedCheckinsForMe.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getUncheckedCheckinsForMe: [GetUncheckedCheckinsForMe]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getUncheckedCheckinsForMe": getUncheckedCheckinsForMe.map { (value: GetUncheckedCheckinsForMe) -> ResultMap in value.resultMap }])
    }

    public var getUncheckedCheckinsForMe: [GetUncheckedCheckinsForMe] {
      get {
        return (resultMap["getUncheckedCheckinsForMe"] as! [ResultMap]).map { (value: ResultMap) -> GetUncheckedCheckinsForMe in GetUncheckedCheckinsForMe(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetUncheckedCheckinsForMe) -> ResultMap in value.resultMap }, forKey: "getUncheckedCheckinsForMe")
      }
    }

    public struct GetUncheckedCheckinsForMe: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Checkin"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(CheckinBasicInfo.self),
          GraphQLFragmentSpread(CheckinRequestInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var checkinBasicInfo: CheckinBasicInfo {
          get {
            return CheckinBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var checkinRequestInfo: CheckinRequestInfo {
          get {
            return CheckinRequestInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class AcceptGuardianMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation AcceptGuardian($guardianAccept: GuardianAcceptDto!) {
      acceptGuardian(guardianAccept: $guardianAccept)
    }
    """

  public let operationName: String = "AcceptGuardian"

  public var guardianAccept: GuardianAcceptDto

  public init(guardianAccept: GuardianAcceptDto) {
    self.guardianAccept = guardianAccept
  }

  public var variables: GraphQLMap? {
    return ["guardianAccept": guardianAccept]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("acceptGuardian", arguments: ["guardianAccept": GraphQLVariable("guardianAccept")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(acceptGuardian: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "acceptGuardian": acceptGuardian])
    }

    public var acceptGuardian: Bool {
      get {
        return resultMap["acceptGuardian"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "acceptGuardian")
      }
    }
  }
}

public final class AddGuardianMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation AddGuardian($guardianAdd: GuardianAddDto!) {
      addGuardian(guardianAdd: $guardianAdd)
    }
    """

  public let operationName: String = "AddGuardian"

  public var guardianAdd: GuardianAddDto

  public init(guardianAdd: GuardianAddDto) {
    self.guardianAdd = guardianAdd
  }

  public var variables: GraphQLMap? {
    return ["guardianAdd": guardianAdd]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("addGuardian", arguments: ["guardianAdd": GraphQLVariable("guardianAdd")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(addGuardian: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "addGuardian": addGuardian])
    }

    public var addGuardian: Bool {
      get {
        return resultMap["addGuardian"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "addGuardian")
      }
    }
  }
}

public final class RemoveGuardianMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation RemoveGuardian($guardianRemove: GuardianRemoveDto!) {
      removeGuardian(guardianRemove: $guardianRemove)
    }
    """

  public let operationName: String = "RemoveGuardian"

  public var guardianRemove: GuardianRemoveDto

  public init(guardianRemove: GuardianRemoveDto) {
    self.guardianRemove = guardianRemove
  }

  public var variables: GraphQLMap? {
    return ["guardianRemove": guardianRemove]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("removeGuardian", arguments: ["guardianRemove": GraphQLVariable("guardianRemove")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(removeGuardian: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "removeGuardian": removeGuardian])
    }

    public var removeGuardian: Bool {
      get {
        return resultMap["removeGuardian"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "removeGuardian")
      }
    }
  }
}

public final class MyGuardiansQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query MyGuardians {
      getMyGuardians {
        __typename
        ...guardianBasicInfo
      }
    }
    """

  public let operationName: String = "MyGuardians"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + GuardianBasicInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getMyGuardians", type: .nonNull(.list(.nonNull(.object(GetMyGuardian.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getMyGuardians: [GetMyGuardian]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getMyGuardians": getMyGuardians.map { (value: GetMyGuardian) -> ResultMap in value.resultMap }])
    }

    public var getMyGuardians: [GetMyGuardian] {
      get {
        return (resultMap["getMyGuardians"] as! [ResultMap]).map { (value: ResultMap) -> GetMyGuardian in GetMyGuardian(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetMyGuardian) -> ResultMap in value.resultMap }, forKey: "getMyGuardians")
      }
    }

    public struct GetMyGuardian: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Guardian"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(GuardianBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var guardianBasicInfo: GuardianBasicInfo {
          get {
            return GuardianBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class GuardianRequestsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query GuardianRequests {
      getGuardiansRequests {
        __typename
        ...guardianAccountInfo
      }
    }
    """

  public let operationName: String = "GuardianRequests"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + GuardianAccountInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getGuardiansRequests", type: .nonNull(.list(.nonNull(.object(GetGuardiansRequest.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getGuardiansRequests: [GetGuardiansRequest]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getGuardiansRequests": getGuardiansRequests.map { (value: GetGuardiansRequest) -> ResultMap in value.resultMap }])
    }

    public var getGuardiansRequests: [GetGuardiansRequest] {
      get {
        return (resultMap["getGuardiansRequests"] as! [ResultMap]).map { (value: ResultMap) -> GetGuardiansRequest in GetGuardiansRequest(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetGuardiansRequest) -> ResultMap in value.resultMap }, forKey: "getGuardiansRequests")
      }
    }

    public struct GetGuardiansRequest: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Guardian"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(GuardianAccountInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var guardianAccountInfo: GuardianAccountInfo {
          get {
            return GuardianAccountInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class AssignDeviceTokenMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation AssignDeviceToken($deviceToken: String!) {
      assignDeviceToken(deviceToken: $deviceToken)
    }
    """

  public let operationName: String = "AssignDeviceToken"

  public var deviceToken: String

  public init(deviceToken: String) {
    self.deviceToken = deviceToken
  }

  public var variables: GraphQLMap? {
    return ["deviceToken": deviceToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("assignDeviceToken", arguments: ["deviceToken": GraphQLVariable("deviceToken")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(assignDeviceToken: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "assignDeviceToken": assignDeviceToken])
    }

    public var assignDeviceToken: Bool {
      get {
        return resultMap["assignDeviceToken"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "assignDeviceToken")
      }
    }
  }
}

public final class SosButtonPressedMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SosButtonPressed($buttonPressed: SosButtonPressedDto!) {
      sosButtonPressed(buttonPressed: $buttonPressed)
    }
    """

  public let operationName: String = "SosButtonPressed"

  public var buttonPressed: SosButtonPressedDto

  public init(buttonPressed: SosButtonPressedDto) {
    self.buttonPressed = buttonPressed
  }

  public var variables: GraphQLMap? {
    return ["buttonPressed": buttonPressed]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("sosButtonPressed", arguments: ["buttonPressed": GraphQLVariable("buttonPressed")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(sosButtonPressed: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "sosButtonPressed": sosButtonPressed])
    }

    public var sosButtonPressed: Bool {
      get {
        return resultMap["sosButtonPressed"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "sosButtonPressed")
      }
    }
  }
}

public final class SosButtonReleasedMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SosButtonReleased($buttonReleased: SosButtonReleasedDto!) {
      sosButtonReleased(buttonReleased: $buttonReleased)
    }
    """

  public let operationName: String = "SosButtonReleased"

  public var buttonReleased: SosButtonReleasedDto

  public init(buttonReleased: SosButtonReleasedDto) {
    self.buttonReleased = buttonReleased
  }

  public var variables: GraphQLMap? {
    return ["buttonReleased": buttonReleased]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("sosButtonReleased", arguments: ["buttonReleased": GraphQLVariable("buttonReleased")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(sosButtonReleased: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "sosButtonReleased": sosButtonReleased])
    }

    public var sosButtonReleased: Bool {
      get {
        return resultMap["sosButtonReleased"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "sosButtonReleased")
      }
    }
  }
}

public final class SosPinEnteredMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SosPinEntered($pinEntered: SosPinEnteredDto!) {
      sosPinEntered(pinEntered: $pinEntered)
    }
    """

  public let operationName: String = "SosPinEntered"

  public var pinEntered: SosPinEnteredDto

  public init(pinEntered: SosPinEnteredDto) {
    self.pinEntered = pinEntered
  }

  public var variables: GraphQLMap? {
    return ["pinEntered": pinEntered]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("sosPinEntered", arguments: ["pinEntered": GraphQLVariable("pinEntered")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(sosPinEntered: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "sosPinEntered": sosPinEntered])
    }

    public var sosPinEntered: Bool {
      get {
        return resultMap["sosPinEntered"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "sosPinEntered")
      }
    }
  }
}

public final class SosPositionUpdateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SosPositionUpdate($positionUpdate: SosPositionUpdateDto!) {
      sosPositionUpdate(positionUpdate: $positionUpdate)
    }
    """

  public let operationName: String = "SosPositionUpdate"

  public var positionUpdate: SosPositionUpdateDto

  public init(positionUpdate: SosPositionUpdateDto) {
    self.positionUpdate = positionUpdate
  }

  public var variables: GraphQLMap? {
    return ["positionUpdate": positionUpdate]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("sosPositionUpdate", arguments: ["positionUpdate": GraphQLVariable("positionUpdate")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(sosPositionUpdate: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "sosPositionUpdate": sosPositionUpdate])
    }

    public var sosPositionUpdate: Bool {
      get {
        return resultMap["sosPositionUpdate"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "sosPositionUpdate")
      }
    }
  }
}

public final class MySosHistoryQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query MySosHistory {
      getMySosHistory {
        __typename
        ...sosBasicInfo
      }
    }
    """

  public let operationName: String = "MySosHistory"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + SosBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getMySosHistory", type: .nonNull(.list(.nonNull(.object(GetMySosHistory.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getMySosHistory: [GetMySosHistory]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getMySosHistory": getMySosHistory.map { (value: GetMySosHistory) -> ResultMap in value.resultMap }])
    }

    public var getMySosHistory: [GetMySosHistory] {
      get {
        return (resultMap["getMySosHistory"] as! [ResultMap]).map { (value: ResultMap) -> GetMySosHistory in GetMySosHistory(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetMySosHistory) -> ResultMap in value.resultMap }, forKey: "getMySosHistory")
      }
    }

    public struct GetMySosHistory: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Sos"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(SosBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, idFromClient: GraphQLID, state: SosState, initialLatitude: Double? = nil, initialLongitude: Double? = nil, buttonPressedAt: String? = nil, buttonReleasedAt: String? = nil, pinEnteredAt: String? = nil, guardiansContactedAt: String? = nil, createdAt: String) {
        self.init(unsafeResultMap: ["__typename": "Sos", "id": id, "idFromClient": idFromClient, "state": state, "initialLatitude": initialLatitude, "initialLongitude": initialLongitude, "buttonPressedAt": buttonPressedAt, "buttonReleasedAt": buttonReleasedAt, "pinEnteredAt": pinEnteredAt, "guardiansContactedAt": guardiansContactedAt, "createdAt": createdAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var sosBasicInfo: SosBasicInfo {
          get {
            return SosBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class LastestSosByAccountQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query LastestSosByAccount($targetAccountId: ID!) {
      getLatestSosByAccount(targetAccountId: $targetAccountId) {
        __typename
        ...sosBasicInfo
      }
    }
    """

  public let operationName: String = "LastestSosByAccount"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + SosBasicInfo.fragmentDefinition)
    return document
  }

  public var targetAccountId: GraphQLID

  public init(targetAccountId: GraphQLID) {
    self.targetAccountId = targetAccountId
  }

  public var variables: GraphQLMap? {
    return ["targetAccountId": targetAccountId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getLatestSosByAccount", arguments: ["targetAccountId": GraphQLVariable("targetAccountId")], type: .nonNull(.object(GetLatestSosByAccount.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getLatestSosByAccount: GetLatestSosByAccount) {
      self.init(unsafeResultMap: ["__typename": "Query", "getLatestSosByAccount": getLatestSosByAccount.resultMap])
    }

    public var getLatestSosByAccount: GetLatestSosByAccount {
      get {
        return GetLatestSosByAccount(unsafeResultMap: resultMap["getLatestSosByAccount"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "getLatestSosByAccount")
      }
    }

    public struct GetLatestSosByAccount: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Sos"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(SosBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, idFromClient: GraphQLID, state: SosState, initialLatitude: Double? = nil, initialLongitude: Double? = nil, buttonPressedAt: String? = nil, buttonReleasedAt: String? = nil, pinEnteredAt: String? = nil, guardiansContactedAt: String? = nil, createdAt: String) {
        self.init(unsafeResultMap: ["__typename": "Sos", "id": id, "idFromClient": idFromClient, "state": state, "initialLatitude": initialLatitude, "initialLongitude": initialLongitude, "buttonPressedAt": buttonPressedAt, "buttonReleasedAt": buttonReleasedAt, "pinEnteredAt": pinEnteredAt, "guardiansContactedAt": guardiansContactedAt, "createdAt": createdAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var sosBasicInfo: SosBasicInfo {
          get {
            return SosBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class SosPositionsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query SosPositions($sosId: ID!) {
      getSosPositions(sosId: $sosId) {
        __typename
        ...sosPositionBasicInfo
      }
    }
    """

  public let operationName: String = "SosPositions"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + SosPositionBasicInfo.fragmentDefinition)
    return document
  }

  public var sosId: GraphQLID

  public init(sosId: GraphQLID) {
    self.sosId = sosId
  }

  public var variables: GraphQLMap? {
    return ["sosId": sosId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getSosPositions", arguments: ["sosId": GraphQLVariable("sosId")], type: .nonNull(.list(.nonNull(.object(GetSosPosition.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getSosPositions: [GetSosPosition]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getSosPositions": getSosPositions.map { (value: GetSosPosition) -> ResultMap in value.resultMap }])
    }

    public var getSosPositions: [GetSosPosition] {
      get {
        return (resultMap["getSosPositions"] as! [ResultMap]).map { (value: ResultMap) -> GetSosPosition in GetSosPosition(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetSosPosition) -> ResultMap in value.resultMap }, forKey: "getSosPositions")
      }
    }

    public struct GetSosPosition: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SosPosition"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(SosPositionBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, latitude: Double, longitude: Double, createdAt: String) {
        self.init(unsafeResultMap: ["__typename": "SosPosition", "id": id, "latitude": latitude, "longitude": longitude, "createdAt": createdAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var sosPositionBasicInfo: SosPositionBasicInfo {
          get {
            return SosPositionBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class SosOfAccountsIAmGuardianOfQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query SosOfAccountsIAmGuardianOf {
      getSosesOfAccountsIAmGuardianOf {
        __typename
        ...sosBasicInfo
        ...sosAccountInfo
      }
    }
    """

  public let operationName: String = "SosOfAccountsIAmGuardianOf"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + SosBasicInfo.fragmentDefinition)
    document.append("\n" + SosAccountInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getSosesOfAccountsIAmGuardianOf", type: .nonNull(.list(.nonNull(.object(GetSosesOfAccountsIAmGuardianOf.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getSosesOfAccountsIAmGuardianOf: [GetSosesOfAccountsIAmGuardianOf]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getSosesOfAccountsIAmGuardianOf": getSosesOfAccountsIAmGuardianOf.map { (value: GetSosesOfAccountsIAmGuardianOf) -> ResultMap in value.resultMap }])
    }

    public var getSosesOfAccountsIAmGuardianOf: [GetSosesOfAccountsIAmGuardianOf] {
      get {
        return (resultMap["getSosesOfAccountsIAmGuardianOf"] as! [ResultMap]).map { (value: ResultMap) -> GetSosesOfAccountsIAmGuardianOf in GetSosesOfAccountsIAmGuardianOf(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetSosesOfAccountsIAmGuardianOf) -> ResultMap in value.resultMap }, forKey: "getSosesOfAccountsIAmGuardianOf")
      }
    }

    public struct GetSosesOfAccountsIAmGuardianOf: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Sos"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(SosBasicInfo.self),
          GraphQLFragmentSpread(SosAccountInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var sosBasicInfo: SosBasicInfo {
          get {
            return SosBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var sosAccountInfo: SosAccountInfo {
          get {
            return SosAccountInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class CreateTripMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation CreateTrip($tripCreate: TripCreateDto!) {
      createTrip(tripCreate: $tripCreate) {
        __typename
        ...tripBasicInfo
      }
    }
    """

  public let operationName: String = "CreateTrip"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripBasicInfo.fragmentDefinition)
    document.append("\n" + TripDestinationBasicInfo.fragmentDefinition)
    document.append("\n" + TripQuickActionBasicInfo.fragmentDefinition)
    return document
  }

  public var tripCreate: TripCreateDto

  public init(tripCreate: TripCreateDto) {
    self.tripCreate = tripCreate
  }

  public var variables: GraphQLMap? {
    return ["tripCreate": tripCreate]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("createTrip", arguments: ["tripCreate": GraphQLVariable("tripCreate")], type: .nonNull(.object(CreateTrip.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createTrip: CreateTrip) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createTrip": createTrip.resultMap])
    }

    public var createTrip: CreateTrip {
      get {
        return CreateTrip(unsafeResultMap: resultMap["createTrip"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "createTrip")
      }
    }

    public struct CreateTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Trip"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripBasicInfo: TripBasicInfo {
          get {
            return TripBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class EndTripMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation EndTrip($tripId: ID!) {
      endTrip(tripId: $tripId)
    }
    """

  public let operationName: String = "EndTrip"

  public var tripId: GraphQLID

  public init(tripId: GraphQLID) {
    self.tripId = tripId
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("endTrip", arguments: ["tripId": GraphQLVariable("tripId")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(endTrip: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "endTrip": endTrip])
    }

    public var endTrip: Bool {
      get {
        return resultMap["endTrip"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "endTrip")
      }
    }
  }
}

public final class AddMessageToTripMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation AddMessageToTrip($tripId: ID!, $activityCreate: ActivityCreateDto!, $activityMessage: ActivityMessagePayload!) {
      addMessageToTrip(
        tripId: $tripId
        activityCreate: $activityCreate
        activityMessage: $activityMessage
      ) {
        __typename
        ...tripActivityBasicInfo
      }
    }
    """

  public let operationName: String = "AddMessageToTrip"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripActivityBasicInfo.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public var tripId: GraphQLID
  public var activityCreate: ActivityCreateDto
  public var activityMessage: ActivityMessagePayload

  public init(tripId: GraphQLID, activityCreate: ActivityCreateDto, activityMessage: ActivityMessagePayload) {
    self.tripId = tripId
    self.activityCreate = activityCreate
    self.activityMessage = activityMessage
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId, "activityCreate": activityCreate, "activityMessage": activityMessage]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("addMessageToTrip", arguments: ["tripId": GraphQLVariable("tripId"), "activityCreate": GraphQLVariable("activityCreate"), "activityMessage": GraphQLVariable("activityMessage")], type: .nonNull(.object(AddMessageToTrip.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(addMessageToTrip: AddMessageToTrip) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "addMessageToTrip": addMessageToTrip.resultMap])
    }

    public var addMessageToTrip: AddMessageToTrip {
      get {
        return AddMessageToTrip(unsafeResultMap: resultMap["addMessageToTrip"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "addMessageToTrip")
      }
    }

    public struct AddMessageToTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["TripActivity"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripActivityBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripActivityBasicInfo: TripActivityBasicInfo {
          get {
            return TripActivityBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class AddPhotoToTripMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation AddPhotoToTrip($tripId: ID!, $activityCreate: ActivityCreateDto!, $activityPhoto: ActivityPhotoPayload!) {
      addPhotoToTrip(
        tripId: $tripId
        activityCreate: $activityCreate
        activityPhoto: $activityPhoto
      ) {
        __typename
        ...tripActivityBasicInfo
      }
    }
    """

  public let operationName: String = "AddPhotoToTrip"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripActivityBasicInfo.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public var tripId: GraphQLID
  public var activityCreate: ActivityCreateDto
  public var activityPhoto: ActivityPhotoPayload

  public init(tripId: GraphQLID, activityCreate: ActivityCreateDto, activityPhoto: ActivityPhotoPayload) {
    self.tripId = tripId
    self.activityCreate = activityCreate
    self.activityPhoto = activityPhoto
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId, "activityCreate": activityCreate, "activityPhoto": activityPhoto]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("addPhotoToTrip", arguments: ["tripId": GraphQLVariable("tripId"), "activityCreate": GraphQLVariable("activityCreate"), "activityPhoto": GraphQLVariable("activityPhoto")], type: .nonNull(.object(AddPhotoToTrip.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(addPhotoToTrip: AddPhotoToTrip) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "addPhotoToTrip": addPhotoToTrip.resultMap])
    }

    public var addPhotoToTrip: AddPhotoToTrip {
      get {
        return AddPhotoToTrip(unsafeResultMap: resultMap["addPhotoToTrip"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "addPhotoToTrip")
      }
    }

    public struct AddPhotoToTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["TripActivity"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripActivityBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripActivityBasicInfo: TripActivityBasicInfo {
          get {
            return TripActivityBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class AddQuickActionToTripMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation AddQuickActionToTrip($tripId: ID!, $activityCreate: ActivityCreateDto!, $activityQuickAction: ActivityQuickActionPayload!) {
      addQuickActionToTrip(
        tripId: $tripId
        activityCreate: $activityCreate
        activityQuickAction: $activityQuickAction
      ) {
        __typename
        ...tripActivityBasicInfo
      }
    }
    """

  public let operationName: String = "AddQuickActionToTrip"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripActivityBasicInfo.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public var tripId: GraphQLID
  public var activityCreate: ActivityCreateDto
  public var activityQuickAction: ActivityQuickActionPayload

  public init(tripId: GraphQLID, activityCreate: ActivityCreateDto, activityQuickAction: ActivityQuickActionPayload) {
    self.tripId = tripId
    self.activityCreate = activityCreate
    self.activityQuickAction = activityQuickAction
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId, "activityCreate": activityCreate, "activityQuickAction": activityQuickAction]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("addQuickActionToTrip", arguments: ["tripId": GraphQLVariable("tripId"), "activityCreate": GraphQLVariable("activityCreate"), "activityQuickAction": GraphQLVariable("activityQuickAction")], type: .nonNull(.object(AddQuickActionToTrip.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(addQuickActionToTrip: AddQuickActionToTrip) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "addQuickActionToTrip": addQuickActionToTrip.resultMap])
    }

    public var addQuickActionToTrip: AddQuickActionToTrip {
      get {
        return AddQuickActionToTrip(unsafeResultMap: resultMap["addQuickActionToTrip"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "addQuickActionToTrip")
      }
    }

    public struct AddQuickActionToTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["TripActivity"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripActivityBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripActivityBasicInfo: TripActivityBasicInfo {
          get {
            return TripActivityBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class ToggleFavouriteTripDestinationMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ToggleFavouriteTripDestination($tripDestinationId: ID!) {
      toggleFavouriteTripDestination(tripDestinationId: $tripDestinationId)
    }
    """

  public let operationName: String = "ToggleFavouriteTripDestination"

  public var tripDestinationId: GraphQLID

  public init(tripDestinationId: GraphQLID) {
    self.tripDestinationId = tripDestinationId
  }

  public var variables: GraphQLMap? {
    return ["tripDestinationId": tripDestinationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("toggleFavouriteTripDestination", arguments: ["tripDestinationId": GraphQLVariable("tripDestinationId")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(toggleFavouriteTripDestination: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "toggleFavouriteTripDestination": toggleFavouriteTripDestination])
    }

    public var toggleFavouriteTripDestination: Bool {
      get {
        return resultMap["toggleFavouriteTripDestination"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "toggleFavouriteTripDestination")
      }
    }
  }
}

public final class ActiveTripQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query ActiveTrip {
      getActiveTrip {
        __typename
        ...tripBasicInfo
      }
    }
    """

  public let operationName: String = "ActiveTrip"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripBasicInfo.fragmentDefinition)
    document.append("\n" + TripDestinationBasicInfo.fragmentDefinition)
    document.append("\n" + TripQuickActionBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getActiveTrip", type: .object(GetActiveTrip.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getActiveTrip: GetActiveTrip? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getActiveTrip": getActiveTrip.flatMap { (value: GetActiveTrip) -> ResultMap in value.resultMap }])
    }

    public var getActiveTrip: GetActiveTrip? {
      get {
        return (resultMap["getActiveTrip"] as? ResultMap).flatMap { GetActiveTrip(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getActiveTrip")
      }
    }

    public struct GetActiveTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Trip"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripBasicInfo: TripBasicInfo {
          get {
            return TripBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class TripQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query Trip($tripId: ID!) {
      getTrip(tripId: $tripId) {
        __typename
        ...tripBasicInfo
        ...tripAccountInfo
      }
    }
    """

  public let operationName: String = "Trip"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripBasicInfo.fragmentDefinition)
    document.append("\n" + TripDestinationBasicInfo.fragmentDefinition)
    document.append("\n" + TripQuickActionBasicInfo.fragmentDefinition)
    document.append("\n" + TripAccountInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public var tripId: GraphQLID

  public init(tripId: GraphQLID) {
    self.tripId = tripId
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getTrip", arguments: ["tripId": GraphQLVariable("tripId")], type: .nonNull(.object(GetTrip.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getTrip: GetTrip) {
      self.init(unsafeResultMap: ["__typename": "Query", "getTrip": getTrip.resultMap])
    }

    public var getTrip: GetTrip {
      get {
        return GetTrip(unsafeResultMap: resultMap["getTrip"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "getTrip")
      }
    }

    public struct GetTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Trip"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripBasicInfo.self),
          GraphQLFragmentSpread(TripAccountInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripBasicInfo: TripBasicInfo {
          get {
            return TripBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var tripAccountInfo: TripAccountInfo {
          get {
            return TripAccountInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class MyTripsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query MyTrips {
      getMyTrips {
        __typename
        ...tripBasicInfo
      }
    }
    """

  public let operationName: String = "MyTrips"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripBasicInfo.fragmentDefinition)
    document.append("\n" + TripDestinationBasicInfo.fragmentDefinition)
    document.append("\n" + TripQuickActionBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getMyTrips", type: .nonNull(.list(.nonNull(.object(GetMyTrip.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getMyTrips: [GetMyTrip]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getMyTrips": getMyTrips.map { (value: GetMyTrip) -> ResultMap in value.resultMap }])
    }

    public var getMyTrips: [GetMyTrip] {
      get {
        return (resultMap["getMyTrips"] as! [ResultMap]).map { (value: ResultMap) -> GetMyTrip in GetMyTrip(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetMyTrip) -> ResultMap in value.resultMap }, forKey: "getMyTrips")
      }
    }

    public struct GetMyTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Trip"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripBasicInfo: TripBasicInfo {
          get {
            return TripBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class MyGuardiansTripsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query MyGuardiansTrips {
      getMyGuardiansTrips {
        __typename
        ...tripBasicInfo
        ...tripAccountInfo
      }
    }
    """

  public let operationName: String = "MyGuardiansTrips"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripBasicInfo.fragmentDefinition)
    document.append("\n" + TripDestinationBasicInfo.fragmentDefinition)
    document.append("\n" + TripQuickActionBasicInfo.fragmentDefinition)
    document.append("\n" + TripAccountInfo.fragmentDefinition)
    document.append("\n" + AccountBasicInfo.fragmentDefinition)
    document.append("\n" + AccountProfilePicture.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getMyGuardiansTrips", type: .nonNull(.list(.nonNull(.object(GetMyGuardiansTrip.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getMyGuardiansTrips: [GetMyGuardiansTrip]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getMyGuardiansTrips": getMyGuardiansTrips.map { (value: GetMyGuardiansTrip) -> ResultMap in value.resultMap }])
    }

    public var getMyGuardiansTrips: [GetMyGuardiansTrip] {
      get {
        return (resultMap["getMyGuardiansTrips"] as! [ResultMap]).map { (value: ResultMap) -> GetMyGuardiansTrip in GetMyGuardiansTrip(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetMyGuardiansTrip) -> ResultMap in value.resultMap }, forKey: "getMyGuardiansTrips")
      }
    }

    public struct GetMyGuardiansTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Trip"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripBasicInfo.self),
          GraphQLFragmentSpread(TripAccountInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripBasicInfo: TripBasicInfo {
          get {
            return TripBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var tripAccountInfo: TripAccountInfo {
          get {
            return TripAccountInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class ActivitiesByTripQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query ActivitiesByTrip($tripId: ID!) {
      getActivitiesByTrip(tripId: $tripId) {
        __typename
        ...tripActivityBasicInfo
      }
    }
    """

  public let operationName: String = "ActivitiesByTrip"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripActivityBasicInfo.fragmentDefinition)
    document.append("\n" + FileBasicInfo.fragmentDefinition)
    return document
  }

  public var tripId: GraphQLID

  public init(tripId: GraphQLID) {
    self.tripId = tripId
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getActivitiesByTrip", arguments: ["tripId": GraphQLVariable("tripId")], type: .nonNull(.list(.nonNull(.object(GetActivitiesByTrip.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getActivitiesByTrip: [GetActivitiesByTrip]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getActivitiesByTrip": getActivitiesByTrip.map { (value: GetActivitiesByTrip) -> ResultMap in value.resultMap }])
    }

    public var getActivitiesByTrip: [GetActivitiesByTrip] {
      get {
        return (resultMap["getActivitiesByTrip"] as! [ResultMap]).map { (value: ResultMap) -> GetActivitiesByTrip in GetActivitiesByTrip(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetActivitiesByTrip) -> ResultMap in value.resultMap }, forKey: "getActivitiesByTrip")
      }
    }

    public struct GetActivitiesByTrip: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["TripActivity"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripActivityBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripActivityBasicInfo: TripActivityBasicInfo {
          get {
            return TripActivityBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class MyTripDestinationsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query MyTripDestinations {
      getMyTripDestinations {
        __typename
        ...tripDestinationBasicInfo
      }
    }
    """

  public let operationName: String = "MyTripDestinations"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + TripDestinationBasicInfo.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getMyTripDestinations", type: .nonNull(.list(.nonNull(.object(GetMyTripDestination.selections))))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getMyTripDestinations: [GetMyTripDestination]) {
      self.init(unsafeResultMap: ["__typename": "Query", "getMyTripDestinations": getMyTripDestinations.map { (value: GetMyTripDestination) -> ResultMap in value.resultMap }])
    }

    public var getMyTripDestinations: [GetMyTripDestination] {
      get {
        return (resultMap["getMyTripDestinations"] as! [ResultMap]).map { (value: ResultMap) -> GetMyTripDestination in GetMyTripDestination(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: GetMyTripDestination) -> ResultMap in value.resultMap }, forKey: "getMyTripDestinations")
      }
    }

    public struct GetMyTripDestination: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["TripDestination"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(TripDestinationBasicInfo.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, name: String, latitude: Double, longitude: Double, favourite: Bool, createdAt: String) {
        self.init(unsafeResultMap: ["__typename": "TripDestination", "id": id, "name": name, "latitude": latitude, "longitude": longitude, "favourite": favourite, "createdAt": createdAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var tripDestinationBasicInfo: TripDestinationBasicInfo {
          get {
            return TripDestinationBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public struct AccountBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment accountBasicInfo on Account {
      __typename
      id
      username
      email
    }
    """

  public static let possibleTypes: [String] = ["Account"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("username", type: .scalar(String.self)),
      GraphQLField("email", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, username: String? = nil, email: String) {
    self.init(unsafeResultMap: ["__typename": "Account", "id": id, "username": username, "email": email])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var username: String? {
    get {
      return resultMap["username"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "username")
    }
  }

  public var email: String {
    get {
      return resultMap["email"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "email")
    }
  }
}

public struct AccountProfilePicture: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment accountProfilePicture on Account {
      __typename
      id
      profilePicture {
        __typename
        ...fileBasicInfo
      }
    }
    """

  public static let possibleTypes: [String] = ["Account"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("profilePicture", type: .object(ProfilePicture.selections)),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, profilePicture: ProfilePicture? = nil) {
    self.init(unsafeResultMap: ["__typename": "Account", "id": id, "profilePicture": profilePicture.flatMap { (value: ProfilePicture) -> ResultMap in value.resultMap }])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var profilePicture: ProfilePicture? {
    get {
      return (resultMap["profilePicture"] as? ResultMap).flatMap { ProfilePicture(unsafeResultMap: $0) }
    }
    set {
      resultMap.updateValue(newValue?.resultMap, forKey: "profilePicture")
    }
  }

  public struct ProfilePicture: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["File"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(FileBasicInfo.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(id: GraphQLID, path: String) {
      self.init(unsafeResultMap: ["__typename": "File", "id": id, "path": path])
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var fileBasicInfo: FileBasicInfo {
        get {
          return FileBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct CheckinRequestBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment checkinRequestBasicInfo on CheckinRequest {
      __typename
      id
      type
      state
      startsAt
      endsAt
      periodInMinutes
      nextCheckinAt
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["CheckinRequest"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("type", type: .nonNull(.scalar(CheckinRequestType.self))),
      GraphQLField("state", type: .nonNull(.scalar(CheckinRequestState.self))),
      GraphQLField("startsAt", type: .scalar(String.self)),
      GraphQLField("endsAt", type: .scalar(String.self)),
      GraphQLField("periodInMinutes", type: .scalar(Double.self)),
      GraphQLField("nextCheckinAt", type: .scalar(String.self)),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, type: CheckinRequestType, state: CheckinRequestState, startsAt: String? = nil, endsAt: String? = nil, periodInMinutes: Double? = nil, nextCheckinAt: String? = nil, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "CheckinRequest", "id": id, "type": type, "state": state, "startsAt": startsAt, "endsAt": endsAt, "periodInMinutes": periodInMinutes, "nextCheckinAt": nextCheckinAt, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var type: CheckinRequestType {
    get {
      return resultMap["type"]! as! CheckinRequestType
    }
    set {
      resultMap.updateValue(newValue, forKey: "type")
    }
  }

  public var state: CheckinRequestState {
    get {
      return resultMap["state"]! as! CheckinRequestState
    }
    set {
      resultMap.updateValue(newValue, forKey: "state")
    }
  }

  public var startsAt: String? {
    get {
      return resultMap["startsAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "startsAt")
    }
  }

  public var endsAt: String? {
    get {
      return resultMap["endsAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "endsAt")
    }
  }

  public var periodInMinutes: Double? {
    get {
      return resultMap["periodInMinutes"] as? Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "periodInMinutes")
    }
  }

  public var nextCheckinAt: String? {
    get {
      return resultMap["nextCheckinAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "nextCheckinAt")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }
}

public struct CheckinRequestRequesterInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment checkinRequestRequesterInfo on CheckinRequest {
      __typename
      id
      requester {
        __typename
        ...accountBasicInfo
        ...accountProfilePicture
      }
    }
    """

  public static let possibleTypes: [String] = ["CheckinRequest"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, requester: Requester) {
    self.init(unsafeResultMap: ["__typename": "CheckinRequest", "id": id, "requester": requester.resultMap])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var requester: Requester {
    get {
      return Requester(unsafeResultMap: resultMap["requester"]! as! ResultMap)
    }
    set {
      resultMap.updateValue(newValue.resultMap, forKey: "requester")
    }
  }

  public struct Requester: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Account"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(AccountBasicInfo.self),
        GraphQLFragmentSpread(AccountProfilePicture.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var accountBasicInfo: AccountBasicInfo {
        get {
          return AccountBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public var accountProfilePicture: AccountProfilePicture {
        get {
          return AccountProfilePicture(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct CheckinRequestTargetInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment checkinRequestTargetInfo on CheckinRequest {
      __typename
      id
      targetAccount {
        __typename
        ...accountBasicInfo
        ...accountProfilePicture
      }
    }
    """

  public static let possibleTypes: [String] = ["CheckinRequest"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("targetAccount", type: .nonNull(.object(TargetAccount.selections))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, targetAccount: TargetAccount) {
    self.init(unsafeResultMap: ["__typename": "CheckinRequest", "id": id, "targetAccount": targetAccount.resultMap])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var targetAccount: TargetAccount {
    get {
      return TargetAccount(unsafeResultMap: resultMap["targetAccount"]! as! ResultMap)
    }
    set {
      resultMap.updateValue(newValue.resultMap, forKey: "targetAccount")
    }
  }

  public struct TargetAccount: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Account"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(AccountBasicInfo.self),
        GraphQLFragmentSpread(AccountProfilePicture.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var accountBasicInfo: AccountBasicInfo {
        get {
          return AccountBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public var accountProfilePicture: AccountProfilePicture {
        get {
          return AccountProfilePicture(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct CheckinRequestGuardiansInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment checkinRequestGuardiansInfo on CheckinRequest {
      __typename
      id
      guardians {
        __typename
        id
        account {
          __typename
          ...accountBasicInfo
          ...accountProfilePicture
        }
      }
    }
    """

  public static let possibleTypes: [String] = ["CheckinRequest"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("guardians", type: .nonNull(.list(.nonNull(.object(Guardian.selections))))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, guardians: [Guardian]) {
    self.init(unsafeResultMap: ["__typename": "CheckinRequest", "id": id, "guardians": guardians.map { (value: Guardian) -> ResultMap in value.resultMap }])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var guardians: [Guardian] {
    get {
      return (resultMap["guardians"] as! [ResultMap]).map { (value: ResultMap) -> Guardian in Guardian(unsafeResultMap: value) }
    }
    set {
      resultMap.updateValue(newValue.map { (value: Guardian) -> ResultMap in value.resultMap }, forKey: "guardians")
    }
  }

  public struct Guardian: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["CheckinRequestGuardian"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("account", type: .nonNull(.object(Account.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(id: GraphQLID, account: Account) {
      self.init(unsafeResultMap: ["__typename": "CheckinRequestGuardian", "id": id, "account": account.resultMap])
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var id: GraphQLID {
      get {
        return resultMap["id"]! as! GraphQLID
      }
      set {
        resultMap.updateValue(newValue, forKey: "id")
      }
    }

    public var account: Account {
      get {
        return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "account")
      }
    }

    public struct Account: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Account"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(AccountBasicInfo.self),
          GraphQLFragmentSpread(AccountProfilePicture.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var accountBasicInfo: AccountBasicInfo {
          get {
            return AccountBasicInfo(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }

        public var accountProfilePicture: AccountProfilePicture {
          get {
            return AccountProfilePicture(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public struct CheckinBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment checkinBasicInfo on Checkin {
      __typename
      id
      latitude
      longitude
      state
      checkedAt
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["Checkin"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("latitude", type: .scalar(Double.self)),
      GraphQLField("longitude", type: .scalar(Double.self)),
      GraphQLField("state", type: .nonNull(.scalar(CheckinState.self))),
      GraphQLField("checkedAt", type: .scalar(String.self)),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, latitude: Double? = nil, longitude: Double? = nil, state: CheckinState, checkedAt: String? = nil, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "Checkin", "id": id, "latitude": latitude, "longitude": longitude, "state": state, "checkedAt": checkedAt, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var latitude: Double? {
    get {
      return resultMap["latitude"] as? Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Double? {
    get {
      return resultMap["longitude"] as? Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "longitude")
    }
  }

  public var state: CheckinState {
    get {
      return resultMap["state"]! as! CheckinState
    }
    set {
      resultMap.updateValue(newValue, forKey: "state")
    }
  }

  public var checkedAt: String? {
    get {
      return resultMap["checkedAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "checkedAt")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }
}

public struct CheckinRequestInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment checkinRequestInfo on Checkin {
      __typename
      id
      request {
        __typename
        ...checkinRequestBasicInfo
        ...checkinRequestRequesterInfo
      }
    }
    """

  public static let possibleTypes: [String] = ["Checkin"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("request", type: .nonNull(.object(Request.selections))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, request: Request) {
    self.init(unsafeResultMap: ["__typename": "Checkin", "id": id, "request": request.resultMap])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var request: Request {
    get {
      return Request(unsafeResultMap: resultMap["request"]! as! ResultMap)
    }
    set {
      resultMap.updateValue(newValue.resultMap, forKey: "request")
    }
  }

  public struct Request: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["CheckinRequest"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(CheckinRequestBasicInfo.self),
        GraphQLFragmentSpread(CheckinRequestRequesterInfo.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var checkinRequestBasicInfo: CheckinRequestBasicInfo {
        get {
          return CheckinRequestBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public var checkinRequestRequesterInfo: CheckinRequestRequesterInfo {
        get {
          return CheckinRequestRequesterInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct CheckinRequestGuardianBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment checkinRequestGuardianBasicInfo on CheckinRequestGuardian {
      __typename
      id
      account {
        __typename
        ...accountBasicInfo
        ...accountProfilePicture
      }
    }
    """

  public static let possibleTypes: [String] = ["CheckinRequestGuardian"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("account", type: .nonNull(.object(Account.selections))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, account: Account) {
    self.init(unsafeResultMap: ["__typename": "CheckinRequestGuardian", "id": id, "account": account.resultMap])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var account: Account {
    get {
      return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
    }
    set {
      resultMap.updateValue(newValue.resultMap, forKey: "account")
    }
  }

  public struct Account: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Account"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(AccountBasicInfo.self),
        GraphQLFragmentSpread(AccountProfilePicture.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var accountBasicInfo: AccountBasicInfo {
        get {
          return AccountBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public var accountProfilePicture: AccountProfilePicture {
        get {
          return AccountProfilePicture(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct FileBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment fileBasicInfo on File {
      __typename
      id
      path
    }
    """

  public static let possibleTypes: [String] = ["File"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("path", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, path: String) {
    self.init(unsafeResultMap: ["__typename": "File", "id": id, "path": path])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var path: String {
    get {
      return resultMap["path"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "path")
    }
  }
}

public struct GuardianBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment guardianBasicInfo on Guardian {
      __typename
      id
      guardian {
        __typename
        ...accountBasicInfo
        ...accountProfilePicture
      }
      acceptedAt
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["Guardian"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("guardian", type: .nonNull(.object(Guardian.selections))),
      GraphQLField("acceptedAt", type: .scalar(String.self)),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, guardian: Guardian, acceptedAt: String? = nil, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "Guardian", "id": id, "guardian": guardian.resultMap, "acceptedAt": acceptedAt, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var guardian: Guardian {
    get {
      return Guardian(unsafeResultMap: resultMap["guardian"]! as! ResultMap)
    }
    set {
      resultMap.updateValue(newValue.resultMap, forKey: "guardian")
    }
  }

  public var acceptedAt: String? {
    get {
      return resultMap["acceptedAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "acceptedAt")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public struct Guardian: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Account"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(AccountBasicInfo.self),
        GraphQLFragmentSpread(AccountProfilePicture.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var accountBasicInfo: AccountBasicInfo {
        get {
          return AccountBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public var accountProfilePicture: AccountProfilePicture {
        get {
          return AccountProfilePicture(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct GuardianAccountInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment guardianAccountInfo on Guardian {
      __typename
      id
      account {
        __typename
        ...accountBasicInfo
        ...accountProfilePicture
      }
      acceptedAt
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["Guardian"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("account", type: .nonNull(.object(Account.selections))),
      GraphQLField("acceptedAt", type: .scalar(String.self)),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, account: Account, acceptedAt: String? = nil, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "Guardian", "id": id, "account": account.resultMap, "acceptedAt": acceptedAt, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var account: Account {
    get {
      return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
    }
    set {
      resultMap.updateValue(newValue.resultMap, forKey: "account")
    }
  }

  public var acceptedAt: String? {
    get {
      return resultMap["acceptedAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "acceptedAt")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public struct Account: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Account"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(AccountBasicInfo.self),
        GraphQLFragmentSpread(AccountProfilePicture.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var accountBasicInfo: AccountBasicInfo {
        get {
          return AccountBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public var accountProfilePicture: AccountProfilePicture {
        get {
          return AccountProfilePicture(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct SosBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment sosBasicInfo on Sos {
      __typename
      id
      idFromClient
      state
      initialLatitude
      initialLongitude
      buttonPressedAt
      buttonReleasedAt
      pinEnteredAt
      guardiansContactedAt
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["Sos"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("idFromClient", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("state", type: .nonNull(.scalar(SosState.self))),
      GraphQLField("initialLatitude", type: .scalar(Double.self)),
      GraphQLField("initialLongitude", type: .scalar(Double.self)),
      GraphQLField("buttonPressedAt", type: .scalar(String.self)),
      GraphQLField("buttonReleasedAt", type: .scalar(String.self)),
      GraphQLField("pinEnteredAt", type: .scalar(String.self)),
      GraphQLField("guardiansContactedAt", type: .scalar(String.self)),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, idFromClient: GraphQLID, state: SosState, initialLatitude: Double? = nil, initialLongitude: Double? = nil, buttonPressedAt: String? = nil, buttonReleasedAt: String? = nil, pinEnteredAt: String? = nil, guardiansContactedAt: String? = nil, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "Sos", "id": id, "idFromClient": idFromClient, "state": state, "initialLatitude": initialLatitude, "initialLongitude": initialLongitude, "buttonPressedAt": buttonPressedAt, "buttonReleasedAt": buttonReleasedAt, "pinEnteredAt": pinEnteredAt, "guardiansContactedAt": guardiansContactedAt, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var idFromClient: GraphQLID {
    get {
      return resultMap["idFromClient"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "idFromClient")
    }
  }

  public var state: SosState {
    get {
      return resultMap["state"]! as! SosState
    }
    set {
      resultMap.updateValue(newValue, forKey: "state")
    }
  }

  public var initialLatitude: Double? {
    get {
      return resultMap["initialLatitude"] as? Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "initialLatitude")
    }
  }

  public var initialLongitude: Double? {
    get {
      return resultMap["initialLongitude"] as? Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "initialLongitude")
    }
  }

  public var buttonPressedAt: String? {
    get {
      return resultMap["buttonPressedAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "buttonPressedAt")
    }
  }

  public var buttonReleasedAt: String? {
    get {
      return resultMap["buttonReleasedAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "buttonReleasedAt")
    }
  }

  public var pinEnteredAt: String? {
    get {
      return resultMap["pinEnteredAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "pinEnteredAt")
    }
  }

  public var guardiansContactedAt: String? {
    get {
      return resultMap["guardiansContactedAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "guardiansContactedAt")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }
}

public struct SosAccountInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment sosAccountInfo on Sos {
      __typename
      id
      account {
        __typename
        ...accountBasicInfo
        ...accountProfilePicture
      }
    }
    """

  public static let possibleTypes: [String] = ["Sos"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("account", type: .nonNull(.object(Account.selections))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, account: Account) {
    self.init(unsafeResultMap: ["__typename": "Sos", "id": id, "account": account.resultMap])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var account: Account {
    get {
      return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
    }
    set {
      resultMap.updateValue(newValue.resultMap, forKey: "account")
    }
  }

  public struct Account: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Account"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(AccountBasicInfo.self),
        GraphQLFragmentSpread(AccountProfilePicture.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var accountBasicInfo: AccountBasicInfo {
        get {
          return AccountBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public var accountProfilePicture: AccountProfilePicture {
        get {
          return AccountProfilePicture(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct SosPositionBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment sosPositionBasicInfo on SosPosition {
      __typename
      id
      latitude
      longitude
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["SosPosition"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
      GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, latitude: Double, longitude: Double, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "SosPosition", "id": id, "latitude": latitude, "longitude": longitude, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var latitude: Double {
    get {
      return resultMap["latitude"]! as! Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Double {
    get {
      return resultMap["longitude"]! as! Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "longitude")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }
}

public struct TripBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment tripBasicInfo on Trip {
      __typename
      id
      state
      shouldArriveAt
      destination {
        __typename
        ...tripDestinationBasicInfo
      }
      quickActions {
        __typename
        ...tripQuickActionBasicInfo
      }
      currentQuickActionIndex
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["Trip"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("state", type: .nonNull(.scalar(TripState.self))),
      GraphQLField("shouldArriveAt", type: .scalar(String.self)),
      GraphQLField("destination", type: .object(Destination.selections)),
      GraphQLField("quickActions", type: .nonNull(.list(.nonNull(.object(QuickAction.selections))))),
      GraphQLField("currentQuickActionIndex", type: .nonNull(.scalar(Int.self))),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, state: TripState, shouldArriveAt: String? = nil, destination: Destination? = nil, quickActions: [QuickAction], currentQuickActionIndex: Int, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "Trip", "id": id, "state": state, "shouldArriveAt": shouldArriveAt, "destination": destination.flatMap { (value: Destination) -> ResultMap in value.resultMap }, "quickActions": quickActions.map { (value: QuickAction) -> ResultMap in value.resultMap }, "currentQuickActionIndex": currentQuickActionIndex, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var state: TripState {
    get {
      return resultMap["state"]! as! TripState
    }
    set {
      resultMap.updateValue(newValue, forKey: "state")
    }
  }

  public var shouldArriveAt: String? {
    get {
      return resultMap["shouldArriveAt"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "shouldArriveAt")
    }
  }

  public var destination: Destination? {
    get {
      return (resultMap["destination"] as? ResultMap).flatMap { Destination(unsafeResultMap: $0) }
    }
    set {
      resultMap.updateValue(newValue?.resultMap, forKey: "destination")
    }
  }

  public var quickActions: [QuickAction] {
    get {
      return (resultMap["quickActions"] as! [ResultMap]).map { (value: ResultMap) -> QuickAction in QuickAction(unsafeResultMap: value) }
    }
    set {
      resultMap.updateValue(newValue.map { (value: QuickAction) -> ResultMap in value.resultMap }, forKey: "quickActions")
    }
  }

  public var currentQuickActionIndex: Int {
    get {
      return resultMap["currentQuickActionIndex"]! as! Int
    }
    set {
      resultMap.updateValue(newValue, forKey: "currentQuickActionIndex")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public struct Destination: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["TripDestination"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(TripDestinationBasicInfo.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(id: GraphQLID, name: String, latitude: Double, longitude: Double, favourite: Bool, createdAt: String) {
      self.init(unsafeResultMap: ["__typename": "TripDestination", "id": id, "name": name, "latitude": latitude, "longitude": longitude, "favourite": favourite, "createdAt": createdAt])
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var tripDestinationBasicInfo: TripDestinationBasicInfo {
        get {
          return TripDestinationBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }

  public struct QuickAction: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["TripQuickAction"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(TripQuickActionBasicInfo.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(id: String, name: String, icon: String) {
      self.init(unsafeResultMap: ["__typename": "TripQuickAction", "id": id, "name": name, "icon": icon])
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var tripQuickActionBasicInfo: TripQuickActionBasicInfo {
        get {
          return TripQuickActionBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct TripAccountInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment tripAccountInfo on Trip {
      __typename
      id
      account {
        __typename
        ...accountBasicInfo
        ...accountProfilePicture
      }
    }
    """

  public static let possibleTypes: [String] = ["Trip"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("account", type: .nonNull(.object(Account.selections))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, account: Account) {
    self.init(unsafeResultMap: ["__typename": "Trip", "id": id, "account": account.resultMap])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var account: Account {
    get {
      return Account(unsafeResultMap: resultMap["account"]! as! ResultMap)
    }
    set {
      resultMap.updateValue(newValue.resultMap, forKey: "account")
    }
  }

  public struct Account: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Account"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(AccountBasicInfo.self),
        GraphQLFragmentSpread(AccountProfilePicture.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var accountBasicInfo: AccountBasicInfo {
        get {
          return AccountBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public var accountProfilePicture: AccountProfilePicture {
        get {
          return AccountProfilePicture(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct TripQuickActionBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment tripQuickActionBasicInfo on TripQuickAction {
      __typename
      id
      name
      icon
    }
    """

  public static let possibleTypes: [String] = ["TripQuickAction"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(String.self))),
      GraphQLField("name", type: .nonNull(.scalar(String.self))),
      GraphQLField("icon", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: String, name: String, icon: String) {
    self.init(unsafeResultMap: ["__typename": "TripQuickAction", "id": id, "name": name, "icon": icon])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: String {
    get {
      return resultMap["id"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var name: String {
    get {
      return resultMap["name"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "name")
    }
  }

  public var icon: String {
    get {
      return resultMap["icon"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "icon")
    }
  }
}

public struct TripActivityBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment tripActivityBasicInfo on TripActivity {
      __typename
      id
      key
      latitude
      longitude
      batteryLevel
      payload
      attachment {
        __typename
        ...fileBasicInfo
      }
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["TripActivity"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("key", type: .nonNull(.scalar(TripActivityKey.self))),
      GraphQLField("latitude", type: .scalar(Double.self)),
      GraphQLField("longitude", type: .scalar(Double.self)),
      GraphQLField("batteryLevel", type: .scalar(Double.self)),
      GraphQLField("payload", type: .nonNull(.scalar(String.self))),
      GraphQLField("attachment", type: .object(Attachment.selections)),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, key: TripActivityKey, latitude: Double? = nil, longitude: Double? = nil, batteryLevel: Double? = nil, payload: String, attachment: Attachment? = nil, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "TripActivity", "id": id, "key": key, "latitude": latitude, "longitude": longitude, "batteryLevel": batteryLevel, "payload": payload, "attachment": attachment.flatMap { (value: Attachment) -> ResultMap in value.resultMap }, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var key: TripActivityKey {
    get {
      return resultMap["key"]! as! TripActivityKey
    }
    set {
      resultMap.updateValue(newValue, forKey: "key")
    }
  }

  public var latitude: Double? {
    get {
      return resultMap["latitude"] as? Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Double? {
    get {
      return resultMap["longitude"] as? Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "longitude")
    }
  }

  public var batteryLevel: Double? {
    get {
      return resultMap["batteryLevel"] as? Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "batteryLevel")
    }
  }

  public var payload: String {
    get {
      return resultMap["payload"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "payload")
    }
  }

  public var attachment: Attachment? {
    get {
      return (resultMap["attachment"] as? ResultMap).flatMap { Attachment(unsafeResultMap: $0) }
    }
    set {
      resultMap.updateValue(newValue?.resultMap, forKey: "attachment")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public struct Attachment: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["File"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLFragmentSpread(FileBasicInfo.self),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(id: GraphQLID, path: String) {
      self.init(unsafeResultMap: ["__typename": "File", "id": id, "path": path])
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    public var fragments: Fragments {
      get {
        return Fragments(unsafeResultMap: resultMap)
      }
      set {
        resultMap += newValue.resultMap
      }
    }

    public struct Fragments {
      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public var fileBasicInfo: FileBasicInfo {
        get {
          return FileBasicInfo(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }
    }
  }
}

public struct TripDestinationBasicInfo: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment tripDestinationBasicInfo on TripDestination {
      __typename
      id
      name
      latitude
      longitude
      favourite
      createdAt
    }
    """

  public static let possibleTypes: [String] = ["TripDestination"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
      GraphQLField("name", type: .nonNull(.scalar(String.self))),
      GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
      GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
      GraphQLField("favourite", type: .nonNull(.scalar(Bool.self))),
      GraphQLField("createdAt", type: .nonNull(.scalar(String.self))),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID, name: String, latitude: Double, longitude: Double, favourite: Bool, createdAt: String) {
    self.init(unsafeResultMap: ["__typename": "TripDestination", "id": id, "name": name, "latitude": latitude, "longitude": longitude, "favourite": favourite, "createdAt": createdAt])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID {
    get {
      return resultMap["id"]! as! GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var name: String {
    get {
      return resultMap["name"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "name")
    }
  }

  public var latitude: Double {
    get {
      return resultMap["latitude"]! as! Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Double {
    get {
      return resultMap["longitude"]! as! Double
    }
    set {
      resultMap.updateValue(newValue, forKey: "longitude")
    }
  }

  public var favourite: Bool {
    get {
      return resultMap["favourite"]! as! Bool
    }
    set {
      resultMap.updateValue(newValue, forKey: "favourite")
    }
  }

  public var createdAt: String {
    get {
      return resultMap["createdAt"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "createdAt")
    }
  }
}
