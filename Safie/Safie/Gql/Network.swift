//
//  Apollo.swift
//  Fooder
//
//  Created by Matyáš Procházka on 28.06.2021.
//

import Foundation
import Apollo

public enum ApiResult<Success, Failure> where Failure : Error {
    case success(Success)
    case failure(Failure)
}

public func formatError(graphqlErrors: [GraphQLError]) -> String {
    var errors: [String] = []
    
    for graphqlError in graphqlErrors {
        print(graphqlError)
        
        guard let extensions = graphqlError.extensions else {
            continue
        }
        
        print(extensions)
        
        guard let exception = extensions["exception"] as? [String: Any] else {
            continue
        }
        
        guard let apiErrors = exception["errors"] as? [AnyObject] else {
            continue
        }
        
        for error in apiErrors {            
            if let message = error["message"] as? String {
                errors.append(message)
            }
        }
    }
    
    return errors.joined(separator: " ")
}

class Network {
    static var shared = Network()
    
    static func reset() -> Void {
        Network.shared = Network()
    }
      
    // Main ApolloClient
    private(set) lazy var apollo: ApolloClient = {
        // The cache is necessary to set up the store, which we're going to hand to the provider
        let cache = InMemoryNormalizedCache()
        let store = ApolloStore(cache: cache)

        let client = URLSessionClient()
        // Append api token interceptor and token expired interceptor
        let provider = NetworkInterceptorProvider(client: client, store: store)

        let requestChainTransport = RequestChainNetworkTransport(interceptorProvider: provider, endpointURL: AppConfig.gqlUrl)
                                                                   
        let tmpApollo = ApolloClient(networkTransport: requestChainTransport, store: store)
        
        tmpApollo.cacheKeyForObject = { "\($0["__typename"] ?? "")_\($0["id"] ?? "")" }
        
        return tmpApollo
    }()
    
    // ApolloClient for refreshing tokens
    private(set) lazy var apolloRefresh: ApolloClient = {
        // The cache is necessary to set up the store, which we're going to hand to the provider
        let cache = InMemoryNormalizedCache()
        let store = ApolloStore(cache: cache)

        let client = URLSessionClient()
        // Append refresh token interceptor
        let provider = NetworkRefreshInterceptorProvider(client: client, store: store)

        let requestChainTransport = RequestChainNetworkTransport(interceptorProvider: provider, endpointURL: AppConfig.gqlUrl)
                                                                   
        return ApolloClient(networkTransport: requestChainTransport, store: store)
    }()
}

// Insert api token and token expired interceptors
// The positions matter
// The positions are taken from Apollo docs
class NetworkInterceptorProvider: LegacyInterceptorProvider {
    override func interceptors<Operation: GraphQLOperation>(for operation: Operation) -> [ApolloInterceptor] {
        var interceptors = super.interceptors(for: operation)
        interceptors.insert(AddApiTokenInterceptor(), at: 0)
        interceptors.insert(TokenExpiredInterceptor(), at: 6)
        return interceptors
    }
}

// Insert refresh token interceptor
class NetworkRefreshInterceptorProvider: LegacyInterceptorProvider {
    override func interceptors<Operation: GraphQLOperation>(for operation: Operation) -> [ApolloInterceptor] {
        var interceptors = super.interceptors(for: operation)
        interceptors.insert(AddRefreshTokenInterceptor(), at: 0)
        return interceptors
    }
}

// Api token expire interceptor
// Refresh the tokens and then retry one more time the initial request
// If the refresh fails, log out the user (that's done in the auth model)
class TokenExpiredInterceptor: ApolloInterceptor {
    var authModel = DI.shared.container.resolve(AuthModel.self)!
    
    func interceptAsync<Operation: GraphQLOperation>(chain: RequestChain, request: HTTPRequest<Operation>, response: HTTPResponse<Operation>?, completion: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
        
        if let errors = response?.parsedResponse?.errors, let error = errors.first {
            if "\(error)" == "Unauthorized" {
                self.authModel.refresh(callback: {
                    chain.retry(request: request, completion: completion)
                })
            } else {
                chain.proceedAsync(request: request, response: response, completion: completion)
            }
        } else {
            chain.proceedAsync(request: request, response: response, completion: completion)
        }
        
        //chain.retry(request: request, completion: completion)
        
        //Network.shared.apollo
        
        //chain.retry(request: request, completion: completion)
    }
    
}

class AddApiTokenInterceptor: ApolloInterceptor {
    var authModel = DI.shared.container.resolve(AuthModel.self)!
    
    func interceptAsync<Operation: GraphQLOperation>(chain: RequestChain, request: HTTPRequest<Operation>, response: HTTPResponse<Operation>?, completion: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
        if let token = self.authModel.token {
            request.addHeader(name: "Authorization", value: "Bearer \(token)")
        }
        
        chain.proceedAsync(request: request, response: response, completion: completion)
    }
}

class AddRefreshTokenInterceptor: ApolloInterceptor {
    var authModel = DI.shared.container.resolve(AuthModel.self)!
    
    func interceptAsync<Operation: GraphQLOperation>(chain: RequestChain, request: HTTPRequest<Operation>, response: HTTPResponse<Operation>?, completion: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
        if let token = self.authModel.refreshToken {
            request.addHeader(name: "Authorization", value: "Bearer \(token)")
        }
        
        chain.proceedAsync(request: request, response: response, completion: completion)
    }
}
