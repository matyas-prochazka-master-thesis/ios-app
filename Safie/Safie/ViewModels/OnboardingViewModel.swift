//
//  OnboardingViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 27.02.2022.
//

import Foundation
import Combine
import UserNotifications

protocol OnboardingViewModelProtocol: ObservableObject {
    associatedtype AppModel: AppModelProtocol
    associatedtype LocationModel: LocationModelProtocol
    
    var appModel: AppModel { get }
    var locationModel: LocationModel { get }
    
    func askForNotificationAuthorization() -> Void
}

final class OnboardingViewModel: OnboardingViewModelProtocol {
    var appModel: AppModel
    var locationModel: LocationModel
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.appModel = DI.shared.container.resolve(AppModel.self)!
        self.locationModel = DI.shared.container.resolve(LocationModel.self)!
        
        self.appModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.locationModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
    }
    
    func askForNotificationAuthorization() -> Void {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound, .criticalAlert]
        
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { _, _ in }
    }
}
