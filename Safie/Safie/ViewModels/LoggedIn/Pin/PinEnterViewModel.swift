//
//  PinEnterViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 26.03.2022.
//

import Foundation
import Combine

protocol PinEnterViewModelProtocol: ObservableObject {
    associatedtype PinModel: PinModelProtocol
    
    var pinModel: PinModel { get }
    
    var pin: String { get set }
    var pinMatched: Bool { get set }
    var pinError: String? { get set }
    
    func checkPin() -> Void
}

final class PinEnterViewModel: PinEnterViewModelProtocol {
    var pinModel: PinModel
    
    @Published var isReady: Bool = false
    
    @Published var pin: String = "" {
        didSet {
            if self.pin.count == 4 {
                self.checkPin()
            } else if self.pin.count > 4 {
                DispatchQueue.main.async { [self] in
                    self.pin = self.pin[0..<4]
                }
            }
        }
    }
    @Published var pinMatched: Bool = false
    @Published var pinError: String?
    
    private var success: () -> Void
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(success: @escaping () -> Void) {
        self.pinModel = DI.shared.container.resolve(PinModel.self)!
        
        self.success = success
        
        self.pinModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
    }
    
    func checkPin() -> Void {
        guard !self.pinMatched else { return }
        
        if self.pinModel.checkPin(pin: self.pin) {
            self.pinMatched = true
            
            self.success()
        } else {
            self.pinMatched = false
            self.pinError = "Wrong PIN"
        }
    }
}
