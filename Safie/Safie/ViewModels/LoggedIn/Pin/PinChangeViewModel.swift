//
//  PinChangeViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import Foundation
import Combine

protocol PinChangeViewModelProtocol: ObservableObject {
    associatedtype PinModel: PinModelProtocol
    
    var pinModel: PinModel { get }
    
    var oldPin: String { get set }
    var oldPinMatched: Bool { get set }
    var oldPinError: String? { get set }
    
    var pin: String { get set }
    var pinError: String? { get set }
    
    var changeSuccess: Bool { get set }
    
    func checkOldPin() -> Void
    func setPin() -> Void
}

final class PinChangeViewModel: PinChangeViewModelProtocol {
    var pinModel: PinModel
    
    @Published var isReady: Bool = false
    
    @Published var oldPin: String = "" {
        didSet {
            if self.oldPin.count == 4 {
                self.checkOldPin()
            } else if self.oldPin.count > 4 {
                DispatchQueue.main.async { [self] in
                    self.oldPin = self.oldPin[0..<4]
                }
            }
        }
    }
    @Published var oldPinMatched: Bool = false
    @Published var oldPinError: String?
    
    @Published var pin: String = "" {
        didSet {
            if self.pin.count > 4 {
                DispatchQueue.main.async { [self] in
                    self.pin = self.pin[0..<4]
                }
            }
        }
    }
    @Published var pinError: String?
    
    @Published var changeSuccess: Bool = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.pinModel = DI.shared.container.resolve(PinModel.self)!
        
        self.pinModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
    }
    
    func checkOldPin() -> Void {
        if self.pinModel.checkPin(pin: self.oldPin) {
            self.oldPinMatched = true
        } else {
            self.oldPinMatched = false
            self.oldPinError = "Wrong current pin"
        }
    }
    
    func setPin() -> Void {
        guard self.oldPinMatched else { return }
        
        guard NSPredicate(format: "SELF MATCHES %@", "[0-9]{4}").evaluate(with: self.pin) else {
            self.pinError = "Pin has to be exactly 4 numbers"
            
            return
        }
                
        if !self.pinModel.savePin(pin: self.pin) {
            self.pinError = "Pin could not be saved"
        } else {
            self.changeSuccess = true
        }
    }
}
