//
//  PinSetViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import Foundation
import Combine

protocol PinSetViewModelProtocol: ObservableObject {
    associatedtype PinModel: PinModelProtocol
    
    var pinModel: PinModel { get }
    
    var pin: String { get set }
    var pinError: String? { get set }
    
    func setPin() -> Void
}

final class PinSetViewModel: PinSetViewModelProtocol {
    var pinModel: PinModel
    
    @Published var isReady: Bool = false
    
    @Published var pin: String = "" {
        didSet {
            if self.pin.count > 4 {
                DispatchQueue.main.async { [self] in
                    self.pin = self.pin[0..<4]
                }
            }
        }
    }
    @Published var pinError: String?
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {        
        self.pinModel = DI.shared.container.resolve(PinModel.self)!
        
        self.pinModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
    }
    
    func setPin() -> Void {        
        guard NSPredicate(format: "SELF MATCHES %@", "[0-9]{4}").evaluate(with: self.pin) else {
            self.pinError = "Pin has to be exactly 4 numbers"
            
            return
        }
                
        if !self.pinModel.savePin(pin: self.pin) {
            self.pinError = "Pin could not be saved"
        }
    }
}
