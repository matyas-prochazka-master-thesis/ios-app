//
//  CheckinRequestDetailViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 05.04.2022.
//

import Foundation
import Combine

protocol CheckinRequestDetailViewModelProtocol: ObservableObject {
    var checkinRequest: CheckinRequest? { get }
    var refreshingRequest: Bool { get }
    
    var checkins: [Checkin] { get }
    var refreshingCheckins: Bool { get }
}

final class CheckinRequestDetailViewModel: CheckinRequestDetailViewModelProtocol {
    private var checkinModel: CheckinModel
    
    private var checkinRequestId: String
    @Published var checkinRequest: CheckinRequest? = nil
    @Published var refreshingRequest: Bool = false
    
    @Published var checkins: [Checkin] = []
    @Published var refreshingCheckins: Bool = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(checkinRequestId: String, checkinRequest: CheckinRequest? = nil) {
        self.checkinModel = DI.shared.container.resolve(CheckinModel.self)!
        
        self.checkinRequestId = checkinRequestId        
        
        
        self.checkinRequest = checkinRequest
        
        self.refreshRequest()
        self.refreshCheckins()
    }
    
    private func refreshRequest() -> Void {
        guard !self.refreshingRequest else { return }
        
        self.refreshingRequest = true
        
        self.checkinModel.getCheckinRequest(checkinRequestId: self.checkinRequestId) { result in
            defer {
                self.refreshingRequest = false
            }
            
            switch result {
            case .success(let checkinRequest):
                self.checkinRequest = checkinRequest
            case .failure(let error):
                switch error {
                case CheckinRequestError.baseError(let message):
                    print(message)
                    
                    break
                }
            }
        }
    }
    
    private func refreshCheckins() -> Void {
        guard !self.refreshingCheckins else { return }
        
        self.refreshingCheckins = true
        
        self.checkinModel.getCheckinsByRequest(checkinRequestId: self.checkinRequestId) { result in
            defer {
                self.refreshingCheckins = false
            }
            
            switch result {
            case .success(let checkins):
                self.checkins = checkins
            case .failure(let error):
                switch error {
                case CheckinsByRequestError.baseError(let message):
                    print(message)
                    
                    break
                }
            }
        }
    }
}
