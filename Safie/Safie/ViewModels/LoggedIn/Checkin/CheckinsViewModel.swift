//
//  CheckinsViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.04.2022.
//

import Foundation
import Combine

protocol CheckinsViewModelProtocol: ObservableObject {
    associatedtype CheckinModel: CheckinModelProtocol
    
    var checkinModel: CheckinModel { get }
    
    var checkingCheckins: [String] { get }
    
    func checkin(checkin: Checkin) -> Void
}

final class CheckinsViewModel: CheckinsViewModelProtocol {
    var checkinModel: CheckinModel
    
    @Published var checkingCheckins: [String] = []
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.checkinModel = DI.shared.container.resolve(CheckinModel.self)!
        
        self.checkinModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.checkinModel.refreshUncheckedCheckinsForMe()
        self.checkinModel.refreshRequestsForMe()
        self.checkinModel.refreshRequestsIFollow()
    }
    
    func checkin(checkin: Checkin) -> Void {
        guard !self.checkingCheckins.contains(checkin.id) else { return }
        
        self.checkingCheckins.append(checkin.id)
        
        self.checkinModel.checkin(checkinId: checkin.id) { _ in
            self.checkingCheckins = self.checkingCheckins.filter { $0 != checkin.id }
            
            self.checkinModel.refreshUncheckedCheckinsForMe()
        }
    }
}
