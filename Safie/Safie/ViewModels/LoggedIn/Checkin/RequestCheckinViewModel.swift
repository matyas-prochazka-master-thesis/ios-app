//
//  RequestCheckinViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 02.04.2022.
//

import Foundation
import Combine

protocol RequestCheckinViewModelProtocol: ObservableObject {
    associatedtype GuardianModel: GuardianModelProtocol
    
    var guardianModel: GuardianModel { get }
    
    var checkinRequestType: CheckinRequestType { get set }
    
    var selectedGuardian: Guardian? { get }
    var requesting: Bool { get }
    var requestSuccess: Bool { get }
    
    var startsAt: Date { get set }
    var endsAt: Date { get set }
    var periodInMinutes: Int { get set }
    
    var periodicalRequestError: String? { get }
    
    func toggleGuardian(guardian: Guardian) -> Void
    func requestCheckin() -> Void
}

final class RequestCheckinViewModel: RequestCheckinViewModelProtocol {
    private var checkinModel: CheckinModel
    
    var guardianModel: GuardianModel
    
    @Published var checkinRequestType: CheckinRequestType = .oneTime
    
    @Published var selectedGuardian: Guardian? = nil
    @Published var requesting: Bool = false
    @Published var requestSuccess: Bool = false
    
    @Published var startsAt: Date = Date() {
        didSet {
            if self.startsAt > self.endsAt {
                self.endsAt = self.startsAt
            }
        }
    }
    @Published var endsAt: Date = Date() {
        didSet {
            if self.endsAt < self.startsAt {
                self.startsAt = self.endsAt
            }
        }
    }
    @Published var periodInMinutes: Int = 30
    
    @Published var periodicalRequestError: String? = nil
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.checkinModel = DI.shared.container.resolve(CheckinModel.self)!
        self.guardianModel = DI.shared.container.resolve(GuardianModel.self)!
        
        self.guardianModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.guardianModel.refreshGuardians()
    }
    
    func toggleGuardian(guardian: Guardian) -> Void {
        if self.selectedGuardian == guardian {
            self.selectedGuardian = nil
        } else {
            self.selectedGuardian = guardian
        }
    }
    
    func requestCheckin() -> Void {
        guard let targetAccount = self.selectedGuardian?.guardian else { return }
        
        guard !self.requesting else { return }
        
        self.requesting = true
        
        if self.checkinRequestType == .oneTime {
            self.checkinModel.requestCheckin(targetAccountId: targetAccount.id) { result in
                defer {
                    self.requesting = false
                }
                
                self.requestSuccess = true
            }
        }
        
        if self.checkinRequestType == .periodical {
            guard self.startsAt < self.endsAt else {
                self.requesting = false
                            
                self.periodicalRequestError = "Wrong dates range"
                
                return
            }
            
            guard self.endsAt.timeIntervalSince(self.startsAt) <= 12 * 60 * 60 else {
                self.periodicalRequestError = "Time interval can be 12 hours max"
                
                return
            }
            
            self.checkinModel.requestPeriodicalCheckin(
                targetAccountId: targetAccount.id,
                startsAt: self.startsAt,
                endsAt: self.endsAt,
                periodInMinutes: self.periodInMinutes,
                guardianAccountIds: []
            ) { result in
                defer {
                    self.requesting = false
                }
                
                self.requestSuccess = true
            }
        }
    }
}
