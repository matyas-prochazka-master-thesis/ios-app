//
//  LoggedInViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 19.08.2021.
//

import Foundation
import Combine

protocol LoggedInViewModelProtocol: ObservableObject {
    associatedtype AccountModel: AccountModelProtocol
    associatedtype PinModel: PinModelProtocol
    associatedtype LocationModel: LocationModelProtocol
    associatedtype NotificationModel: NotificationModelProtocol
    associatedtype CheckinModel: CheckinModelProtocol
    associatedtype NotificationOpenModel: NotificationOpenModelProtocol
    associatedtype ActiveTripModel: ActiveTripModelProtocol
    
    var accountModel: AccountModel { get }
    var pinModel: PinModel { get }
    var locationModel: LocationModel { get }
    var notificationModel: NotificationModel { get }
    var checkinModel: CheckinModel { get }
    var notificationOpenModel: NotificationOpenModel { get }
    var activeTripModel: ActiveTripModel { get }
    
    var isReady: Bool { get }
    
    var soses: [Sos] { get set }
    var sosesRefreshing: Bool { get set }
    
    func refresh() -> Void
}

final class LoggedInViewModel: LoggedInViewModelProtocol {
    var accountModel: AccountModel
    var pinModel: PinModel
    var locationModel: LocationModel
    var notificationModel: NotificationModel
    var checkinModel: CheckinModel
    var notificationOpenModel: NotificationOpenModel
    var activeTripModel: ActiveTripModel
    
    private var sosModel: SosModel
    
    @Published var isReady: Bool = false
    
    @Published var soses: [Sos] = []
    @Published var sosesRefreshing: Bool = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.accountModel = DI.shared.container.resolve(AccountModel.self)!
        self.pinModel = DI.shared.container.resolve(PinModel.self)!
        self.locationModel = DI.shared.container.resolve(LocationModel.self)!
        self.notificationModel = DI.shared.container.resolve(NotificationModel.self)!
        self.checkinModel = DI.shared.container.resolve(CheckinModel.self)!
        self.notificationOpenModel = DI.shared.container.resolve(NotificationOpenModel.self)!
        self.activeTripModel = DI.shared.container.resolve(ActiveTripModel.self)!
        self.sosModel = DI.shared.container.resolve(SosModel.self)!
        
        self.accountModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.pinModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.locationModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.notificationModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.checkinModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.notificationOpenModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.activeTripModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.notificationModel.sendDeviceTokenToServer()
        self.refresh()
        
        self.isReady = true
        
        let timer = Timer(timeInterval: 20.0, repeats: true) { _ in
            self.refresh()
        }
        
        RunLoop.current.add(timer, forMode: .common)
    }
    
    func refresh() -> Void {
        self.refreshSoses()
        self.checkinModel.refreshUncheckedCheckinsForMe()
    }
    
    private func refreshSoses() -> Void {
        guard !self.sosesRefreshing else { return }
        
        self.sosesRefreshing = true
        
        self.sosModel.getSosesOfAccountsIAmGuardianOf { result in
            defer {
                self.sosesRefreshing = false                
            }
            
            switch result {
            case .success(let soses):
                self.soses = soses
            case .failure(let error):
                switch error {
                case SosesOfAccountsIAmGuardianOfError.baseError(let message):
                    print(message)
                    
                    break
                }
            }
        }
    }
}
