//
//  ProfilePictureUpdateViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import Foundation
import Combine
import UIKit

protocol ProfilePictureUpdateViewModelProtocol: ObservableObject {
    var updateSuccess: Bool { get }
    var globalError: String? { get set }
    
    var profilePicture: UIImage? { get set }
    
    func updateProfilePicture() -> Void
}

final class ProfilePictureUpdateViewModel: ProfilePictureUpdateViewModelProtocol {
    @Published var updateSuccess: Bool = false
    @Published var globalError: String?
        
    var profilePicture: UIImage?
    
    private var accountModel: AccountModel
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.accountModel = DI.shared.container.resolve(AccountModel.self)!
    }
    
    func updateProfilePicture() -> Void {
        self.globalError = nil
        
        if let profilePicture = self.profilePicture,
           let tmpProfilePictureBase64 = profilePicture.jpegData(compressionQuality: 1)?.base64EncodedString()
        {
            let profilePictureBase64 = "data:image/jpeg;base64,\(tmpProfilePictureBase64)"
            
            self.accountModel.updateProfilePicture(profilePictureBase64: profilePictureBase64) { result in
                switch result {
                case .failure(let error):
                    switch error {
                    case UpdateProfilePictureError.baseError(let message):
                        self.globalError = message
                    }
                case .success(_):
                    self.updateSuccess = true
                    
                    break
                }
            }
        }
    }
}
