//
//  ProfileViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import Foundation
import Combine
import UIKit

protocol MyProfileViewModelProtocol: ObservableObject {
    associatedtype AccountModel: AccountModelProtocol
    associatedtype GuardianModel: GuardianModelProtocol
    
    var accountModel: AccountModel { get }
    var guardianModel: GuardianModel { get }
    
    var profilePictureUpdating: Bool { get }
    var profilePictureUpdateSuccess: Bool { get }
    var profilePictureError: String? { get set }
    var profilePicture: UIImage? { get set }
    
    func updateProfilePicture() -> Void
    func logOut() -> Void
}

final class MyProfileViewModel: MyProfileViewModelProtocol {
    private var authModel: AuthModel
    var accountModel: AccountModel
    var guardianModel: GuardianModel
    
    @Published var profilePictureUpdating: Bool = false
    @Published var profilePictureUpdateSuccess: Bool = false
    @Published var profilePictureError: String?
    var profilePicture: UIImage?
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.authModel = DI.shared.container.resolve(AuthModel.self)!
        self.accountModel = DI.shared.container.resolve(AccountModel.self)!
        self.guardianModel = DI.shared.container.resolve(GuardianModel.self)!
        
        self.accountModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.guardianModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
                
        DispatchQueue.main.async {
            self.accountModel.refresh()
            self.guardianModel.refreshGuardians()
            self.guardianModel.refreshGuardianRequests()
        }
    }
    
    func updateProfilePicture() -> Void {
        self.profilePictureError = nil
        
        if let profilePicture = self.profilePicture,
           let tmpProfilePictureBase64 = profilePicture.jpegData(compressionQuality: 1)?.base64EncodedString()
        {
            let profilePictureBase64 = "data:image/jpeg;base64,\(tmpProfilePictureBase64)"
            
            self.profilePictureUpdating = true
            
            self.accountModel.updateProfilePicture(profilePictureBase64: profilePictureBase64) { result in
                defer {
                    self.profilePictureUpdating = false
                }
                
                switch result {
                case .failure(let error):
                    switch error {
                    case UpdateProfilePictureError.baseError(let message):
                        self.profilePictureError = message
                    }
                case .success(_):
                    self.profilePictureUpdateSuccess = true
                    
                    break
                }
            }
        }
    }
    
    func logOut() -> Void {
        self.authModel.logOut()
    }
}
