//
//  UsernameUpdateViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import Foundation
import Combine

protocol UsernameUpdateViewModelProtocol: ObservableObject {
    var updateSuccess: Bool { get }
    var globalError: String? { get set }
    
    var username: StringInput { get set }
    
    func updateUsername() -> Void
}

final class UsernameUpdateViewModel: UsernameUpdateViewModelProtocol {
    @Published var updateSuccess: Bool = false
    @Published var globalError: String?
        
    var username: StringInput = StringInput().required("Povinné")
    
    private var accountModel: AccountModel
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.accountModel = DI.shared.container.resolve(AccountModel.self)!
        
        self.username.subscribe(notify: { self.objectWillChange.send() })
    }
    
    func updateUsername() -> Void {
        self.globalError = nil
        
        self.username.validate()
        
        if self.username.valid {
            self.accountModel.updateUsername(username: self.username.value) { result in
                switch result {
                case .failure(let error):
                    switch error {
                    case UpdateUsernameError.baseError(let message):
                        self.globalError = message
                    }
                case .success(_):
                    self.updateSuccess = true
                    
                    break
                }
            }
        }
    }
}
