//
//  SosHistoryViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import Foundation
import Combine

protocol SosHistoryViewModelProtocol: ObservableObject {
    var soses: [Sos] { get set }
    var refreshing: Bool { get set }
}

final class SosHistoryViewModel: SosHistoryViewModelProtocol {
    private var sosModel: SosModel
    
    @Published var soses: [Sos] = []
    @Published var refreshing: Bool = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.sosModel = DI.shared.container.resolve(SosModel.self)!
        
        self.refresh()
    }
    
    private func refresh() -> Void {
        guard !self.refreshing else { return }
        
        self.refreshing = true
        
        self.sosModel.getMySosHistory { result in
            defer {
                self.refreshing = false
            }
            
            switch result {
            case .success(let soses):
                self.soses = soses
            case .failure(let error):
                switch error {
                case MySosHistoryError.baseError(let message):
                    print(message)
                    
                    break
                }
            }
        }
    }
}
