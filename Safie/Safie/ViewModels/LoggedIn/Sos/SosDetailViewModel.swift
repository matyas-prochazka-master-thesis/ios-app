//
//  SosDetailViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import Foundation
import Combine
import MapKit
import SwiftUIPager

protocol SosDetailViewModelProtocol: ObservableObject {
    var sos: Sos? { get set }
    var refreshing: Bool { get set }
    
    var sosPositions: [SosPosition] { get set }
    var refreshingPositions: Bool { get set }
    
    var region: MKCoordinateRegion { get set }
    var regionReady: Bool { get set }
    
    var shownPositionId: String? { get set }
    var page: Page { get set }
    
    func viewIndex(_ index: Int) -> Void
}

final class SosDetailViewModel: SosDetailViewModelProtocol {
    private var sosModel: SosModel
    
    @Published var sos: Sos?
    @Published var refreshing: Bool = false
    
    @Published var sosPositions: [SosPosition] = []
    @Published var refreshingPositions: Bool = false
    
    @Published var region: MKCoordinateRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 50.0755, longitude: 14.4378),
        span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
    )
    @Published var regionReady: Bool = false
    
    @Published var shownPositionId: String?
    @Published var page: Page = .first()
    
    private var sosId: String
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(sosId: String, sos: Sos? = nil) {
        self.sosId = sosId
        
        self.sosModel = DI.shared.container.resolve(SosModel.self)!
        
        if sos != nil {
            self.sos = sos
        } else {
            self.refresh()
        }
        
        self.refreshPositions()
            
        let timer = Timer(timeInterval: 30.0, repeats: true) { _ in
            self.refreshPositions()
        }
        
        RunLoop.current.add(timer, forMode: .common)
    }
    
    func viewIndex(_ index: Int) -> Void {
        guard index < self.sosPositions.count else { return }
        
        let selectedSosPosition = self.sosPositions[index]        
        
        self.shownPositionId = selectedSosPosition.id
    }
    
    private func refresh() -> Void {
        guard !self.refreshing else { return }
        
        //self.refreshing = true
    }
    
    private func refreshPositions() -> Void {
        guard !self.refreshingPositions else { return }
        
        self.refreshingPositions = true
        
        self.sosModel.getSosPositions(sosId: self.sosId) { result in
            defer {
                self.refreshingPositions = false
            }
            
            switch result {
            case .success(let sosPositions):
                let firstPositions = self.sosPositions.isEmpty
                
                self.sosPositions = sosPositions
                
                if firstPositions {
                    self.updateRegion()
                }
            case .failure(let error):
                switch error {
                case SosPositionsError.baseError(let message):
                    print(message)
                    
                    break
                }
            }
        }
    }
    
    private func updateRegion() -> Void {
        defer { self.regionReady = true }
        
        guard !self.sosPositions.isEmpty else { return }
        
        guard
            let minLatitude = self.sosPositions.min(by: { $0.latitude < $1.latitude })?.latitude,
            let maxLatitude = self.sosPositions.max(by: { $0.latitude > $1.latitude })?.latitude,
            let minLongitude = self.sosPositions.min(by: { $0.longitude < $1.longitude })?.longitude,
            let maxLongitude = self.sosPositions.max(by: { $0.longitude > $1.longitude })?.longitude
        else { return }
        
        let latitudeCenter = (minLatitude + maxLatitude) / 2
        let longitudeCenter = (minLongitude + maxLongitude) / 2
        
        let latitudeDelta = maxLatitude - minLatitude
        let longitudeDelta = maxLongitude - minLongitude
        
        self.region = MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: latitudeCenter, longitude: longitudeCenter),
            span: MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
        )
        
        self.viewIndex(0)
    }
}
