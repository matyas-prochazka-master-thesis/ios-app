//
//  SosButtonViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 26.03.2022.
//

import Foundation
import Combine

protocol SosButtonViewModelProtocol: ObservableObject {
    func buttonPressed() -> Void
    func buttonReleased() -> Void
    func pinEntered() -> Void
    func clean() -> Void
}

final class SosButtonViewModel: SosButtonViewModelProtocol {
    private var sosModel: SosModel
    
    private var idFromClient: String
    private var timer: Timer?
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.sosModel = DI.shared.container.resolve(SosModel.self)!
        
        self.idFromClient = UUID().uuidString
    }
    
    func buttonPressed() -> Void {
        self.sosModel.buttonPressed(idFromClient: self.idFromClient) { result in            
            let timer = Timer(timeInterval: 5.0, repeats: true) { _ in
                self.positionUpdate()
            }
            
            self.timer = timer
            
            RunLoop.current.add(timer, forMode: .common)
        }
    }
    
    func buttonReleased() -> Void {
        self.sosModel.buttonReleased(idFromClient: self.idFromClient) { result in
            
        }
    }
    
    func pinEntered() -> Void {
        self.sosModel.pinEntered(idFromClient: self.idFromClient) { result in
            
        }
    }
    
    func clean() -> Void {
        self.regenerateIdFromClient()
        self.positionUpdating()
    }
    
    private func regenerateIdFromClient() -> Void {
        self.idFromClient = UUID().uuidString
    }
    
    private func positionUpdating() -> Void {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    private func positionUpdate() -> Void {
        self.sosModel.positionUpdate(idFromClient: self.idFromClient) { result in
            
        }
    }
}
