//
//  TripViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 07.04.2022.
//

import Foundation
import Combine

protocol TripViewModelProtocol: ObservableObject {
    associatedtype ActiveTripModel: ActiveTripModelProtocol
    
    var activeTripModel: ActiveTripModel { get }
}

final class TripViewModel: TripViewModelProtocol {
    private var tripModel: TripModel
    
    var activeTripModel: ActiveTripModel
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.tripModel = DI.shared.container.resolve(TripModel.self)!
        self.activeTripModel = DI.shared.container.resolve(ActiveTripModel.self)!
        
        self.activeTripModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
    }
}
