//
//  MyGuardiansTripHistoryViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 10.04.2022.
//

import Foundation
import Combine

protocol MyGuardiansTripHistoryViewModelProtocol: ObservableObject {
    var trips: [Trip] { get }
}

final class MyGuardiansTripHistoryViewModel: MyGuardiansTripHistoryViewModelProtocol {
    private var tripModel: TripModel
    
    @Published var trips: [Trip] = []
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.tripModel = DI.shared.container.resolve(TripModel.self)!
        
        self.refresh()
    }
    
    private func refresh() -> Void {
        self.tripModel.getMyGuardiansTrips { result in
            switch result {
            case .success(let trips):
                self.trips = trips
            case .failure(_):
                break
            }
        }
        
    }
}
