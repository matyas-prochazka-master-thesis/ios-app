//
//  NewDestinationViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 08.04.2022.
//

import SwiftUI
import Combine
import MapKit

struct Marker: Identifiable {
    var id = UUID()
    var coordinates: CLLocationCoordinate2D
}

protocol NewDestinationViewModelProtocol: ObservableObject {
    var region: MKCoordinateRegion { get set }
    var selectingFromMap: Bool { get set }
    
    var searchResults: [SearchLocation] { get set }
    var searching: Bool { get set }
    
    var locationToConfirm: SearchLocation? { get set }
    var markers: [Marker] { get set }
    
    var onSelect: (_ location: SearchLocation) -> Void { get }
        
    func selectLocation(_ location: SearchLocation) -> Void
    func startSelectingFromMap() -> Void
    func selectFromMap(_ title: String) -> Void
    func search(search: String) -> Void
    func selectLocationFromAutocomplete(_ location: SearchLocation) -> Void
    func confirmSelectionFromMap() -> Void
}

final class NewDestinationViewModel: NSObject, MKLocalSearchCompleterDelegate, NewDestinationViewModelProtocol {
    private var tripModel: TripModel
    
    @Published var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 50.0775, longitude: 14.4378),
        span: MKCoordinateSpan(latitudeDelta: 1.5, longitudeDelta: 1.5)
    )
    @Published var selectingFromMap = true
    
    private var searchCompleter = MKLocalSearchCompleter()
    private var lastSearchRequest: MKLocalSearch? = nil
    
    @Published var searchResults = [SearchLocation]()
    @Published var searching: Bool = false
    
    @Published var locationToConfirm: SearchLocation? = nil
    @Published var markers = [Marker]()
    
    var onSelect: (_ location: SearchLocation) -> Void
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(onSelect: @escaping (_ location: SearchLocation) -> Void) {
        self.tripModel = DI.shared.container.resolve(TripModel.self)!
                
        self.onSelect = onSelect
        
        super.init()
        
        self.searchCompleter.delegate = self
    }
    
    func selectLocation(_ location: SearchLocation) {
        self.onSelect(location)
    }
    
    func startSelectingFromMap() {
        self.markers = []
        self.locationToConfirm = nil
        self.searching = false
        self.selectingFromMap = true
    }
    
    func selectFromMap(_ title: String) {
        let latitude = self.region.center.latitude
        let longitude = self.region.center.longitude
        
        let location = SearchLocation(
            id: UUID().uuidString,
            title: title,
            subtitle: "Lokalita z mapy",
            latitude: latitude,
            longitude: longitude
        )
        
        self.selectLocation(location)
    }
    
    func search(search: String) {
        if search.count > 2 {
            self.selectingFromMap = false
            self.searching = true
            
            self.searchCompleter.cancel()
            self.searchCompleter.queryFragment = search
        }
    }
    
    func selectLocationFromAutocomplete(_ location: SearchLocation) {
        // The search results do not have coordinate information
        // So we need to make another request to get them from the location name
        let searchRequest = MKLocalSearch.Request()
        
        searchRequest.naturalLanguageQuery = "\(location.title) \(location.subtitle ?? "")"
        searchRequest.resultTypes = [.address, .pointOfInterest]
        
        let lastSearchRequest = MKLocalSearch(request: searchRequest)
        
        lastSearchRequest.start { (response, error) in
            guard let response = response else {
                return
            }
            
            if response.mapItems.count > 0 {
                let item = response.mapItems[0]
                
                if let coordinates = item.placemark.location?.coordinate {
                    let selectedLocation = SearchLocation(
                        id: UUID().uuidString,
                        title: location.title,
                        subtitle: location.subtitle,
                        latitude: coordinates.latitude,
                        longitude: coordinates.longitude
                    )
                     
                    self.locationToConfirm = selectedLocation
                    self.region = MKCoordinateRegion(
                        center: coordinates,
                        span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                    )
                    self.markers = [Marker(coordinates: coordinates)]
                    self.searching = false
                }
            }
        }
    }
    
    func confirmSelectionFromMap() {
        if let location = self.locationToConfirm {
            self.selectLocation(location)
            
            self.startSelectingFromMap()
        }
    }
    
    
    // Delegate function for returning search results
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        var searchResults: [SearchLocation] = [];
        
        var count = 0
        
        for result in completer.results {
            if count >= 6 {
                continue
            }
                        
            count += 1
                
            searchResults.append(
                SearchLocation(
                    id: UUID().uuidString,
                    title: result.title,
                    subtitle: result.subtitle
                )
            )
        }
        
        self.searchResults = searchResults
    }
    
    private func completer(completer: MKLocalSearchCompleter, didFailWithError error: NSError) {
    }
}
