//
//  PlanTripViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import Foundation
import Combine

protocol PlanTripViewModelProtocol: ObservableObject {
    associatedtype ActiveTripModel: ActiveTripModelProtocol
    
    var activeTripModel: ActiveTripModel { get }
        
    var withShouldArriveAt: Bool { get set }
    var shouldArriveAt: Date { get set }
    
    var withDestination: Bool { get set }
    var selectedDestination: TripDestination? { get set }
    var selectedCreateDestination: TripDestinationCreateDto? { get set }
    
    var routePlan: [QuickAction] { get }
    
    func startTrip() -> Void
}

final class PlanTripViewModel: PlanTripViewModelProtocol {
    var activeTripModel: ActiveTripModel
        
    @Published var withShouldArriveAt: Bool = false
    @Published var shouldArriveAt: Date = Date()
    
    @Published var withDestination: Bool = false
    @Published var selectedDestination: TripDestination? = nil
    @Published var selectedCreateDestination: TripDestinationCreateDto? = nil
    
    @Published var routePlan: [QuickAction] = []
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {        
        self.activeTripModel = DI.shared.container.resolve(ActiveTripModel.self)!
        
        self.activeTripModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
    }
    
    func startTrip() -> Void {
        self.activeTripModel.startTrip(
            shouldArriveAt: self.withShouldArriveAt ? self.shouldArriveAt : nil,
            tripDestinationId: self.withDestination ? self.selectedDestination?.id : nil,
            tripDestinationCreateDto: self.withDestination ? self.selectedCreateDestination : nil,
            quickActions: self.routePlan
        )
    }
}
