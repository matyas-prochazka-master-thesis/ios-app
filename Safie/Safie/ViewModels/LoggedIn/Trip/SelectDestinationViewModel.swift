//
//  SelectDestinationViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 08.04.2022.
//

import Foundation
import Combine

protocol SelectDestinationViewModelProtocol: ObservableObject {
    var favouriteDestinations: [TripDestination] { get }
    var historyDestinations: [TripDestination] { get }
    var refreshingDestinations: Bool { get }
        
    func favouriteDestination(destination: TripDestination) -> Void
}

final class SelectDestinationViewModel: SelectDestinationViewModelProtocol {
    private var tripModel: TripModel
    
    @Published var favouriteDestinations: [TripDestination] = []
    @Published var historyDestinations: [TripDestination] = []
    @Published var refreshingDestinations: Bool = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.tripModel = DI.shared.container.resolve(TripModel.self)!
        
        self.refreshDestinations()
    }
    
    func favouriteDestination(destination: TripDestination) -> Void {
        // Local favourite
        self.historyDestinations = self.historyDestinations.map { historyDestination in
            if historyDestination == destination {
                var mutableHistoryDestination = historyDestination
                
                mutableHistoryDestination.favourite = !destination.favourite
                
                return mutableHistoryDestination
            }
            
            return historyDestination
        }
        
        
        // Server request
        self.tripModel.toggleFavouriteTripDestination(tripDestinationId: destination.id) { _ in
            self.refreshDestinations()
        }
    }
    
    private func refreshDestinations() -> Void {
        guard !self.refreshingDestinations else { return }
        
        self.refreshingDestinations = true
        
        self.tripModel.getMyTripDestinations { result in
            defer {
                self.refreshingDestinations = false
            }
            
            switch result {
            case .success(let destinations):
                self.historyDestinations = destinations
                self.favouriteDestinations = destinations.filter { $0.favourite }
            case .failure(let error):
                print(error)
            }
        }
    }
}
