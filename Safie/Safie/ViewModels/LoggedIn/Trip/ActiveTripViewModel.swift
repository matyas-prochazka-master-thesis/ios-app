//
//  ActiveTripViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import Foundation
import Combine
import UIKit

protocol ActiveTripViewModelProtocol: ObservableObject {
    associatedtype ActiveTripModel: ActiveTripModelProtocol
    
    var activeTripModel: ActiveTripModel { get }
    
    var message: StringInput { get set }
    var sendingMessage: Bool { get }
    
    var photo: UIImage? { get }
    var sendingPhoto: Bool { get }
    
    func sendMessage() -> Void
}

final class ActiveTripViewModel: ActiveTripViewModelProtocol {
    private var tripModel: TripModel
    
    var activeTripModel: ActiveTripModel
    
    var message: StringInput = StringInput().required("Povinné")
    @Published var sendingMessage: Bool = false
    
    @Published var photo: UIImage? = nil
    @Published var sendingPhoto: Bool = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.tripModel = DI.shared.container.resolve(TripModel.self)!
        self.activeTripModel = DI.shared.container.resolve(ActiveTripModel.self)!
        
        self.activeTripModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.message.subscribe(notify: { self.objectWillChange.send() })
    }
    
    func sendMessage() -> Void {
        self.message.validate()
        
        guard self.message.valid else { return }
        
        guard !self.sendingMessage else { return }
        
        self.sendingMessage = true
        
        self.activeTripModel.addMessage(message: self.message.value) { result in
            defer {
                self.sendingMessage = false
            }
            
            switch result {
            case .success(_):
                self.message.value = ""
            case .failure(_):
                break
            }
        }
    }
    
    func sendPhoto() -> Void {
        guard
            let photo = self.photo,
            let tmpPhotoBase64 = photo.jpegData(compressionQuality: 1)?.base64EncodedString()
        else {
            return
        }
        
        let photoBase64 = "data:image/jpeg;base64,\(tmpPhotoBase64)"
        
        guard !self.sendingPhoto else { return }
        
        self.sendingPhoto = true
        
        self.activeTripModel.addPhoto(photo: photoBase64) { result in
            defer {
                self.sendingPhoto = false
            }
            
            switch result {
            case .success(_):
                self.photo = nil
            case .failure(_):
                break
            }
        }
    }
}
