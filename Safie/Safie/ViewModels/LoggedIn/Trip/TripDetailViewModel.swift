//
//  TripDetailViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 09.04.2022.
//

import Foundation
import Combine
import MapKit
import SwiftUIPager

protocol TripDetailViewModelProtocol: ObservableObject {
    var trip: Trip? { get }
    var activities: [TripActivity] { get }
    
    var region: MKCoordinateRegion { get set }
    var regionReady: Bool { get set }
    
    var shownActivityId: String? { get set }
    var page: Page { get set }
    
    func viewIndex(_ index: Int) -> Void
}

final class TripDetailViewModel: TripDetailViewModelProtocol {
    private var tripModel: TripModel
    
    private var tripId: String
    
    @Published var trip: Trip?
    @Published var activities: [TripActivity] = []
    
    @Published var region: MKCoordinateRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 50.0755, longitude: 14.4378),
        span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
    )
    @Published var regionReady: Bool = false
    
    @Published var shownActivityId: String?
    @Published var page: Page = .first()
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(tripId: String) {
        self.tripModel = DI.shared.container.resolve(TripModel.self)!
        
        self.tripId = tripId
        
        self.refreshTrip()
        self.refreshActivites()
            
        let timer = Timer(timeInterval: 30.0, repeats: true) { _ in
            self.refreshActivites()
        }
        
        RunLoop.current.add(timer, forMode: .common)
    }
    
    func viewIndex(_ index: Int) -> Void {
        guard index < self.activities.count else { return }
        
        let selectedActivity = self.activities[index]
        
        self.shownActivityId = selectedActivity.id
    }
    
    private func refreshTrip() -> Void {
        self.tripModel.getTrip(tripId: self.tripId) { result in
            switch result {
            case .success(let trip):
                self.trip = trip
            case .failure(_):
                break
            }
        }
    }
    
    private func refreshActivites() -> Void {
        self.tripModel.getActivitiesByTrip(tripId: self.tripId) { result in
            switch result {
            case .success(let activities):
                let firstActivities = self.activities.isEmpty
                
                self.activities = activities
                
                if firstActivities {
                    self.updateRegion()
                }
            case .failure(_):
                break
            }
        }
    }
    
    // Update the region so that all the activities are shown on the map
    private func updateRegion() -> Void {
        defer { self.regionReady = true }
        
        guard !self.activities.isEmpty else { return }
        
        let filteredActivities = self.activities.filter { $0.coordinates != nil }
        
        guard
            let minLatitude = filteredActivities.min(by: { $0.latitude! < $1.latitude! })?.latitude,
            let maxLatitude = filteredActivities.max(by: { $0.latitude! > $1.latitude! })?.latitude,
            let minLongitude = filteredActivities.min(by: { $0.longitude! < $1.longitude! })?.longitude,
            let maxLongitude = filteredActivities.max(by: { $0.longitude! > $1.longitude! })?.longitude
        else { return }
        
        let latitudeCenter = (minLatitude + maxLatitude) / 2
        let longitudeCenter = (minLongitude + maxLongitude) / 2
        
        let latitudeDelta = maxLatitude - minLatitude
        let longitudeDelta = maxLongitude - minLongitude
        
        self.region = MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: latitudeCenter, longitude: longitudeCenter),
            span: MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
        )
        
        self.viewIndex(0)
    }
}
