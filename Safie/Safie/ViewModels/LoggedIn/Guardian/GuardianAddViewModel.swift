//
//  GuardianAddViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import Foundation
import Combine

protocol GuardianAddViewModelProtocol: ObservableObject {
    var addSuccess: Bool { get }
    var globalError: String? { get set }
    
    var username: StringInput { get set }
    
    func addGuardian() -> Void
}

final class GuardianAddViewModel: GuardianAddViewModelProtocol {
    @Published var addSuccess: Bool = false
    @Published var globalError: String?
        
    var username: StringInput = StringInput().required("Povinné")
    
    private var guardianModel: GuardianModel
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.guardianModel = DI.shared.container.resolve(GuardianModel.self)!
        
        self.username.subscribe(notify: { self.objectWillChange.send() })
    }
    
    func addGuardian() -> Void {
        self.globalError = nil
        
        self.username.validate()
        
        if self.username.valid {
            self.guardianModel.addGuardian(username: self.username.value) { result in
                switch result {
                case .failure(let error):
                    switch error {
                    case AddGuardianError.baseError(let message):
                        self.globalError = message
                    }
                case .success(_):
                    self.addSuccess = true
                    
                    break
                }
            }
        }
    }
}
