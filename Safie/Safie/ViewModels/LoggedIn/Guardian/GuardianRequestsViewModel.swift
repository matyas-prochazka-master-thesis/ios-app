//
//  GuardianRequestsViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 01.04.2022.
//

import Foundation
import Combine

protocol GuardianRequestsViewModelProtocol: ObservableObject {
    associatedtype GuardianModel: GuardianModelProtocol
    
    var guardianModel: GuardianModel { get }
    
    var acceptingGuardians: [String] { get }
    
    func acceptGuardian(guardian: Guardian) -> Void
}

final class GuardianRequestsViewModel: GuardianRequestsViewModelProtocol {
    var guardianModel: GuardianModel
    
    @Published var acceptingGuardians: [String] = []
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.guardianModel = DI.shared.container.resolve(GuardianModel.self)!
        
        self.guardianModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.guardianModel.refreshGuardianRequests()
    }
    
    func acceptGuardian(guardian: Guardian) -> Void {
        guard !self.acceptingGuardians.contains(guardian.id) else { return }
        
        self.acceptingGuardians.append(guardian.id)
        
        self.guardianModel.acceptGuardian(guardianId: guardian.id) { _ in
            self.acceptingGuardians = self.acceptingGuardians.filter { $0 != guardian.id }
        }
    }
}
