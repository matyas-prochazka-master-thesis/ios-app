//
//  GuardianViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.03.2022.
//

import Foundation
import Combine

protocol GuardianViewModelProtocol: ObservableObject {
    var guardian: Guardian? { get set }
}

final class GuardianViewModel: GuardianViewModelProtocol {
    private var guardianModel: GuardianModel
    
    private var guardianId: String
    
    @Published var guardian: Guardian?
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(guardianId: String, guardian: Guardian? = nil) {
        self.guardianModel = DI.shared.container.resolve(GuardianModel.self)!
        self.guardianId = guardianId
        
        if guardian != nil {
            self.guardian = guardian
        } else {
            self.refresh()
        }
    }
    
    private func refresh() -> Void {
        
    }
}
