//
//  GuardiansViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 22.03.2022.
//

import Foundation
import Combine

protocol GuardiansViewModelProtocol: ObservableObject {
    associatedtype GuardianModel: GuardianModelProtocol
    
    var guardianModel: GuardianModel { get }
    
    func removeGuardians(at offsets: IndexSet) -> Void
}

final class GuardiansViewModel: GuardiansViewModelProtocol {
    var guardianModel: GuardianModel
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.guardianModel = DI.shared.container.resolve(GuardianModel.self)!
        
        self.guardianModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.guardianModel.refreshGuardians()
        self.guardianModel.refreshGuardianRequests()
    }
    
    func removeGuardians(at offsets: IndexSet) -> Void {
        let startIndex: Int = offsets[offsets.startIndex]
        let endIndex: Int = offsets[offsets.endIndex]
    
        let guardiansToRemove = self.guardianModel.guardians[startIndex..<endIndex]
        
        self.guardianModel.guardians.remove(atOffsets: offsets)
        
        guardiansToRemove.forEach { guardian in
            self.guardianModel.removeGuardian(guardianId: guardian.id) { _ in
                
            }
        }
    }
}
