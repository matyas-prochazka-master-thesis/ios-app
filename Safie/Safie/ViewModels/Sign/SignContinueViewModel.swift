//
//  SignContinueViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 18.11.2021.
//

import Foundation
import Combine
import AuthenticationServices

protocol SignContinueViewModelProtocol: ObservableObject {
    var globalError: String? { get set }
    var signing: Bool { get }

    func signInAppleConfigure(_ request: ASAuthorizationAppleIDRequest) -> Void
    func signInAppleHandle(_ authResult: Result<ASAuthorization, Error>) -> Void
    
    func signInFacebook() -> Void
    func signInGoogle() -> Void
}

final class SignContinueViewModel: SignContinueViewModelProtocol {
    @Published var globalError: String?
    @Published var signing: Bool = false
    
    private var authModel: AuthModel
    
    init() {
        self.authModel = DI.shared.container.resolve(AuthModel.self)!
    }
    
    func signInAppleConfigure(_ request: ASAuthorizationAppleIDRequest) {
        request.requestedScopes = [.email, .fullName]
    }
    
    func signInAppleHandle(_ authResult: Result<ASAuthorization, Error>) {
        switch authResult {
        case .success(let auth):
            print(auth)
            switch auth.credential {
            case let appleIdCredentials as ASAuthorizationAppleIDCredential:
                print(appleIdCredentials)
                
                self.signInApple(appleIdCredentials)
                
            default:
                print(auth.credential)
            }
            
        case .failure(let error):
            print(error)
        }
    }
    
    private func signInApple(_ appleIdCredentials: ASAuthorizationAppleIDCredential) {
        self.globalError = nil
                
        guard let authorizationCode = appleIdCredentials.authorizationCode else {
            return
        }
        
        let code = String(decoding: authorizationCode, as: UTF8.self)
        
        guard !self.signing else { return }
        
        self.signing = true
        
        self.authModel.signInApple(code: code) { result in
            defer {
                self.signing = false
            }
            
            switch result {
            case .failure(let error):
                switch error {
                case SignInAppleError.baseError(let message):
                    self.globalError = message
                }
            case .success(_):
                break
            }
        }
    }
    
    func signInFacebook() {
        guard !self.signing else { return }
        
        self.signing = true
        
        self.authModel.signInFacebook { result in
            defer {
                self.signing = false
            }
            
            switch result {
            case .failure(let error):
                switch error {
                case SignInFacebookError.baseError(let message):
                    self.globalError = message
                }
            case .success(_):
                break
            }
        }
    }
    
    func signInGoogle() {
        guard !self.signing else { return }
        
        self.signing = true
        
        self.authModel.signInGoogle { result in
            defer {
                self.signing = false
            }
            
            switch result {
            case .failure(let error):
                switch error {
                case SignInGoogleError.baseError(let message):
                    self.globalError = message
                }
            case .success(_):
                break
            }
        }
    }
}
