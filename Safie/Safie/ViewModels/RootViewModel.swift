//
//  RootViewModel.swift
//  Safie
//
//  Created by Matyáš Procházka on 24.06.2021.
//

import Foundation
import Combine
import SwiftUI

protocol RootViewModelProtocol: ObservableObject {
    associatedtype AuthModel: AuthModelProtocol
    associatedtype AppModel: AppModelProtocol
    
    var authModel: AuthModel { get }
    var appModel: AppModel { get }
    
    var ready: Bool { get set }
    var isSignedIn: Bool { get set }
}

final class RootViewModel: RootViewModelProtocol {
    var authModel: AuthModel
    var appModel: AppModel        
    
    @Published var ready: Bool = false
    @Published var isSignedIn: Bool = false
        
    private var subscriptions = Set<AnyCancellable>()
    
    init() {            
        self.authModel = DI.shared.container.resolve(AuthModel.self)!
        self.appModel = DI.shared.container.resolve(AppModel.self)!
        
        self.authModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        self.appModel.objectWillChange.sink { _ in
            self.objectWillChange.send()
        }.store(in: &subscriptions)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2500)) {
            withAnimation {
                self.ready = true
            }
        }
    }
}
