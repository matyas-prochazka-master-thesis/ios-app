//
//  LanguageController.swift
//  Safie
//
//  Created by Matyáš Procházka on 03.03.2022.
//

import Foundation

struct Language {
    let code: String
    let name: String
}

final class LanguageController {
    static let shared = LanguageController()
    
    enum Constansts {
        enum AppGroup {
            static let identifier = "group.com.belcode.safie.container"
        }
        enum UserDefaults {
            static let selectedLanguageKey = "SelectedLanguageKey"
        }
    }
    
    let availableLanguages = [
        Language(code: "en", name: "English"),
        Language(code: "cs", name: "Czech")
    ]
    
    var defaultLanguage: Language { availableLanguages.first! }
    
    private init() {}
    
    func getLanguage() -> Language {
        guard let appGroupDefaults = UserDefaults(suiteName: Constansts.AppGroup.identifier) else {
            fatalError("Can't find app group with identifier: \(Constansts.AppGroup.identifier)")
        }
        
        if
            let selectedLanguage = appGroupDefaults.string(forKey: Constansts.UserDefaults.selectedLanguageKey),
            let language = availableLanguages.first(where: { $0.code == selectedLanguage })
        {
            return language
        }
        
        return defaultLanguage
    }
    
    func setLanguage(_ code: String) {
        guard let appGroupDefaults = UserDefaults(suiteName: Constansts.AppGroup.identifier) else {
            fatalError("Can't find app group with identifier: \(Constansts.AppGroup.identifier)")
        }
        
        appGroupDefaults.setValue(code, forKey: Constansts.UserDefaults.selectedLanguageKey)
    }
}
